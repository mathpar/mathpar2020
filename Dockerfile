FROM tomcat:10.1.30-jdk21

RUN rm -r webapps/* | true
ADD target/mathpar.war /usr/local/tomcat/webapps/ROOT.war