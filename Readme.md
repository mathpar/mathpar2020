# How to work with the project
## Setup
1. Install JAVA and MAVEN
2. Run from terminal/cmd: mvn -DskipTests=true clean package

## Run

Run either local server or docker image

### Local server
Run from terminal/cmd: mvn -DskipTests=true spring-boot:run

### Docker
docker build -t mathpar .
docker run -d -p 8080:8080 mathpar

Navigate to http://localhost:8080

## Test
Either run *mvn test* from terminal/cmd or run tests in your IDE.

### Tests in IntelliJ IDEA
#### Execution
Right click on *src -> test -> java* and then Run 'All Tests'. You may also run specific tests from your package by navigating further.

#### Check coverage
After you've run the tests, go to Main Menu -> Run -> Show Coverage Data, select test results and click show selected.

## Commit guidelines
https://gist.github.com/robertpainsi/b632364184e70900af4ab688decf6f53

## Branch strategy
We work in branch develop. When it is ready to be pushed further, we merge it to master.
When working on a feature, create branch feature/name_of_a_feature. When this feature is ready, create a new pull request.
If it is approved, it will be merged to develop branch.