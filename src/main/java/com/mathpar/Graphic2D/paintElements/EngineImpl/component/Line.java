package com.mathpar.Graphic2D.paintElements.EngineImpl.component;

import com.mathpar.Graphic2D.paintElements.EngineImpl.operator.MiddleOfLine;
import com.mathpar.Graphic2D.paintElements.EngineImpl.register.ArgType;
import com.mathpar.Graphic2D.paintElements.GeometryEngine.error.OperatorException;
import com.mathpar.Graphic2D.paintElements.GeometryEngine.interfaces.GeometryVar;
import lombok.Getter;
import lombok.Setter;

import java.util.Collections;

public class Line implements GeometryVar {
    public Point point1;
    public Point point2;

    @Setter
    @Getter
    private TypeOfLine typeOfLine = TypeOfLine.LINE;
    @Getter
    private double k;
    @Getter
    private double b;

    @Override
    public Point getDisplayPoint() {
        Point point = null;
        try {
            point = (Point) new MiddleOfLine()
                    .apply(
                            Collections.singletonList(new Line(point1, point2)),
                            null,
                            null
                    );
        } catch (OperatorException ignored) {
        }

        assert point != null;
        point.setY(point.getY() + 0.3);

        return point;
    }

    public Line(Point point1, Point point2) {
        this.point1 = point1;
        this.point2 = point2;
        calculateKAndB();
    }

    public Point getPoint1() {
        return point1;
    }

    public void setPoint1(Point point1) {
        this.point1 = point1;
        calculateKAndB();
    }

    public Point getPoint2() {
        return point2;
    }

    public void setPoint2(Point point2) {
        this.point2 = point2;
        calculateKAndB();
    }

    public boolean isPointOnThisLine(Point p) {
        double x = p.getX();
        double y = p.getY();

        return y == getK() * x + getB();
    }

    private void calculateKAndB() {
        if (point2.getX() != point1.getX()) {
            this.k = (point2.getY() - point1.getY()) / (point2.getX() - point1.getX());
            this.b = (point1.getY() * point2.getX() - point2.getY() * point1.getY()) / (point2.getX() - point1.getX());
        } else {
            this.k = Double.POSITIVE_INFINITY;
            this.b = point1.getX();
        }
    }

    public boolean containPoint(Point p){
        if(k!=Double.POSITIVE_INFINITY)
            return (p.getY()==k*p.getX()+b)&&(((p.getX()<=Math.max(point1.x,point2.x)) && (p.getX())>=Math.min(point1.x,point2.x))&&((p.getY()<=Math.max(point1.y,point2.y)) && (p.getY())>=Math.min(point1.y,point2.y)));
        return (p.x==point1.x) && ((p.getY()<=Math.max(point1.y,point2.y)) && (p.getY())>=Math.min(point1.y,point2.y));
    }

    @Override
    public String toString() {
        return "Line{" +
                "point1=" + point1 +
                "point2=" + point2 +
                '}';
    }

    @Override
    public ArgType getType() {
        return ArgType.LINE;
    }

    @Override
    public String draw() {
        return String.format("\\tablePlot([[%s,%s],[%s,%s]])", this.point1.getX(), this.point2.getX(), this.point1.getY(), this.point2.getY());
    }

    public enum TypeOfLine {
        LINE,
        INFINITE_LINE,
        RAY
    }
}
