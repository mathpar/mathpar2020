package com.mathpar.Graphic2D.paintElements.EngineImpl.component;

import com.mathpar.Graphic2D.paintElements.EngineImpl.register.ArgType;
import com.mathpar.Graphic2D.paintElements.GeometryEngine.interfaces.GeometryVar;
import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
public class Point implements GeometryVar {
    public double x;
    public double y;

    @Override
    public Point getDisplayPoint() {
        return new Point(x + 0.1, y + 0.1);
    }

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getLengthToPoint(Point p) {
        return Math.sqrt(Math.pow(x - p.x, 2) + Math.pow(y - p.y, 2));
    }

    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    @Override
    public String draw() {
        return String.format("\\pointsPlot([[%s],[%s]], [\\hbox{O}])", this.x, this.y);
    }

    public double[] getCoordinates(Point point) {
        double[] coords = new double[2];
        coords[0] = point.x;
        coords[1] = point.y;
        return coords;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Point point = (Point) o;
        return Double.compare(point.x, x) == 0 &&
                Double.compare(point.y, y) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    @Override
    public ArgType getType() {
        return ArgType.POINT;
    }

}
