package com.mathpar.Graphic2D.paintElements.EngineImpl.component;

import com.mathpar.Graphic2D.paintElements.EngineImpl.register.ArgType;
import com.mathpar.Graphic2D.paintElements.GeometryEngine.interfaces.GeometryVar;
import lombok.Getter;
import lombok.Setter;

public class Text implements GeometryVar {

    public Text(String text,Point start,int font_size){
        fontSize = font_size;
        startPoint = start;
        this.text = text;
    }

    private int fontSize;
    private Point startPoint;
    @Getter
    private String text;

    @Override
    public ArgType getType() {
        return ArgType.TEXT;
    }

    @Override
    public String draw() {
        return "\\textPlot(['"+ text +"', "+ fontSize +", "+ startPoint.getX()+", "+ startPoint.getY() +", 1])";
    }

    public static void main(String[] args) {
        Text t = new Text("Test",new Point(1,1),15);
        System.out.println(t.draw());
    }
}
