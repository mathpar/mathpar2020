package com.mathpar.Graphic2D.paintElements.EngineImpl.operator;

import com.mathpar.Graphic2D.paintElements.EngineImpl.component.Line;
import com.mathpar.Graphic2D.paintElements.EngineImpl.component.Point;
import com.mathpar.Graphic2D.paintElements.EngineImpl.register.ArgType;
import com.mathpar.Graphic2D.paintElements.GeometryEngine.error.OperatorException;
import com.mathpar.Graphic2D.paintElements.GeometryEngine.interfaces.GeometryVar;
import com.mathpar.Graphic2D.paintElements.GeometryEngine.interfaces.Operator;
import com.mathpar.Graphic2D.paintElements.GeometryEngine.model.ScreenParams;
import com.mathpar.number.Ring;

import java.util.Collections;
import java.util.List;

public class MiddleOfLine implements Operator {

    private static final List<ArgType> argTypes = Collections.unmodifiableList(Collections.singletonList(
            ArgType.LINE
    ));

    @Override
    public List<ArgType> getArgTypes() {
        return argTypes;
    }

    @Override
    public String getFullName() {
        return "middle";
    }

    @Override
    public GeometryVar apply(List<GeometryVar> args, ScreenParams screen, Ring ring) throws OperatorException {

        Line line = (Line) args.listIterator().next();
        if(line.getTypeOfLine() == Line.TypeOfLine.LINE)
            return new Point((line.getPoint1().getX() + line.getPoint2().getX())/2,(line.getPoint1().getY() + line.getPoint2().getY())/2);
        else
            throw new OperatorException("Wrong type of line");
    }
}
