package com.mathpar.Graphic2D.paintElements.EngineImpl.operator;

import com.mathpar.Graphic2D.paintElements.EngineImpl.component.IntNum;
import com.mathpar.Graphic2D.paintElements.EngineImpl.component.Point;
import com.mathpar.Graphic2D.paintElements.EngineImpl.register.ArgType;
import com.mathpar.Graphic2D.paintElements.GeometryEngine.interfaces.GeometryVar;
import com.mathpar.Graphic2D.paintElements.GeometryEngine.interfaces.Operator;
import com.mathpar.Graphic2D.paintElements.GeometryEngine.model.ScreenParams;
import com.mathpar.number.Ring;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;

/**
 * Intentionally set unregistered
 */
public class Set2D implements Operator {

    private static final List<ArgType> argTypes = Collections.unmodifiableList(Arrays.asList(
            ArgType.INT, ArgType.INT, ArgType.INT, ArgType.INT
    ));

    private static final String name = "set2D";


    @Override
    public List<ArgType> getArgTypes() {
        return argTypes;
    }

    @Override
    public String getFullName() {
        return name;
    }

    @Override
    public GeometryVar apply(List<GeometryVar> args, ScreenParams screen, Ring ring) {
        ListIterator<GeometryVar> argsIt = args.listIterator();
        // args are already validated, so it's ok to keep using iterator to get all args
        IntNum xMin = (IntNum) argsIt.next();
        IntNum xMax = (IntNum) argsIt.next();
        IntNum yMin = (IntNum) argsIt.next();
        IntNum yMax = (IntNum) argsIt.next();

        screen.setXMin(xMin.getValue());
        screen.setXMax(xMax.getValue());
        screen.setYMin(yMin.getValue());
        screen.setYMax(yMax.getValue());

        return new Point(xMin.getValue(), yMin.getValue());
    }

}
