package com.mathpar.Graphic2D.paintElements.EngineImpl.operator;

import com.mathpar.Graphic2D.paintElements.EngineImpl.component.ArrayOfGeometryVar;
import com.mathpar.Graphic2D.paintElements.EngineImpl.component.Circle;
import com.mathpar.Graphic2D.paintElements.EngineImpl.component.IntNum;
import com.mathpar.Graphic2D.paintElements.EngineImpl.component.Point;
import com.mathpar.Graphic2D.paintElements.EngineImpl.register.ArgType;
import com.mathpar.Graphic2D.paintElements.GeometryEngine.error.OperatorException;
import com.mathpar.Graphic2D.paintElements.GeometryEngine.interfaces.GeometryVar;
import com.mathpar.Graphic2D.paintElements.GeometryEngine.interfaces.Operator;
import com.mathpar.Graphic2D.paintElements.GeometryEngine.model.ScreenParams;
import com.mathpar.number.Ring;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;

public class TangentFromPointToCircle implements Operator {
    private static final List<ArgType> argTypes = Collections.unmodifiableList(Arrays.asList(
            ArgType.POINT, ArgType.CIRCLE, ArgType.INT
    ));

    @Override
    public List<ArgType> getArgTypes() {
        return argTypes;
    }

    @Override
    public String getFullName() {
        return "Tangents";
    }



    @Override
    public GeometryVar apply(List<GeometryVar> args, ScreenParams screen, Ring ring) throws OperatorException {

        ListIterator<GeometryVar> argsIt = args.listIterator();
        Point point = (Point) argsIt.next();
        Circle circle = (Circle) argsIt.next();
        int index = ((IntNum)argsIt.next()).getValue();
        if(point.getLengthToPoint(circle.getCenter())<circle.getRadius())
            throw new OperatorException("Point in circle");
        else if (point.getLengthToPoint(circle.getCenter()) == circle.getRadius())
            return new ArrayOfGeometryVar(new Point[]{point});
        if (index<0||index>1)
            throw new OperatorException("Wrong index");
        double b = point.getLengthToPoint(circle.getCenter());
        double th = Math.acos(circle.getRadius() / b);
        double d = Math.atan2(point.getY() - circle.getCenter().getY(), point.getX() - circle.getCenter().getX());
        double d1 = d + th ;
        double d2 = d - th ;

        Point p1 = new Point(circle.getCenter().getX() + circle.getRadius() * Math.cos(d1),circle.getCenter().getY() + circle.getRadius() * Math.sin(d1));
        Point p2 = new Point(circle.getCenter().getX() + circle.getRadius() * Math.cos(d2),circle.getCenter().getY() + circle.getRadius() * Math.sin(d2));
        return index==0?p1:p2;
    }
}
