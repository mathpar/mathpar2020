package com.mathpar.Graphic2D.paintElements.EngineImpl.operator;

import com.mathpar.Graphic2D.paintElements.EngineImpl.component.*;
import com.mathpar.Graphic2D.paintElements.EngineImpl.register.ArgType;
import com.mathpar.Graphic2D.paintElements.GeometryEngine.error.OperatorException;
import com.mathpar.Graphic2D.paintElements.GeometryEngine.interfaces.GeometryVar;
import com.mathpar.Graphic2D.paintElements.GeometryEngine.interfaces.Operator;
import com.mathpar.Graphic2D.paintElements.GeometryEngine.model.ScreenParams;
import com.mathpar.number.Array;
import com.mathpar.number.Ring;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;
import java.util.stream.Collectors;

public class TangentsFromPointToCircle implements Operator {

    private static final List<ArgType> argTypes = Collections.unmodifiableList(Arrays.asList(
            ArgType.POINT, ArgType.CIRCLE
    ));

    @Override
    public List<ArgType> getArgTypes() {
        return argTypes;
    }

    @Override
    public String getFullName() {
        return "Tangents";
    }



        @Override
    public GeometryVar apply(List<GeometryVar> args, ScreenParams screen, Ring ring) throws OperatorException {

        ListIterator<GeometryVar> argsIt = args.listIterator();
        Point point = (Point) argsIt.next();
        Circle circle = (Circle) argsIt.next();

        if(point.getLengthToPoint(circle.getCenter())<circle.getRadius())
            throw new OperatorException("Point in circle");
        else if (point.getLengthToPoint(circle.getCenter()) == circle.getRadius())
            return new ArrayOfGeometryVar(new Point[]{point});
        double b = point.getLengthToPoint(circle.getCenter());
        double th = Math.acos(circle.getRadius() / b);
        double d = Math.atan2(point.getY() - circle.getCenter().getY(), point.getX() - circle.getCenter().getX());
        double d1 = d + th ;
        double d2 = d - th ;

        Point p1 = new Point(circle.getCenter().getX() + circle.getRadius() * Math.cos(d1),circle.getCenter().getY() + circle.getRadius() * Math.sin(d1));
        Point p2 = new Point(circle.getCenter().getX() + circle.getRadius() * Math.cos(d2),circle.getCenter().getY() + circle.getRadius() * Math.sin(d2));
        return new ArrayOfGeometryVar(new GeometryVar[]{p1,p2});
    }

    public static void main(String[] args) throws OperatorException {
        Point point = new Point(7, 8);
        Circle circle = new Circle(5,new Point(0, 1));

        TangentsFromPointToCircle tg = new TangentsFromPointToCircle();
        List<GeometryVar> arrgs = Collections.unmodifiableList(Arrays.asList(
                point, circle
        ));

        ArrayOfGeometryVar arr = (ArrayOfGeometryVar) tg.apply(arrgs, new ScreenParams(), null);
        Point res = ((Point)arr.getVars()[0]);
        System.out.println(res.getLengthToPoint(circle.getCenter()));

        res = ((Point)arr.getVars()[1]);
        System.out.println(res.getLengthToPoint(circle.getCenter()));

        System.out.println(arr.draw());
    }
}
