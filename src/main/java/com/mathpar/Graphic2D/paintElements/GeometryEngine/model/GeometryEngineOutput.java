package com.mathpar.Graphic2D.paintElements.GeometryEngine.model;

import com.mathpar.Graphic2D.paintElements.GeometryEngine.error.EngineException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GeometryEngineOutput {
    private String processedInput;
    private String outputCode;
    private EngineException error;
}
