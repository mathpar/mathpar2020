package com.mathpar.Graphic2D.paintElements.GeometryEngine.model;

import com.mathpar.Graphic2D.paintElements.GeometryEngine.interfaces.Operator;
import com.mathpar.Graphic2D.paintElements.GeometryEngine.interfaces.VarStorage;
import com.mathpar.number.Ring;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Map;

@Data
@AllArgsConstructor
public class ParserParams {
    String line;
    VarStorage vars;
    Map<String, Map<String, Operator>> operators;
    ScreenParams screen;
    Ring ring;
}
