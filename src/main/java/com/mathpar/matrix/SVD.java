package com.mathpar.matrix;
import com.mathpar.number.*;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Random;

public class SVD {
    public static void main(String[] args) {
        Ring ring = new Ring("R64[]");
    for (int n=4; n<64; n=n*2){
        double[][] m = generateMatrix(n);
        MatrixD A = new MatrixD(m, ring);
        System.out.print("n="+n+"   ");
    //    System.out.println(A);
        long t1= System.currentTimeMillis();
        MatrixD[] result = SVD(A, ring);
        long t2= System.currentTimeMillis()-t1;
        System.out.println(" time="+t2);
        MatrixD U = result[0];
        MatrixD D = result[1];
        MatrixD W = result[2];
        MatrixD B = U.multCU(D, ring).multCU(W, ring);
        MatrixD Check = B.subtract(A, ring);
        System.out.println("Check final SVD: maxAbs absolute value = " + Check.max(ring).value);
    }}

    public static double[][] generateMatrix(int N) {
        Random rand = new Random();
        double[][] res = new double[N][N];

        for(int i =0; i < N ; i++) {
            for (int j = 0; j < N; j++) {
                res[i][j] = rand.nextDouble() * 10.0;
            }
        }

        return res;
    }

    public static double[][] getPerfectMatrix() {
        return new double[][] {{1,2,1,1}, {1,1,2,1}, {1,1,1,2}, {1,2,1,1}};
    }

    public static double[][] getPerfectBDMatrix() {
        return new double[][] {{-2, 4.33, -0, -0}, {0, 0.5, 0, 0}, {0, -0, 1, -0}, {0, -0, -0, -1}};
    }

    public static void print_matrix(MatrixD m) {
        for (int i = 0; i < m.M.length; ++i) {
            for (int j = 0; j < m.M.length; ++j)
                System.out.printf("%3.4f ", m.M[i][j].doubleValue());
            System.out.println("");
        }
        System.out.println("");
    }

    public static MatrixD[] SVD(MatrixD A, Ring ring) {
        if (A.rowNum() != A.colNum()) return null;
        //    long tt1=System.currentTimeMillis();
        MatrixD[] result = biDiagonalize(A, ring);
    //    long tt2=System.currentTimeMillis()-tt1;
     //   System.out.println("timeBiDiag="+tt2+"  ");
        MatrixD d=result[1]; int n= d.M.length;
        Element[][] DD=new Element[2][n];
        for(int k=0; k<n-1;k++){DD[1][k]=d.M[k][k]; DD[0][k]=d.M[k][k+1];}
        DD[1][n-1]=d.M[n-1][n-1]; DD[0][n-1]=ring.numberZERO;;
       MatrixD dd=new MatrixD(DD);
     //   System.out.println(dd);
        MatrixD[] diagonal = diagonalize(result[1], ring);
        result[0] = result[0].transpose(ring).multCU(diagonal[0].transpose(ring), ring);
        result[1] = diagonal[1];
        result[2] = diagonal[2].transpose(ring).multCU(result[2].transpose(ring), ring);

        return result;
    }

    public static MatrixD[] biDiagonalize(MatrixD A, Ring ring) {
        int nn=A.M.length; int nn1 = nn - 1;
        MatrixD I = MatrixD.ONE(nn, ring);
        MatrixD Ai =A;
        MatrixD UU=MatrixD.ONE(nn, ring);
        MatrixD WW=MatrixD.ONE(nn, ring);
        int i = 0;
        while (true) {
            VectorS x = new VectorS( Ai.takeColumn(i + 1).V.clone());
            for (int k = 0; k < i; k++) {x.V[k] = ring.numberZERO;}
            Element norm_x = x.norm2(i, ring).sqrt(ring);
            if (! norm_x.isZero(ring)) {
                VectorS u = new VectorS(x.V.clone());
                u.V[i] = u.V[i].subtract(norm_x, ring);
                VectorS Ut = (VectorS) u.transpose(ring);
                Element XU = x.multiply(Ut, ring);
                Element mult = ring.numberONE.divide(XU, ring);
                MatrixD uUt = (MatrixD) Ut.multiply(u, ring);
                VectorS Vec=(VectorS) u.multiply(Ai, ring);
                VectorS VecU=(VectorS) u.multiply(UU, ring);
                MatrixD UtUAi = (MatrixD) Ut.multiply( Vec,ring).multiply(mult, ring);
                Ai = (MatrixD) Ai.subtract(UtUAi, ring);
                MatrixD UtU_U = (MatrixD) Ut.multiply( VecU,ring).multiply(mult, ring);
                UU = (MatrixD) UU.subtract(UtU_U, ring);
            }
            if (i == nn1-1) {break;}
            VectorS x_r =new VectorS( Ai.takeRow(i + 1).V.clone());
            for (int k = 0; k < i + 1; k++) x_r.V[k] = ring.numberZERO;
            Element norm_x_r = x_r.norm2(i + 1, ring).sqrt(ring);
            if (! norm_x_r.isZero(ring)) {
                VectorS u_r = new VectorS(x_r.V.clone());
                u_r.V[i + 1] = u_r.V[i + 1].subtract(norm_x_r, ring);
                VectorS Ut_r = (VectorS) u_r.transpose(ring);
                Element XU_r = x_r.multiply(Ut_r, ring);
                Element mult_r = ring.numberONE.divide(XU_r, ring);
                MatrixD uUt_r = (MatrixD) Ut_r.multiply(u_r, ring);
                VectorS VecT=(VectorS) Ai.multiply(Ut_r, ring);
                VectorS VecW = (VectorS) WW.multiply(Ut_r, ring);
                MatrixD AiUtU = (MatrixD)VecT.multiply( u_r,ring).multiply(mult_r, ring);
                Ai = (MatrixD) Ai.subtract(AiUtU, ring);
                MatrixD W_UtU = (MatrixD) VecW.multiply(u_r,ring).multiply(mult_r, ring);
                WW = (MatrixD) WW.subtract(W_UtU, ring);
            }
            i++;
        }
        return new MatrixD[]{UU, Ai, WW};
    }



    public static MatrixD[] biDiagonalize_Old(MatrixD A, Ring ring) {
        MatrixD I = new MatrixD(MatrixS.scalarMatrix(A.M.length, ring.numberONE, ring));
        int operationsAmount = A.M.length - 1;
        int i = 0;
        MatrixD Ai = A;

        ArrayList<MatrixD> As = new ArrayList<>(2 * A.M.length - 3);
        ArrayList<MatrixD> Ps = new ArrayList<>(A.M.length - 1);
        ArrayList<MatrixD> Qs = new ArrayList<>(A.M.length - 2);

        while (i < operationsAmount) {
            VectorS x = Ai.takeColumn(i + 1);
            for (int k = 0; k < i; k++) {
                x.V[k] = ring.numberZERO;
            }

            if (x.norm2(i + 1, ring).isZero(ring)) {
                Ps.add(new MatrixD(I.M.clone()));
            } else {
                Element norm_x = x.norm(ring);
                VectorS u = new VectorS(x.V.clone());

                if (u.V[i].value < 0)
                    u.V[i] = u.V[i].subtract(norm_x, ring);
                else
                    u.V[i] = u.V[i].add(norm_x, ring);

                Element e = x.V[i];
                if (x.V[i].value < 0)
                    e = x.V[i].multiply(new NumberR64(-1), ring);

                Element norm_Squ = norm_x.multiply(norm_x.add(e, ring), ring).multiply(new NumberR64(2), ring);
                VectorS ut = (VectorS) u.transpose(ring);
                NumberR64 norm_div = NumberR64.valueOf(2 / norm_Squ.value);
                MatrixD uut = (MatrixD) ut.multiply(u, ring);
                MatrixD nd_uut = (MatrixD) norm_div.multiply(uut, ring);
                MatrixD Pi = I.subtract(nd_uut, ring);
                Ps.add(Pi);

                Element dT = u.multiply(Ai, ring);
                Element nd_u = norm_div.multiply(ut, ring);
                Element nd_u_dT = nd_u.multiply(dT, ring);
                Ai = (MatrixD) Ai.subtract(nd_u_dT, ring);
                As.add(new MatrixD(Ai.M.clone()));
            }

            if (i == (A.M.length - 2)) {
                i++;
                continue;
            }

            VectorS x_r = new VectorS(Ai.takeRow(i + 1).V.clone());

            for (int k = 0; k < i + 1; k++) {
                x_r.V[k] = ring.numberZERO;
            }

            if (x_r.norm2(i + 2, ring).isZero(ring)) {
                Qs.add(new MatrixD(I.M.clone()));
            } else {
                Element norm_x_r = x_r.norm(ring); //new FvalOf(ring).getFunctionValueR64(F.SQRT, ((F) x_r.norm(ring)).X[0]);
                VectorS u_r = new VectorS(x_r.V.clone());

                if (u_r.V[i + 1].value < 0)
                    u_r.V[i + 1] = u_r.V[i + 1].subtract(norm_x_r, ring);
                else
                    u_r.V[i + 1] = u_r.V[i + 1].add(norm_x_r, ring);

                Element e_r = x_r.V[i + 1];
                if (x_r.V[i + 1].value < 0)
                    e_r = x_r.V[i + 1].multiply(new NumberR64(-1), ring);
                Element norm_Squ_r = norm_x_r.multiply(norm_x_r.add(e_r, ring), ring).multiply(new NumberR64(2), ring);
                VectorS ut_r = (VectorS) u_r.transpose(ring);
                Element norm_div_r = NumberR64.valueOf(2 / norm_Squ_r.value);
                MatrixD utu_r = (MatrixD) ut_r.multiply(u_r, ring);
                MatrixD nd_utu_r = (MatrixD) norm_div_r.multiply(utu_r, ring);
                MatrixD Qi = I.subtract(nd_utu_r, ring);
                Qs.add(Qi);

                VectorS d_r = (VectorS) Ai.multiply(ut_r, ring);
                VectorS nd_d_r = (VectorS) norm_div_r.multiply(d_r, ring);
                MatrixD nd_d_u_r = (MatrixD) nd_d_r.multiply(u_r, ring);
                Ai = Ai.subtract(nd_d_u_r, ring);
                As.add(new MatrixD(Ai.M.clone()));
            }

            i++;
        }
        MatrixD U;
        if (!Ps.isEmpty()) {
            U = Ps.get(Ps.size() - 1);
            for (int j = Ps.size() - 2; j >= 0; j--) {
                U = U.multCU(Ps.get(j), ring);
            }
        } else U = MatrixD.ONE(operationsAmount + 1, ring);

        MatrixD W;
        if (!Qs.isEmpty()) {
            W = Qs.get(0);
            for (int j = 1; j < Qs.size(); j++) {
                W = W.multCU(Qs.get(j), ring);
            }
        } else W = MatrixD.ONE(operationsAmount + 1, ring);

        MatrixD A_final = As.get(As.size() - 1);
        return new MatrixD[]{U, A_final, W};
    }

    public static void checkBidiagonalization(MatrixD initialMatrix, MatrixD[] result, Ring ring, boolean printDetails) {
        if (printDetails) {
            System.out.println("----------------U---------------");
            System.out.println(result[0]);
            System.out.println("----------------A---------------");
            System.out.println(result[1]);
            System.out.println("----------------W---------------");
            System.out.println(result[2]);
        }

        MatrixD UA = result[0].transpose(ring).multCU(result[1], ring);

        if (printDetails) {
            System.out.println("----------------UA---------------");
            System.out.println(UA);
        }

        MatrixD UAW = UA.multCU(result[2].transpose(ring), ring);

        if (printDetails) {
            System.out.println("----------------UAW---------------");
            System.out.println(UAW);
        }

        MatrixD Check = UAW.subtract(initialMatrix, ring);
        System.out.println("Check BD: maxAbs absolute value = " + Check.max(ring).value);
    }

    // ********************* NIKOLAY ***************************
    public static MatrixD[] diagonalize(MatrixD A, Ring r) {
        int n = A.rowNum();
        MatrixD left;
        MatrixD right;
        MatrixD tmp = A.copy();
        MatrixD L = MatrixD.ONE(n, r);
        MatrixD R = MatrixD.ONE(n, r);

        boolean side = true;
        int iteration = 1;

        while (!checkSecondDiagonalValues(tmp, n, r)) {
            if (side) {
                for (int i = 0; i < n - 1; ++i) {
                    if (!tmp.getElement(i, i + 1).isZero(r)) {
                        right = getRightGivensRotationMatrix(n, i, tmp.getElement(i, i), tmp.getElement(i, i + 1), r);
                        R = multLinesRight(R, right, i, r);
                        tmp = multSquareRight(tmp, right, i, r);
                    }
                }
            } else {
                for (int j = 0; j < n - 1; ++j) {
                    if (!tmp.getElement(j + 1, j).isZero(r)) {
                        left = getGivensRotationMatrix(n, j, tmp.getElement(j, j), tmp.getElement(j + 1, j), r);
                        L = multLinesLeft(left, L, j, r);
                        tmp = multSquareLeft(left, tmp, j, r);
                    }
                }
            }
            side = !side;
            iteration++;
        }
        return  new MatrixD[] {L, tmp, R};
    }

    public static  MatrixD getUpperGivensMatrix(int n, int i, Element a, Element b, Ring ring){
        MatrixD G = MatrixD.ONE(n, ring);

        if (b.isZero(ring)) {
            G.M[i][i] = ring.numberONE;
            G.M[i][i + 1] = ring.numberZERO;
            G.M[i + 1][i] = ring.numberZERO;
            G.M[i + 1][i + 1] = ring.numberONE;
        } else {
            Element c = new Element(1), s = new Element(0);
            Element r = (a.pow(2, ring).add(b.pow(2, ring), ring)).sqrt(ring);

            c = a.divide(r, ring);
            s = b.divide(r, ring);

            if (c.isInfinite() || s.isInfinite()) {
                G.M[i][i] = ring.numberONE;
                G.M[i][i + 1] = ring.numberZERO;
                G.M[i + 1][i] = ring.numberZERO;
                G.M[i + 1][i + 1] = ring.numberONE;
            } else {
                G.M[i][i] = c;
                G.M[i][i + 1] = s;
                G.M[i + 1][i] = s.negate(ring);
                G.M[i + 1][i + 1] = c;
            }
        }

        return G;
    }

    public static MatrixD getRightGivensRotationMatrix(int n, int i, Element a, Element b, Ring ring) {
        MatrixD G = MatrixD.ONE(n, ring);
        Element r = (a.pow(2, ring).add(b.pow(2, ring), ring)).sqrt(ring);     // Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));

        if (b.isZero(ring) || r.value <= 0) {
            G.M[i][i] = ring.numberONE;
            G.M[i][i + 1] = ring.numberZERO;
            G.M[i + 1][i] = ring.numberZERO;
            G.M[i + 1][i + 1] = ring.numberONE;
        } else {
            Element c = a.divide(r, ring);                                            // a/r;
            Element s = b.divide(r, ring);                               // (-b)/r;
            if (c.isInfinite() || s.isInfinite()) {
                G.M[i][i] = ring.numberONE;
                G.M[i][i + 1] = ring.numberZERO;
                G.M[i + 1][i] = ring.numberZERO;
                G.M[i + 1][i + 1] = ring.numberONE;
            } else {
                G.M[i][i] = c;
                G.M[i][i + 1] = s.negate(ring);
                G.M[i + 1][i] = s;
                G.M[i + 1][i + 1] = c;
            }
        }
        return G;
    }

    public static MatrixD getGivensRotationMatrix(int n, int i, Element a, Element b, Ring ring) {
        MatrixD G = MatrixD.ONE(n, ring);
        if (b.isZero(ring)) {
            G.M[i][i] = ring.numberONE;
            G.M[i][i + 1] = ring.numberZERO;
            G.M[i + 1][i] = ring.numberZERO;
            G.M[i + 1][i + 1] = ring.numberONE;
        } else {
            Element r = (a.pow(2, ring).add(b.pow(2, ring), ring)).sqrt(ring);     // Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
            Element c = a.divide(r, ring);                                            // a/r;
            Element s = b.divide(r, ring);                               // (-b)/r;
            if (c.isInfinite() || s.isInfinite()) {
                G.M[i][i] = ring.numberONE;
                G.M[i][i + 1] = ring.numberZERO;
                G.M[i + 1][i] = ring.numberZERO;
                G.M[i + 1][i + 1] = ring.numberONE;
            } else {
                G.M[i][i] = c;
                G.M[i][i + 1] = s;
                G.M[i + 1][i] = s.negate(ring);
                G.M[i + 1][i + 1] = c;
            }
        }
        return G;
    }

    public static boolean checkSecondDiagonalValues(MatrixD temp, int n, Ring ring) {
        for (int i = 0; i < (n-1); i++) {
            if (!temp.getElement(i, i+1).isZero(ring) || !temp.getElement(i+1, i).isZero(ring))
                return false;
        }
        return true;
    }


    public static MatrixD getTriangleMatrixNumber64(int n, int mod, Ring ring) {
        MatrixD L = new MatrixD(n, n, mod, ring);
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                if (!(j<=i))
                    L.M[i][j] = ring.numberZERO;
        return L;
    }

    public static MatrixD[] getTwoGivensRotationMatrices(double a, double b, double d, int n, int i, int j, Ring ring) {
        MatrixD left = MatrixD.ONE(n, ring);
        MatrixD right = MatrixD.ONE(n, ring);
        double c = 0d;
        double s = 0d;
        double C = 0d;
        double S = 0d;
        double t = 0d;
        double T = 0d;

        if (a != 0d && d != 0d) {
//            System.out.println("a ≠ 0, d ≠ 0 \n");
            t = ((-1d*(b*b+d*d-a*a)) + Math.sqrt(Math.pow((b*b+d*d-a*a), 2) + 4d*a*a*b*b)) / (2d*a*b);
            T = (-1d/d)*(a*t + b);
        } else if (a != 0d && d == 0d) {
//            System.out.println("a ≠ 0, d = 0 \n");
            T = 0d;
            t = (-b)/a;
        } else if (a == 0d && d != 0d) {
//            System.out.println("a = 0, d ≠ 0 \n");
            t = 0d;
            T = (-b)/d;
        } else {                                    // a = 0 & d = 0
//            System.out.println("a = 0, d = 0 \n");
            c = 1d;
            s = 0d;
            C = 0d;
            S = 1d;
        }

        if (!(a == 0d && d == 0d)) {
            c = Math.sqrt(1d/(1d+t*t));                                 // c = Math.cos(Math.atan(t));
            s = t*c;                                                    // s = Math.sin(Math.atan(t));
            C = Math.sqrt(1d/(1d+T*T));                                 // C = Math.cos(Math.atan(T));
            S = T*C;                                                    // S = Math.sin(Math.atan(T));
        }

        left.M[i][i] = new NumberR64(c);
        left.M[i][j] = new NumberR64(-s);
        left.M[j][i] = new NumberR64(s);
        left.M[j][j] = new NumberR64(c);

        right.M[i][i] = new NumberR64(C);
        right.M[i][j] = new NumberR64(-S);
        right.M[j][i] = new NumberR64(S);
        right.M[j][j] = new NumberR64(C);

        return new MatrixD[]{left, right};
//            long [][] arr = {{1, 0}, {5, 2}};
//            MatrixD test = new MatrixD(arr, ring);
//            MatrixD[] lr = getTwoGivensRotationMatrices(test.getElement(0,0).doubleValue(), test.getElement(1, 0).doubleValue(),
//                    test.getElement(1,1).doubleValue(), 2, 0, 1, ring);
//            System.out.println("L * A * R = \n");
//            System.out.println(lr[0].multiplyMatr(test, ring).multiplyMatr(lr[1], ring).toString());
    }

    public static void removeNonDiagonalValues(MatrixD d, Ring ring) {
        for (int i = 0; i < d.M.length; i++) {
            for (int j = 0; j < d.M[0].length; j++) {
                if (i != j) {
                    d.M[i][j] = ring.numberZERO;
                }
            }
        }
    }

    public static boolean isPowerOfTwo(int number) {
        return number > 0 && ((number & (number - 1)) == 0);
    }

    public static MatrixD getSubMatrix(MatrixD matrix, int start_i, int end_i, int start_j, int end_j) {
        matrix = matrix.copy();
        int rowNum = end_i - start_i + 1;
        int colNum = end_j - start_j + 1;

        Element[][] e = new Element[rowNum][colNum];
        for (int i = start_i; i <= end_i; i++) {
            for (int j = start_j; j <= end_j; j++) {
                e[i-start_i][j-start_j] = matrix.getElement(i, j);
            }
        }

        return new MatrixD(e, 0);
    }

    public static MatrixD insertMatrixToMatrix(MatrixD matrix, MatrixD block, int i_start, int j_start){
        block = block.copy();
        MatrixD result = matrix.copy();

        for (int i = 0; i < block.rowNum(); i++) {
            for (int j = 0; j < block.colNum(); j++) {
                result.M[i+i_start][j+j_start] = block.getElement(i, j);
            }
        }

        return result;
    }

    public static void readBlock(MatrixD matrix, int iOffset, int jOffset, Element[][] elements, int h) {
        for (int i = iOffset; i < (h+iOffset); i++) {
            for (int j = jOffset; j < (h+jOffset); j++) {
                elements[i-iOffset][j-jOffset] = matrix.getElement(i, j);
            }
        }
    }

    public static MatrixD getBlock(MatrixD input, int block) {
        int rowNum = input.rowNum();
        int colNum = input.colNum();

        int i_start = 0, i_end = 0, j_start = 0, j_end = 0;

        if (block == 1) {
            i_start = rowNum / 4;
            i_end = rowNum - (rowNum / 4) - 1;
            j_start = 0;
            j_end = (colNum / 2) - 1;
        } else if (block == 2) {
            i_start = 0;
            i_end = (rowNum / 2) - 1;
            j_start = 0;
            j_end = (colNum / 2) - 1;
        } else if (block == 3) {
            i_start = rowNum / 2;
            i_end = rowNum - 1;
            j_start = colNum / 2;
            j_end = colNum - 1;
        } else if (block == 4) {
            i_start = rowNum / 4;
            i_end = rowNum - (rowNum / 4) - 1;
            j_start = colNum / 2;
            j_end = colNum - 1;
        }

        return getSubMatrix(input, i_start, i_end, j_start, j_end);
    }

    public static MatrixD block4_(MatrixD input, char b)  {
        if ((input.rowNum() != input.colNum()) ||
                !isPowerOfTwo(input.rowNum()))
        {  return null;}


        MatrixD matrix = input.copy();
        int n = matrix.rowNum();
        int h = n/2;

        if (n == 1) {
            return matrix;
        } else {
            switch (b) {
                case 'A': return getSubMatrix(matrix, 0, h-1, 0, h-1);
                case 'B': return getSubMatrix(matrix, 0, h-1, h, n-1);
                case 'C': return getSubMatrix(matrix, h, n-1, 0, h-1);
                case 'D': return getSubMatrix(matrix, h, n-1, h, n-1);
                default: return matrix;
            }
        }
    }

    // В результате у матрицы B поменяется только ряд x и ряд y
    public static MatrixD multSquareRight(MatrixD a, MatrixD b, int pos, Ring ring) {

        Element c11 = a.M[pos][pos].multiply(b.M[pos][pos], ring);
        c11 = c11.add(a.M[pos][pos + 1].multiply(b.M[pos + 1][pos], ring), ring);

        Element c12 = a.M[pos][pos].multiply(b.M[pos][pos + 1], ring);
        c12 = c12.add(a.M[pos][pos + 1].multiply(b.M[pos + 1][pos + 1], ring), ring);

        Element c21 = a.M[pos + 1][pos].multiply(b.M[pos][pos], ring);
        c21 = c21.add(a.M[pos + 1][pos + 1].multiply(b.M[pos + 1][pos], ring), ring);

        Element c22 = a.M[pos + 1][pos].multiply(b.M[pos][pos + 1], ring);
        c22 = c22.add(a.M[pos + 1][pos + 1].multiply(b.M[pos + 1][pos + 1], ring), ring);

        a.M[pos][pos] =  c11;
        a.M[pos][pos + 1] =  c12;
        a.M[pos + 1][pos] =  c21;
        a.M[pos + 1][pos + 1] =  c22;

        return a;
    }

    public static MatrixD multSquareLeft(MatrixD a, MatrixD b, int pos, Ring ring) {

        Element c11 = a.M[pos][pos].multiply(b.M[pos][pos], ring);
        c11 = c11.add(a.M[pos][pos + 1].multiply(b.M[pos + 1][pos], ring), ring);

        Element c12 = a.M[pos][pos].multiply(b.M[pos][pos + 1], ring);
        c12 = c12.add(a.M[pos][pos + 1].multiply(b.M[pos + 1][pos + 1], ring), ring);

        Element c21 = a.M[pos + 1][pos].multiply(b.M[pos][pos], ring);
        c21 = c21.add(a.M[pos + 1][pos + 1].multiply(b.M[pos + 1][pos], ring), ring);

        Element c22 = a.M[pos + 1][pos].multiply(b.M[pos][pos + 1], ring);
        c22 = c22.add(a.M[pos + 1][pos + 1].multiply(b.M[pos + 1][pos + 1], ring), ring);

        b.M[pos][pos] =  c11;
        b.M[pos][pos + 1] =  c12;
        b.M[pos + 1][pos] =  c21;
        b.M[pos + 1][pos + 1] =  c22;

        return b;
    }

    public static MatrixD multLinesRight(MatrixD a, MatrixD b, int line, Ring ring) {
        int size = a.rowNum();
        Element[][] r = new Element[2][size];

        for (int i = 0; i < 2; ++i) {
            if (line + i < size) {

                for (int k = 0; k < size; ++k) {

                    Element v = a.M[k][0].multiply(b.M[0][line + i], ring);

                    for (int j = 1; j < size; ++j) {
                        v = v.add(a.M[k][j].multiply(b.M[j][line + i], ring), ring);
                    }

                    r[i][k] = v;
                }
            }
        }

        for (int i = 0; i < 2; ++i) {
            if (line + i < size) {
                for (int j = 0; j < size; ++j) {
                    a.M[j][i + line] = r[i][j];
                }
            }
        }
        return a;
    }

    public static MatrixD multLinesLeft(MatrixD a, MatrixD b, int line, Ring ring) {
        int size = a.rowNum();
        Element[][] r = new Element[2][size];

        for (int i = 0; i < 2; ++i) {
            if (line + i < size) {

                for (int k = 0; k < size; ++k) {

                    Element v = a.M[line + i][0].multiply(b.M[0][k], ring);

                    for (int j = 1; j < size; ++j) {
                        v = v.add(a.M[line + i][j].multiply(b.M[j][k], ring), ring);
                    }

                    r[i][k] = v;
                }
            }
        }

        for (int i = 0; i < 2; ++i) {
            if (line + i < size) {
                for (int j = 0; j < size; ++j) {
                    b.M[i + line][j] = r[i][j];
                }
            }
        }
        return b;
    }

}
