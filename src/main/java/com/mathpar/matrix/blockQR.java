/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mathpar.matrix;
import static com.mathpar.matrix.BlockExecuter.getGivensRotationMatrix;
import static com.mathpar.matrix.BlockExecuter.getSubMatrix;
import static com.mathpar.matrix.BlockExecuter.isPowerOfTwo;
import static com.mathpar.matrix.BlockExecuter.leftMultiplyGivensToMatrix;
import static com.mathpar.matrix.BlockExecuter.rightMultiplyMatrixToGivens;
import com.mathpar.matrix.MatrixD;
import com.mathpar.number.*;
 
import java.util.concurrent.atomic.AtomicInteger;

public class blockQR {

    private static AtomicInteger counter = new AtomicInteger(1);

    public static void main(String[] args) {
        Ring ring = new Ring("R64[]");
        int[][] m = new int[][]{{1, 1, 1, 1}, {1, 1, 2, 1}, {1, 1, 1, 2}, {1, 2, 1, 1}};
        MatrixD A = new MatrixD(m, ring);
        MatrixD[] QR =  blockQR(A, ring);
        MatrixD Q = QR[0];
        MatrixD R = QR[1];
        MatrixD check = Q.multiplyMatr(R, ring);

        System.out.println("A = \n" + A.toString() + "\n");
        //System.out.println("Q = \n" + Q.toString() + "\n");
        //System.out.println("R = \n" + R.toString() + "\n");
        System.out.println("check = \n" + check.toString() + "\n");

        System.out.println("Threads created: " + counter.get());
    }
    public static MatrixD[]  blockQR(MatrixD A, Ring ring) {
        int rowNum = A.rowNum();
        int colNum = A.colNum();
        int nn=Math.max(colNum, colNum);
        if ((rowNum != colNum) || ! Element.isPowerOfTwo(rowNum) || rowNum == 0)    return null;
        if (rowNum == 2)  return givensQR(A, ring);
        MatrixD M = A.copy();
        MatrixD ones = MatrixD.ONE(M.rowNum(), ring);

        /** 1. QR Разложение блока C */
        MatrixD C = block4(M, 'C');
        MatrixD D = block4(M, 'D');
        MatrixD[] Q1R1 =  blockQR(C, ring);
        MatrixD Q1 = Q1R1[0];
        MatrixD R1 = Q1R1[1];
        MatrixD D1 = Q1.transpose(ring).multiplyMatr(D, ring);
        M = insertMatrixToMatrix(M, R1, M.rowNum() / 2, 0);
        M = insertMatrixToMatrix(M, D1, M.rowNum() / 2, M.colNum() / 2);
        Q1 = insertMatrixToMatrix(ones, Q1, M.rowNum() / 2, M.colNum() / 2);
        //System.out.println("M = \n" + M.toString() + "\n");

        /** 2. Взять левую часть матрицы M и выполнить blockQrForParallelogram + применить изменения к правой стороне */
        MatrixD AC = getSubMatrix(M, 0, M.rowNum() - 1, 0, (M.colNum() / 2) - 1);
        MatrixD BD = getSubMatrix(M, 0, M.rowNum() - 1, M.colNum() / 2, M.colNum() - 1);
        MatrixD[] Q2R2 = blockQrForParallelogram(AC, ring);
        MatrixD Q2 = Q2R2[0];
        MatrixD R2 = Q2R2[1];
        BD = Q2.transpose(ring).multiplyMatr(BD, ring);
        M = insertMatrixToMatrix(M, R2, 0, 0);
        M = insertMatrixToMatrix(M, BD, 0, M.colNum() / 2);
        //System.out.println("M = \n" + M.toString() + "\n");

        /** 3. QR разложение блока D */
        D = block4(M, 'D');
        MatrixD[] Q3R3 =  blockQR(D, ring);
        MatrixD Q3 = Q3R3[0];
        MatrixD R3 = Q3R3[1];
        M = insertMatrixToMatrix(M, R3, M.rowNum() / 2, M.colNum() / 2);
        Q3 = insertMatrixToMatrix(ones, Q3, M.rowNum() / 2, M.colNum() / 2);
        //System.out.println("M = \n" + M.toString() + "\n");

        MatrixD Q = Q1.multiplyMatr(Q2, ring).multiplyMatr(Q3, ring);
        //System.out.println("CHECK: \n" + Q.multiplyMatr(M, ring).toString());

        return new MatrixD[] {Q, M};
    }

        public static MatrixD block4(MatrixD input, char b)  {
        if ((input.rowNum() != input.colNum()) || !isPowerOfTwo(input.rowNum()))
return null;

        MatrixD matrix = input.copy();
        int n = matrix.rowNum();
        int h = n/2;

        if (n == 1) {
            return matrix;
        } else {
            switch (b) {
                case 'A': return getSubMatrix(matrix, 0, h-1, 0, h-1);
                case 'B': return getSubMatrix(matrix, 0, h-1, h, n-1);
                case 'C': return getSubMatrix(matrix, h, n-1, 0, h-1);
                case 'D': return getSubMatrix(matrix, h, n-1, h, n-1);
                default: return matrix;
            }
        }
    }
   
    public static MatrixD insertMatrixToMatrix(MatrixD matrix, MatrixD block, int i_start, int j_start){
        block = block.copy();
        MatrixD result = matrix.copy();

        for (int i = 0; i < block.rowNum(); i++) {
            for (int j = 0; j < block.colNum(); j++) {
                result.M[i+i_start][j+j_start] = block.getElement(i, j);
            }
        }

        return result;
    }
        public static MatrixD getBlock(MatrixD input, int block) {
        int rowNum = input.rowNum();
        int colNum = input.colNum();

        int i_start = 0, i_end = 0, j_start = 0, j_end = 0;

        if (block == 1) {
            i_start = rowNum / 4;
            i_end = rowNum - (rowNum / 4) - 1;
            j_start = 0;
            j_end = (colNum / 2) - 1;
        } else if (block == 2) {
            i_start = 0;
            i_end = (rowNum / 2) - 1;
            j_start = 0;
            j_end = (colNum / 2) - 1;
        } else if (block == 3) {
            i_start = rowNum / 2;
            i_end = rowNum - 1;
            j_start = colNum / 2;
            j_end = colNum - 1;
        } else if (block == 4) {
            i_start = rowNum / 4;
            i_end = rowNum - (rowNum / 4) - 1;
            j_start = colNum / 2;
            j_end = colNum - 1;
        }

        return getSubMatrix(input, i_start, i_end, j_start, j_end);
    }
    
          /**
     * Returns matrices Q, R such that Q*R = A
     */
    public static MatrixD[] givensQR(MatrixD A, Ring ring)  
  {
        int colCounter = 1;
        int n = A.rowNum();
        MatrixD Q = MatrixD.ONE(n, ring);
        MatrixD R = A.copy();
        MatrixD GTemp;

        for (int i=0; i<n-1; i++) {
//            System.out.println("ИТЕРАЦИЯ " + i  + "\n");
            for (int j=n-1; j>colCounter-1; j--) {
                if (Math.abs(R.getElement(j, i).doubleValue()) > 0) {
//                    System.out.println("ОБНУЛЯЕМ ЭЛЕМЕНТ " + j + ", " + i + "\n");
                    GTemp = getGivensRotationMatrix(n, j-1, j, R.getElement(j-1, i), R.getElement(j, i), ring);
//                    System.out.println("МАТРИЦА ВРАЩЕНИЯ = " + "\n");
//                    System.out.println(GTemp.toString()+ "\n");
                    Q = rightMultiplyMatrixToGivens(Q, GTemp, j-1, j, ring);
                    R = leftMultiplyGivensToMatrix(GTemp.transpose(ring), R, j-1, j, ring);
//                    System.out.println("МАТРИЦА ВРАЩЕНИЯ t * Temp = " + "\n");
//                    System.out.println(R.toString() + "\n");
                }
            }
            colCounter++;
        }

        return new MatrixD[]{Q, R};
    }     
    
    /**
     * Рекурсивная процедура для обнуления внутреннего параллелограмма
     * Matrix A (2n * n)
     */
    protected static MatrixD[] blockQrForParallelogram(MatrixD A, Ring ring)  {
        int rowNum = A.rowNum();
        int colNum = A.colNum();
        if (!(rowNum == 2*colNum))
return null;

        if (rowNum == 4) {
            return blockQrForBasicMatrix(A, ring);
        } else {
            MatrixD input = A.copy();
            int n = rowNum;

            // Block 1
            MatrixD block_1 = getBlock(input, 1);
            MatrixD[] Q1R1 = blockQrForParallelogram(block_1, ring);
            MatrixD Q1 = Q1R1[0];
            MatrixD R1 = Q1R1[1];
            MatrixD aff_b1 = getBlock(input, 4);
            aff_b1 = Q1.transpose(ring).multiplyMatr(aff_b1, ring);
            input = insertMatrixToMatrix(input, R1, input.rowNum() / 4, 0);
            input = insertMatrixToMatrix(input, aff_b1, input.rowNum() / 4, input.colNum()/2);
            //System.out.println(input.toString() + "\n");

            // Block 2 & Block 3
            MatrixD block_2 = getBlock(input, 2);
            MatrixD block_3 = getBlock(input, 3);
            MatrixD[] Q2R2 = new MatrixD[2];
            MatrixD[] Q3R3 = new MatrixD[2];
            Thread[] threads = new Thread[2];
            Thread blockExecuter_2 = new BlockExecuter(block_2, Q2R2, ring);
            threads[0] = blockExecuter_2;
            Thread blockExecuter_3 = new BlockExecuter(block_3, Q3R3, ring);
            threads[1] = blockExecuter_3;
            for (Thread t : threads) {
                t.start();
            }
            for (Thread t : threads) {
                try {
                    t.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            counter.addAndGet(2);
            MatrixD Q2 = Q2R2[0];
            MatrixD R2 = Q2R2[1];
            MatrixD aff_b2 = getSubMatrix(input, 0, (input.rowNum() / 2) - 1, input.colNum() / 2, input.colNum() - 1);
            aff_b2 = Q2.transpose(ring).multiplyMatr(aff_b2, ring);
            input = insertMatrixToMatrix(input, R2, 0, 0);
            input = insertMatrixToMatrix(input, aff_b2, 0, input.colNum() / 2);
            MatrixD Q3 = Q3R3[0];
            MatrixD R3 = Q3R3[1];
            input = insertMatrixToMatrix(input, R3, input.rowNum() / 2, input.colNum() / 2);
            //System.out.println(input.toString() + "\n");

            // Block 4
            MatrixD block_4 = getBlock(input, 4);
            MatrixD[] Q4R4 = blockQrForParallelogram(block_4, ring);
            MatrixD Q4 = Q4R4[0];
            MatrixD R4 = Q4R4[1];
            input = insertMatrixToMatrix(input, R4, input.rowNum() / 4, input.colNum() / 2);
            //System.out.println(input.toString() + "\n");

            MatrixD ones = MatrixD.ONE(n, ring);

            MatrixD Q_ru = insertMatrixToMatrix(ones, Q4, n / 4, n / 4);
            //System.out.println("Q_ru = \n" + Q_ru.toString() + "\n");
            MatrixD Q_lu_Q_rd = insertMatrixToMatrix(ones, Q2, 0, 0);
            Q_lu_Q_rd = insertMatrixToMatrix(Q_lu_Q_rd, Q3, n/2, n/2);
            //System.out.println("Q_lu_Q_rd = \n" + Q_lu_Q_rd.toString() + "\n");
            MatrixD Q_ld = insertMatrixToMatrix(ones, Q1, n / 4, n / 4);
            //System.out.println("Q_ld = \n" + Q_ld.toString() + "\n");

            MatrixD Q = Q_ld.multiplyMatr(Q_lu_Q_rd, ring).multiplyMatr(Q_ru, ring);
            //System.out.println(Q.multiplyMatr(input, ring).toString());

            return new MatrixD[] {Q, input};
        }
    }

    /**
     * Обнуление параллелограмма внутри матрицы размера 4 * 2
     */
    private static MatrixD[] blockQrForBasicMatrix(MatrixD A, Ring ring)  {
        int rowNum = A.rowNum();
        int colNum = A.colNum();
        if (!(rowNum == 4 && colNum == 2))
return null;

        MatrixD M2 = A.copy();
        MatrixD Q;

        // 1. Q_ld
        MatrixD Q_ld = getGivensRotationMatrix(rowNum, 1, 2, M2.getElement(1, 0), M2.getElement(2, 0), ring);
        //System.out.println("Q_ld = \n" + Q_ld.toString() + "\n");
        M2 = Q_ld.transpose(ring).multiplyMatr(M2, ring);
        //System.out.println("M2 = \n" + M2.toString() + "\n");

        // 2. Q_lu
        MatrixD Q_lu = getGivensRotationMatrix(rowNum, 0, 1, M2.getElement(0, 0), M2.getElement(1, 0), ring);
        //System.out.println("Q_lu = \n" + Q_lu.toString() + "\n");
        M2 = Q_lu.transpose(ring).multiplyMatr(M2, ring);
        //System.out.println("M2 = \n" + M2.toString() + "\n");

        // 3. Q_rd
        MatrixD Q_rd = getGivensRotationMatrix(rowNum, 2, 3, M2.getElement(2, 1), M2.getElement(3, 1), ring);
        //System.out.println("Q_rd = \n" + Q_rd.toString() + "\n");
        M2 = Q_rd.transpose(ring).multiplyMatr(M2, ring);
        //System.out.println("M2 = \n" + M2.toString() + "\n");

        // 4. Q_ru
        MatrixD Q_ru = getGivensRotationMatrix(rowNum, 1, 2, M2.getElement(1, 1), M2.getElement(2, 1), ring);
        //System.out.println("Q_ru = \n" + Q_ru.toString() + "\n");
        M2 = Q_ru.transpose(ring).multiplyMatr(M2, ring);

        Q = Q_ld.multiplyMatr(Q_lu, ring).multiplyMatr(Q_rd, ring).multiplyMatr(Q_ru, ring);

        //System.out.println("M2 = \n" + M2.toString() + "\n");
        //System.out.println("Q * M2 = \n" + Q.multiplyMatr(M2, ring).toString());

        return new MatrixD[] {Q, M2};
    }

}

class BlockExecuter extends Thread {
    MatrixD input;
    MatrixD[] output;
    Ring ring;

    public BlockExecuter(MatrixD input, MatrixD[] output, Ring ring) {
        this.input = input;
        this.output = output;
        this.ring = ring;
    }

      public static MatrixD getGivensRotationMatrix(int n, int i, int j, Element a, Element b, Ring ring) {
        MatrixD G = MatrixD.ONE(n, ring);
        if (b.isZero(ring)) {
            G.M[i][i] = ring.numberONE;
            G.M[i][j] = ring.numberZERO;
            G.M[j][i] = ring.numberZERO;
            G.M[j][j] = ring.numberONE;
        } else {
            Element r = a.pow(2, ring).add(b.pow(2, ring), ring).sqrt(ring);     // Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
            Element c = a.divide(r, ring);                                            // a/r;
            Element s = b.negate(ring).divide(r, ring);                               // (-b)/r;
            if (c.isInfinite() || s.isInfinite()) {
                G.M[i][i] = ring.numberONE;
                G.M[i][j] = ring.numberZERO;
                G.M[j][i] = ring.numberZERO;
                G.M[j][j] = ring.numberONE;
            } else {
                G.M[i][i] = c;
                G.M[i][j] = s;
                G.M[j][i] = s.negate(ring);
                G.M[j][j] = c;
            }
        }
        return G;
    }

    public static boolean checkSecondDiagonalValues(MatrixD temp, int n, Ring ring) {
        for (int i = 0; i < (n-1); i++) {
            if (!temp.getElement(i, i+1).isZero(ring) || !temp.getElement(i+1, i).isZero(ring))
                return false;
        }
        return true;
    }


    public static MatrixD getTriangleMatrixNumber64(int n, int mod, Ring ring) {
        MatrixD L = new MatrixD(n, n, mod, ring);
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                if (!(j<=i))
                    L.M[i][j] = ring.numberZERO;
        return L;
    }

    public static MatrixD[] getTwoGivensRotationMatrices(double a, double b, double d, int n, int i, int j, Ring ring) {
        MatrixD left = MatrixD.ONE(n, ring);
        MatrixD right = MatrixD.ONE(n, ring);
        double c = 0d;
        double s = 0d;
        double C = 0d;
        double S = 0d;
        double t = 0d;
        double T = 0d;

        if (a != 0d && d != 0d) {
//            System.out.println("a ≠ 0, d ≠ 0 \n");
            t = ((-1d*(b*b+d*d-a*a)) + Math.sqrt(Math.pow((b*b+d*d-a*a), 2) + 4d*a*a*b*b)) / (2d*a*b);
            T = (-1d/d)*(a*t + b);
        } else if (a != 0d && d == 0d) {
//            System.out.println("a ≠ 0, d = 0 \n");
            T = 0d;
            t = (-b)/a;
        } else if (a == 0d && d != 0d) {
//            System.out.println("a = 0, d ≠ 0 \n");
            t = 0d;
            T = (-b)/d;
        } else {                                    // a = 0 & d = 0
//            System.out.println("a = 0, d = 0 \n");
            c = 1d;
            s = 0d;
            C = 0d;
            S = 1d;
        }

        if (!(a == 0d && d == 0d)) {
            c = Math.sqrt(1d/(1d+t*t));                                 // c = Math.cos(Math.atan(t));
            s = t*c;                                                    // s = Math.sin(Math.atan(t));
            C = Math.sqrt(1d/(1d+T*T));                                 // C = Math.cos(Math.atan(T));
            S = T*C;                                                    // S = Math.sin(Math.atan(T));
        }

        left.M[i][i] = new NumberR64(c);
        left.M[i][j] = new NumberR64(-s);
        left.M[j][i] = new NumberR64(s);
        left.M[j][j] = new NumberR64(c);

        right.M[i][i] = new NumberR64(C);
        right.M[i][j] = new NumberR64(-S);
        right.M[j][i] = new NumberR64(S);
        right.M[j][j] = new NumberR64(C);

        return new MatrixD[]{left, right};
//            long [][] arr = {{1, 0}, {5, 2}};
//            MatrixD test = new MatrixD(arr, ring);
//            MatrixD[] lr = getTwoGivensRotationMatrices(test.getElement(0,0).doubleValue(), test.getElement(1, 0).doubleValue(),
//                    test.getElement(1,1).doubleValue(), 2, 0, 1, ring);
//            System.out.println("L * A * R = \n");
//            System.out.println(lr[0].multiplyMatr(test, ring).multiplyMatr(lr[1], ring).toString());
    }

    public static void removeNonDiagonalValues(MatrixD d, Ring ring) {
        for (int i = 0; i < d.M.length; i++) {
            for (int j = 0; j < d.M[0].length; j++) {
                if (i != j) {
                    d.M[i][j] = ring.numberZERO;
                }
            }
        }
    }

    public static boolean isPowerOfTwo(int number) {
        return number > 0 && ((number & (number - 1)) == 0);
    }

    public static MatrixD getSubMatrix(MatrixD matrix, int start_i, int end_i, int start_j, int end_j) {
        matrix = matrix.copy();
        int rowNum = end_i - start_i + 1;
        int colNum = end_j - start_j + 1;

        Element[][] e = new Element[rowNum][colNum];
        for (int i = start_i; i <= end_i; i++) {
            for (int j = start_j; j <= end_j; j++) {
                e[i-start_i][j-start_j] = matrix.getElement(i, j);
            }
        }

        return new MatrixD(e, 0);
    }



    public static void readBlock(MatrixD matrix, int iOffset, int jOffset, Element[][] elements, int h) {
        for (int i = iOffset; i < (h+iOffset); i++) {
            for (int j = jOffset; j < (h+jOffset); j++) {
                elements[i-iOffset][j-jOffset] = matrix.getElement(i, j);
            }
        }
    }





    // В результате у матрицы B поменяется только ряд x и ряд y
    public static MatrixD leftMultiplyGivensToMatrix(MatrixD A, MatrixD B, int x, int y, Ring ring) {
        MatrixD result = B.copy();
        Element zero = ring.numberZERO;
        Element res;

        int k = B.colNum();
        int n = B.rowNum();

        VectorS newXrow = new VectorS(k);
        VectorS newYrow = new VectorS(k);
        VectorS xRowFromA = A.takeRow(x+1);
        VectorS yRowFromA = A.takeRow(y+1);

        for (int i = 0; i < k; i++) {
            VectorS iColFromB = B.takeColumn(i+1);
            // замена рядка x
            res = zero;
            for (int j = 0; j < n; j++) {
                res = res.add((xRowFromA.V[j]).multiply(iColFromB.V[j], ring), ring);
            }
            newXrow.V[i] = res;
            // замена рядка y
            res = zero;
            for (int j = 0; j < n; j++) {
                res = res.add((yRowFromA.V[j]).multiply(iColFromB.V[j], ring), ring);
            }
            newYrow.V[i] = res;
        }

        for (int h = 0; h < k; h++) {
            result.M[x][h] = newXrow.V[h];
            result.M[y][h] = newYrow.V[h];
        }

        return result;
    }

    // В результате у матрицы A поменяется только столбец x и столбец y
    public static MatrixD rightMultiplyMatrixToGivens(MatrixD A, MatrixD B, int x, int y, Ring ring) {
        MatrixD result = A.copy();
        Element zero = ring.numberZERO;
        Element res;

        int n = A.rowNum();

        VectorS newXcol = new VectorS(n);
        VectorS newYcol = new VectorS(n);
        VectorS xColFromB = B.takeColumn(x+1);
        VectorS yColFromB = B.takeColumn(y+1);

        for (int i = 0; i < n; i++) {
            VectorS iRowFromA = A.takeRow(i+1);
            // замена столбца x
            res = zero;
            for (int j = 0; j < n; j++) {
                res = res.add(iRowFromA.V[j].multiply(xColFromB.V[j], ring), ring);
            }
            newXcol.V[i] = res;
            // замена столбца y
            res = zero;
            for (int j = 0; j < n; j++) {
                res = res.add(iRowFromA.V[j].multiply(yColFromB.V[j], ring), ring);
            }
            newYcol.V[i] = res;
        }

        for (int h = 0; h < n; h++) {
            result.M[h][x] = newXcol.V[h];
            result.M[h][y] = newYcol.V[h];
        }

        return result;
    }
  
    
    @Override
    public void run() {
 
            MatrixD[] qr = blockQR.blockQrForParallelogram(input, ring);
            output[0] = qr[0];
            output[1] = qr[1];
 
        }
    }
