package com.mathpar.number;

public class ElementFactory {
    public static Element one(Ring ring) {
        return Ring.oneOfType(ring.algebra[0]);
    }
}
