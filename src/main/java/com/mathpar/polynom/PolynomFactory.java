package com.mathpar.polynom;

import com.mathpar.number.Element;
import com.mathpar.number.ElementFactory;
import com.mathpar.number.Ring;

public class PolynomFactory {
    public static Polynom one(Ring ring) {
        return new Polynom(new int[0], new Element[] {ElementFactory.one(ring)});
    }
}
