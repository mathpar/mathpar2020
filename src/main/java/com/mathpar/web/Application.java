/**
* Copyright © 2011 Mathparca Ltd. All rights reserved.
*/

package com.mathpar.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.jdbc.core.JdbcTemplate;

@Configuration
@SpringBootApplication
@ComponentScan(
        basePackages = {"com.mathpar.web"},
        excludeFilters = {
                @ComponentScan.Filter(
                        value = {ApplicationPlayground.class},
                        type = FilterType.ASSIGNABLE_TYPE)
        })
public class Application extends SpringBootServletInitializer {
    private static final Logger LOG = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) throws Exception {
        SpringApplication sa = new SpringApplication(Application.class);
        ConfigurableApplicationContext ctx = sa.run(args);
        final JdbcTemplate jdbcTpl = ctx.getBean(JdbcTemplate.class);
        final String h2Version = jdbcTpl.queryForObject("SELECT H2VERSION ( );", String.class);
        LOG.info("==============================================");
        LOG.info("H2 version: {}", h2Version);
        LOG.info("==============================================");
    }

    // This is to run spring boot inside Tomcat container (not using embedded one)
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(Application.class);
    }
}
