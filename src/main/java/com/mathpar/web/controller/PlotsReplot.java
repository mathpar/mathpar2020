package com.mathpar.web.controller;

import com.mathpar.func.Page;
import com.mathpar.web.entity.Replot3DRequest;
import com.mathpar.web.executor.ReplotGeomtry3dTimeoutRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessageType;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Controller
public class PlotsReplot {

    private final SimpMessagingTemplate webSocketTemplate;

    @Autowired
    public PlotsReplot(SimpMessagingTemplate webSocketTemplate) {
        this.webSocketTemplate = webSocketTemplate;
    }

    @MessageMapping("/replot")
    public void send(@Header("simpSessionId") String sessionId,
                     @Header("simpSessionAttributes") Map<String, Object> sessionAttributes,
                     @Payload Replot3DRequest replotReq) throws Exception {
        SimpMessageHeaderAccessor headerAccessor = SimpMessageHeaderAccessor.create(SimpMessageType.MESSAGE);
        headerAccessor.setSessionId(sessionId);
        headerAccessor.setLeaveMutable(true);

        Page page = (Page) sessionAttributes.get("page");

        List<Double> currentFrameParams = new ArrayList<>(Collections.nCopies(replotReq.getParameters().size(), 1.0));
        List<Double> paramsStepsPerFrame = replotReq.getParameters().stream().map(
                (param) -> (1 - param) / replotReq.getFramesNumber()
        ).toList();


        for (int i = 0; i < replotReq.getFramesNumber(); i++) {
            for (int j = 0; j < currentFrameParams.size(); j++) {
                double newFrameParam = currentFrameParams.get(j) - paramsStepsPerFrame.get(j);
                currentFrameParams.set(j, newFrameParam);
            }

            String task = createReplotTask(currentFrameParams, replotReq.getSettings());

            final Object geometry = new ReplotGeomtry3dTimeoutRunner()
                    .run(page, task, replotReq.getSectionId());
            if (geometry != null) {
                webSocketTemplate.convertAndSendToUser(sessionId, "/queue/geometry",
                        geometry, headerAccessor.getMessageHeaders());
            }
        }
    }

    /*
     *  Format numbers to prevent scientific notation for small values
     */
    private static String createReplotTask(List<Double> currentFrameParams, String replotSettings) {
        StringBuilder task = new StringBuilder();
        task.append("\\replot([");
        if (!currentFrameParams.isEmpty()) {
            task.append(String.format("%.16f", currentFrameParams.get(0)));
        }
        for (int j = 1; j < currentFrameParams.size(); j++) {
            task.append(",");
            task.append(String.format("%.16f", currentFrameParams.get(j)));
        }
        task.append("], ");
        task.append(replotSettings);
        task.append(");");
        return task.toString();
    }
}
