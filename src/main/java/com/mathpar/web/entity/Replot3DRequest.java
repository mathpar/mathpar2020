package com.mathpar.web.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class Replot3DRequest {
    private int sectionId;
    private int framesNumber;
    private List<Double> parameters;
    private String settings;
}
