/**
* Copyright © 2011 Mathparca Ltd. All rights reserved.
*/

package com.mathpar.web.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SpaceMemoryResponse implements IMathparResponse {
    private String space;
    private String memory;
    private double[] paramsValues;
    private int framesNumber;
}
