/**
 * Copyright © 2011 Mathparca Ltd. All rights reserved.
 */

package com.mathpar.web.executor;

import com.mathpar.func.F;
import com.mathpar.func.Fname;
import com.mathpar.func.Page;
import com.mathpar.number.Element;
import com.mathpar.number.Ring;
import com.mathpar.web.entity.Plots3DCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;

public class ReplotGeometry3dCallable implements Callable<Object> {
    private static final Logger LOG = LoggerFactory.getLogger(ReplotGeometry3dCallable.class);

    private final Page page;
    private final String task;
    private final int sectionId;

    public ReplotGeometry3dCallable(Page page, String task, int sectionId) {
        this.page = page;
        this.task = task;
        this.sectionId = sectionId;
    }

    @Override
    public Object call() throws Exception {
        String ignore = page.execution(task, sectionId);
        // TODO: better function detection - it could be not the last.
        final Element expr = page.expr.get(page.expr.size() - 1);
        if (expr instanceof Fname) {
            Fname exprFname = ((Fname) expr);
            Element[] exprArgs = exprFname.X;
            String funcName = exprFname.name;
            if (exprArgs.length == 1 && exprArgs[0] instanceof F) {
                final F func = (F) exprArgs[0];
                Object result = Collections.emptyList();

                Ring pageRing = page.ring();
                page.change3dPlotForReplot = false;
                try {
                    switch (func.name) {
                        case F.IMPLICIT_PLOT3D:
                            result = getDataForImplicitPlot(funcName + " = " + func);
                            break;
                        case F.EXPLICIT_PLOT3D:
                            result = getDataForExplicitPlot(funcName + " = " + func);
                            break;
                        case F.PARAMETRIC_PLOT3D:
                            result = getDataForParametricPlot(
                                    "SPACE = " + Ring.ringR64xyzt + "; " + funcName + " = " + func
                            );
                            break;
                        case F.SHOW_PLOTS_3D:
                            result = getDataForShowPlots3D(
                                    "SPACE = " + Ring.ringR64xyzt + "; " + funcName + " = " + func
                            );
                        default:
                            break;
                    }
                } finally {
                    page.setRing(pageRing);
                    page.change3dPlotForReplot = true;
                }

                return result;
            }
        }
        LOG.warn("Can't get parameters for {} function. Return null.", F.FUNC_NAMES[F.REPLOT]);
        return null;
    }

    private List<double[]> getDataForImplicitPlot(String task) {
        return new ImplicitPlot3dTimeoutRunner().run(this.page, task, this.sectionId);
    }

    private List<double[]> getDataForExplicitPlot(String task) {
        return new ExplicitPlot3dTimeoutRunner().run(this.page, task, this.sectionId);
    }

    private List<double[]> getDataForParametricPlot(String task) {
        return new ParametricPlot3dTimeoutRunner().run(this.page, task, this.sectionId);
    }

    private Plots3DCollection getDataForShowPlots3D(String task) {
        return new PlotCollection3DTimeoutRunner().run(this.page, task, this.sectionId);
    }
}
