<#assign title = "Math partner">
<#assign nav_active = _("navbar.about")>
<#assign change_lang_link = "../" + _("navbar.changelang_locale") + "/contact.html">
<#assign ru_link = "../ru/contact.html">
<#assign en_link = "../en/contact.html">
<#assign he_link = "../iw/contact.html">
<#assign ua_link = "../ua/contact.html">
<#assign am_link = "../am/contact.html">
<#include "common/header.ftl">
<#include "common/top_navbar.ftl">

<div class="container">
  <div class="row">
    <div class="well jumbotron col-md-8 col-md-offset-2">
      <h3>${_("contact.title")}</h3>
      <p> Version 12.02 from 02/02/2022  </p>
     <p> &copy; Mathparca  2011 </p>
      <p><b>Email:</b> <a href="mailto:malaschonok@ukma.edu.ua">malaschonok@ukma.edu.ua</a></p>
    </div>
  </div>
  <div class="row">
    <div class="well col-md-8 col-md-offset-2">
      <h3>${_("contact.thanks")}</h3>
      ${_("contact.grants")}
       If you use MathPartner, then please refer to the article: <p> 
       G.I.Malaschonok. MathPartner Computer Algebra.  Programming and Computer Software  (2017) 43, N.2, 112–118. <p> 
      (See also arxiv.org/abs/2204.11549,  arxiv.org/abs/2204.11061, arxiv.org/abs/2204.11118 ) <p>      
    </div>
  </div>
</div>

<script src="${path_prefix}js/libs/require.js"></script>
<script>require({baseUrl: '${path_prefix}js/'},['libs/jquery', 'libs/bootstrap']);</script>

<#include "common/footer.ftl">
