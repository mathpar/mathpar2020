var objectsGlobal = [], objectId = 0, distanceToPointLimit = 10;
var points = {
    'yp': [],
    'ym': []
}
function comparePoint( a, b ) {
    if ( a.x < b.x ){
        return -1;
    } else {
        return 1;
    }
}
function intersectAllFaces(scene) {
    var intersectPoints = [];
    for(var firstCounter = 0; firstCounter < objectsGlobal.length - 1; firstCounter++) {
        for(var secondCounter = firstCounter + 1; secondCounter < objectsGlobal.length; secondCounter++) {
            var intersectPoints1 = intersectFaces(objectsGlobal[firstCounter], objectsGlobal[secondCounter]);
            var intersectPoints2 = intersectFaces(objectsGlobal[secondCounter], objectsGlobal[firstCounter]);
            var intersectPoints = intersectPoints1.concat(intersectPoints2);
            if (intersectPoints.length < 2) {
                console.warn("ERROR DURING RAY TRACING");
            } else {
                var material = new THREE.LineBasicMaterial({
                    color: 0x00ff00,
                    linewidth: 10
                });
                var geometry = new THREE.Geometry();
                for(verticeId in intersectPoints) {
                    var vertice = intersectPoints[verticeId];
                    if (vertice.y < 0) {
                        points.yp.push(new THREE.Vector3(vertice.x, -vertice.y, vertice.z));
                    } else {
                        points.yp.push(new THREE.Vector3(vertice.x, vertice.y, vertice.z));
                    }
                }
                points.yp.sort(comparePoint);
                for(verticeId in points.yp) {
                    var vertice = points.yp[verticeId];
                    geometry.vertices.push(vertice);
                }

                var line = new THREE.Line(geometry, material);
                scene.add( line );

                var geometryM = new THREE.Geometry();
                for(verticeId in points.yp) {
                    var vertice = points.yp[verticeId];
                    geometryM.vertices.push(new THREE.Vector3(vertice.x, -vertice.y, vertice.z));
                }

                var lineM = new THREE.Line(geometryM, material);
                scene.add( lineM );
                //console.log(points);
/*
                var material = new THREE.LineBasicMaterial({
                    color: 0x00ff00
                });
                var geometry = new THREE.Geometry();
                for(verticeId in intersectPoints) {
                    var vertice = intersectPoints[verticeId];
                    geometry.vertices.push(new THREE.Vector3(vertice.x, vertice.y, vertice.z));
                }

                var line = new THREE.Line(geometry, material);
                scene.add( line );*/
            }
        }
    }
    return intersectPoints;
}


function intersectFaces(object1Faces, object2Faces) {
    var intersectPoints = [];

    for(object1FaceId in object1Faces) {
        var object1Face = object1Faces[object1FaceId];

        var v1 = new THREE.Vector3(object1Face[0].x, object1Face[0].y, object1Face[0].z);
        var v2 = new THREE.Vector3(object1Face[1].x, object1Face[1].y, object1Face[1].z);
        var v3 = new THREE.Vector3(object1Face[2].x, object1Face[2].y, object1Face[2].z);

        var ray01 = new THREE.Ray(v1, new THREE.Vector3().subVectors(v2, v1).normalize());
        var ray02 = new THREE.Ray(v1, new THREE.Vector3().subVectors(v3, v1).normalize());
        var ray12 = new THREE.Ray(v2, new THREE.Vector3().subVectors(v3, v2).normalize());

        for(object2FaceId in object2Faces) {
            var object2Face = object2Faces[object2FaceId];

            var t1 = new THREE.Vector3(object2Face[0].x, object2Face[0].y, object2Face[0].z);
            var t2 = new THREE.Vector3(object2Face[1].x, object2Face[1].y, object2Face[1].z);
            var t3 = new THREE.Vector3(object2Face[2].x, object2Face[2].y, object2Face[2].z);

            if (doTrianglesIntersect(new THREE.Triangle(t1, t2, t3), new THREE.Triangle(v1, v2, v3))) {
                var intersect = new THREE.Vector3();

                ray01.intersectTriangle(t1, t2, t3, true, intersect);
                if (intersect.x !== 0 && intersect.y !== 0 && intersect.z !== 0) {
                    var dp = ray01.distanceToPoint(intersect);
                    if (dp != 0/* && dp > distanceToPointLimit*/)
                        intersectPoints.push(intersect.clone());
                }

                intersect = new THREE.Vector3();
                ray02.intersectTriangle(t1, t2, t3, true, intersect);
                if (intersect.x !== 0 && intersect.y !== 0 && intersect.z !== 0) {
                    var dp = ray02.distanceToPoint(intersect);
                    if (dp != 0/* && dp > distanceToPointLimit*/)
                        intersectPoints.push(intersect.clone());
                }

                intersect = new THREE.Vector3();
                ray12.intersectTriangle(t1, t2, t3, true, intersect);
                if (intersect.x !== 0 && intersect.y !== 0 && intersect.z !== 0) {
                    var dp = ray12.distanceToPoint(intersect);
                    if (dp != 0/* && dp > distanceToPointLimit*/)
                        intersectPoints.push(intersect.clone());
                }
            }
        }
    }
    return intersectPoints === [] ? false : intersectPoints;
}

function doTrianglesIntersect(t1, t2) {

    /*
    Adapated from section "4.1 Separation of Triangles" of:

     - [Dynamic Collision Detection using Oriented Bounding Boxes](https://www.geometrictools.com/Documentation/DynamicCollisionDetection.pdf)
    */


    // Triangle 1:

    var A0 = t1.a;
    var A1 = t1.b;
    var A2 = t1.c;

    var E0 = A1.clone().sub(A0);
    var E1 = A2.clone().sub(A0);

    var E2 = E1.clone().sub(E0);

    var N = E0.clone().cross(E1);


    // Triangle 2:

    var B0 = t2.a;
    var B1 = t2.b;
    var B2 = t2.c;

    var F0 = B1.clone().sub(B0);
    var F1 = B2.clone().sub(B0);

    var F2 = F1.clone().sub(F0);

    var M = F0.clone().cross(F1);


    var D = B0.clone().sub(A0);


    function areProjectionsSeparated(p0, p1, p2, q0, q1, q2) {
        var min_p = Math.min(p0, p1, p2),
            max_p = Math.max(p0, p1, p2),
            min_q = Math.min(q0, q1, q2),
            max_q = Math.max(q0, q1, q2);

        return ((min_p > max_q) || (max_p < min_q));
    }


    // Only potential separating axes for non-parallel and non-coplanar triangles are tested.


    // Seperating axis: N

    {
        var p0 = 0,
            p1 = 0,
            p2 = 0,
            q0 = N.dot(D),
            q1 = q0 + N.dot(F0),
            q2 = q0 + N.dot(F1);

        if (areProjectionsSeparated(p0, p1, p2, q0, q1, q2))
            return false;
    }


    // Separating axis: M

    {
        var p0 = 0,
            p1 = M.dot(E0),
            p2 = M.dot(E1),
            q0 = M.dot(D),
            q1 = q0,
            q2 = q0;

        if (areProjectionsSeparated(p0, p1, p2, q0, q1, q2))
            return false;
    }


    // Seperating axis: E0 × F0

    {
        var p0 = 0,
            p1 = 0,
            p2 = -(N.dot(F0)),
            q0 = E0.clone().cross(F0).dot(D),
            q1 = q0,
            q2 = q0 + M.dot(E0);

        if (areProjectionsSeparated(p0, p1, p2, q0, q1, q2))
            return false;
    }


    // Seperating axis: E0 × F1

    {
        var p0 = 0,
            p1 = 0,
            p2 = -(N.dot(F1)),
            q0 = E0.clone().cross(F1).dot(D),
            q1 = q0 - M.dot(E0),
            q2 = q0;

        if (areProjectionsSeparated(p0, p1, p2, q0, q1, q2))
            return false;
    }


    // Seperating axis: E0 × F2

    {
        var p0 = 0,
            p1 = 0,
            p2 = -(N.dot(F2)),
            q0 = E0.clone().cross(F2).dot(D),
            q1 = q0 - M.dot(E0),
            q2 = q1;

        if (areProjectionsSeparated(p0, p1, p2, q0, q1, q2))
            return false;
    }


    // Seperating axis: E1 × F0

    {
        var p0 = 0,
            p1 = N.dot(F0),
            p2 = 0,
            q0 = E1.clone().cross(F0).dot(D),
            q1 = q0,
            q2 = q0 + M.dot(E1);

        if (areProjectionsSeparated(p0, p1, p2, q0, q1, q2))
            return false;
    }


    // Seperating axis: E1 × F1

    {
        var p0 = 0,
            p1 = N.dot(F1),
            p2 = 0,
            q0 = E1.clone().cross(F1).dot(D),
            q1 = q0 - M.dot(E1),
            q2 = q0;

        if (areProjectionsSeparated(p0, p1, p2, q0, q1, q2))
            return false;
    }


    // Seperating axis: E1 × F2

    {
        var p0 = 0,
            p1 = N.dot(F2),
            p2 = 0,
            q0 = E1.clone().cross(F2).dot(D),
            q1 = q0 - M.dot(E1),
            q2 = q1;

        if (areProjectionsSeparated(p0, p1, p2, q0, q1, q2))
            return false;
    }


    // Seperating axis: E2 × F0

    {
        var p0 = 0,
            p1 = N.dot(F0),
            p2 = p1,
            q0 = E2.clone().cross(F0).dot(D),
            q1 = q0,
            q2 = q0 + M.dot(E2);

        if (areProjectionsSeparated(p0, p1, p2, q0, q1, q2))
            return false;
    }


    // Seperating axis: E2 × F1

    {
        var p0 = 0,
            p1 = N.dot(F1),
            p2 = p1,
            q0 = E2.clone().cross(F1).dot(D),
            q1 = q0 - M.dot(E2),
            q2 = q0;

        if (areProjectionsSeparated(p0, p1, p2, q0, q1, q2))
            return false;
    }


    // Seperating axis: E2 × F2

    {
        var p0 = 0,
            p1 = N.dot(F2),
            p2 = p1,
            q0 = E2.clone().cross(F2).dot(D),
            q1 = q0 - M.dot(E2),
            q2 = q1;

        if (areProjectionsSeparated(p0, p1, p2, q0, q1, q2))
            return false;
    }


    return true;
}

define([
    'jquery', 'util', 'three', 'three-detector', 'three-canvas',
    'three-orbit-controls', 'three-window-resize', 'three-font-helvetiker'
], function ($, util, THREEjs, THREEjsDetector, THREEjsCanvas,
             THREEjsOrbitControls, THREEjsWindowResize, THREEjsFontHelvetiker) {
    'use strict';

    /**
     * @param sectionId
     * @param $imageBelow
     * @constructor
     */
    var Plot3dCollection = function (sectionId, $imageBelow, isIntersection) {
        'use strict';

        var this_ = this;

        this.sectionId = sectionId;
        this.$imageBelow = $imageBelow;
        this.$imageBelow.addClass('threed');

        this.$imageBelow.height(0.7 * this.$imageBelow.width());

        // SCENE
        this.scene = new THREE.Scene();
        // CAMERA
        this.SCREEN_WIDTH = this.$imageBelow.parent().width();
        this.SCREEN_HEIGHT = this.$imageBelow.parent().width() * 0.75;// this.$imageBelow.height();
        this.VIEW_ANGLE = 45;
        this.ASPECT = this.SCREEN_WIDTH / this.SCREEN_HEIGHT;
        this.NEAR = 0.1;
        this.FAR = 20000;
        this.camera = new THREE.PerspectiveCamera(this.VIEW_ANGLE, this.ASPECT, this.NEAR, this.FAR);
        this.scene.add(this.camera);
        this.camera.position.set(20, 20, 10);
        this.camera.up = new THREE.Vector3(0, 0, 1);
        this.camera.lookAt(this.scene.position);
        // RENDERER
        if (Detector.webgl) {
            this.renderer = new THREE.WebGLRenderer({antialias: true, alpha: true});
            console.log('webgl');
        } else {
            this.renderer = new THREE.CanvasRenderer({alpha: true});
            console.log('no webgl - canvas.');
        }
        this.renderer.setSize(this.SCREEN_WIDTH, this.SCREEN_HEIGHT);
        this.renderer.setClearColor(0x000000, 0);

        // EVENTS
        // TODO: do something with window resize.
        //THREEx.WindowResize(this.renderer, this.camera);

        // CONTROLS
        this.controls = new THREE.OrbitControls(this.camera, this.renderer.domElement);

        this.$canvas = $(this.renderer.domElement);
        this.$canvas.addClass('plot3d-implicit');
        this.$canvas.width(this.$imageBelow.width());
        this.$canvas.height(this.$imageBelow.height());
        this.$canvas.insertBefore(this.$imageBelow);

        this.isIntersection = isIntersection;

        // this.geometry = new  THREE.Geometry();
        this.implicitMeshes = [];
        this.explicitMeshes = [];
        this.parametricMeshes = [];

        console.log('start ajax');
        var task = $('#section_' + this.sectionId).find('textarea').val();
        if (task.indexOf('\\intersection3D') >= 0) {
            task = task.replace('\\intersection3D', '\\showPlots3D');
        }
        $.ajax({
            url: util.url('/api/showplots3d'),
            type: 'POST',
            contentType: 'application/json',
            dataType: 'json',
            data: JSON.stringify({
                sectionId: this.sectionId,
                task: task
            })
        }).done(function (geom) {
            objectsGlobal = [];
            objectId = 0;
            geom['implicitPlots'].forEach(function (e) {
                this_.drawImplicitPlot3D(e);
            });
            geom['explicitPlots'].forEach(function (e) {
                this_.drawExplicitPlot3D(e);
            });
            geom['parametricPlots'].forEach(function (e) {
                this_.drawPrametricPlot3D(e);
            });
            console.log('start: ' + Math.floor(Date.now() / 1000));
            var wins = 0;
            if (this_.isIntersection) {
                intersectAllFaces(this_.scene);
                /*
                var currentObject, comparableObject;
                for(var fi = 0; fi < objectsGlobal.length - 1; fi++) {
                    console.log("fi: " + fi);
                    currentObject = objectsGlobal[fi];
                    for(var fj = fi + 1; fj < objectsGlobal.length; fj++) {
                        console.log("fj: " + fj);
                        comparableObject = objectsGlobal[fj];
                        for (var i = 1; i < 6/*currentObject.length*//*; i++) {
                            console.log("i: " + i);
                            if ((i + 1) % 3 === 0) {

                                var p1 = new THREE.Vector3(currentObject[i - 2][0], currentObject[i - 2][1], currentObject[i - 2][2]);
                                var p2 = new THREE.Vector3(currentObject[i - 1][0], currentObject[i - 1][1], currentObject[i - 1][2]);
                                var p3 = new THREE.Vector3(currentObject[i][0], currentObject[i][1], currentObject[i][2]);

                                var r1 = new THREE.Ray(p1, new THREE.Vector3().subVectors(p2, p1).normalize());
                                var r2 = new THREE.Ray(p1, new THREE.Vector3().subVectors(p3, p1).normalize());
                                var r3 = new THREE.Ray(p2, new THREE.Vector3().subVectors(p1, p2).normalize());
                                var r4 = new THREE.Ray(p2, new THREE.Vector3().subVectors(p3, p2).normalize());
                                var r5 = new THREE.Ray(p3, new THREE.Vector3().subVectors(p1, p3).normalize());
                                var r6 = new THREE.Ray(p3, new THREE.Vector3().subVectors(p2, p3).normalize());

                                for (var j = 1; j < 6/*comparableObject.length*//*; j++) {

                                    console.log("j: " + j);
                                    if ((j + 1) % 3 === 0) {
                                        var v1 = new THREE.Vector3(comparableObject[j - 2][0], comparableObject[j - 2][1], comparableObject[j - 2][2]);
                                        var v2 = new THREE.Vector3(comparableObject[j - 1][0], comparableObject[j - 1][1], comparableObject[j - 1][2]);
                                        var v3 = new THREE.Vector3(comparableObject[j][0], comparableObject[j][1], comparableObject[j][2]);

                                        var intersect = new THREE.Vector3();
                                        intersect = r1.intersectTriangle(v1, v2, v3, true, intersect);
                                        if (intersect) {
                                            console.log("intersect 1: " + intersect);
                                            wins++;
                                        }
                                        intersect = r2.intersectTriangle(v1, v2, v3, true, intersect);
                                        if (intersect) {
                                            console.log("intersect 2: " + intersect);
                                            wins++;
                                        }
                                        intersect = r3.intersectTriangle(v1, v2, v3, true, intersect);
                                        if (intersect) {
                                            console.log("intersect 3: " + intersect);
                                            wins++;
                                        }
                                        intersect = r4.intersectTriangle(v1, v2, v3, true, intersect);
                                        if (intersect) {
                                            console.log("intersect 4: " + intersect);
                                            wins++;
                                        }
                                        intersect = r5.intersectTriangle(v1, v2, v3, true, intersect);
                                        if (intersect) {
                                            console.log("intersect 5: " + intersect);
                                            wins++;
                                        }
                                        intersect = r6.intersectTriangle(v1, v2, v3, true, intersect);
                                        if (intersect) {
                                            console.log("intersect 6: " + intersect);
                                            wins++;
                                        }
                                        console.log('f');
                                    }
                                }
                            }
                        }
                    }
                }*/
            }
            console.log('end: ' + Math.floor(Date.now() / 1000));

            /*var material = new THREE.LineBasicMaterial( { color: 0x0000ff } );
            this_.geometry.vertices.push(new THREE.Vector3( -10, 0, 0) );
            this_.geometry.vertices.push(new THREE.Vector3( 0, 10, 0) );
            this_.geometry.vertices.push(new THREE.Vector3( 10, 0, 0) );

            var line = new THREE.Line( this_.geometry, material );

            scene.add( line );*/
            this_.render();
            this_.animate();
        }).error(function () {
            alert('Internal Server Error');
        });
    };

    Plot3dCollection.prototype.drawImplicitPlot3D = function(geom) {
        var this_ = this;
        var i,
            xMin = geom[0][0],
            xMax = geom[0][1],
            yMin = geom[0][2],
            yMax = geom[0][3],
            zMin = geom[0][4],
            zMax = geom[0][5],
            lightX = 10,
            lightY = 12,
            lightZ = 0,
            color = geom[0][9],
            axesSize = 1.2 * Math.max(xMax, yMax, zMax);
        this_.axisHelper = new THREE.AxisHelper(axesSize);
        this_.scene.add(this_.axisHelper);

        this_.xLabel = createAxisLabel('x', new THREE.Vector3(axesSize, 0, 0), this_.camera.rotation);
        this_.yLabel = createAxisLabel('y', new THREE.Vector3(0, axesSize, 0), this_.camera.rotation);
        this_.zLabel = createAxisLabel('z', new THREE.Vector3(0, 0, axesSize), this_.camera.rotation);
        this_.scene.add(this_.xLabel);
        this_.scene.add(this_.yLabel);
        this_.scene.add(this_.zLabel);

        // LIGHT
        this_.light = new THREE.PointLight(0xffffff, 1.2);
        this_.light.position.set(lightX, lightY, lightZ);
        this_.scene.add(this_.light);

        var geometry = new THREE.Geometry();
        objectsGlobal[objectId] = [];
        var facesCount = 0;
        for (i = 1; i < geom.length; i++) {
            geometry.vertices.push(toVector3(geom[i]));
            if ((i + 1) % 3 === 0) {
                geometry.faces.push(new THREE.Face3(i - 2, i - 1, i));
            }
            if ((i) % 3 === 0) {
                objectsGlobal[objectId].push([geometry.vertices[geometry.faces[facesCount].a], geometry.vertices[geometry.faces[facesCount].b], geometry.vertices[geometry.faces[facesCount].c]]);
                facesCount++;
            }
        }
        objectId++;
        geometry.computeFaceNormals();
        geometry.computeVertexNormals();

        this_.colorMaterial = new THREE.MeshLambertMaterial({
            color: '#' + Math.floor(Math.random()*16777215).toString(16),
            side: THREE.DoubleSide,
            shading: THREE.SmoothShading
        });

        let mesh = new THREE.Mesh(geometry, this_.colorMaterial);
        this_.scene.add(mesh);
        this_.implicitMeshes.push(mesh);
    };

    Plot3dCollection.prototype.drawExplicitPlot3D = function(geom) {
        var this_ = this;
        var
            xMin = geom[0][0],
            xMax = geom[0][1],
            yMin = geom[0][2],
            yMax = geom[0][3],
            zMin = geom[0][4],
            zMax = geom[0][5],
            stacks = geom[0][6],
            lightX = 10,
            lightY = 12,
            lightZ = 0,
            color = geom[0][10],
            axesSize = 1.2 * Math.max(xMax, yMax, zMax);
        this_.axisHelper = new THREE.AxisHelper(axesSize);
        this_.scene.add(this_.axisHelper);

        this_.xLabel = createAxisLabel('x', new THREE.Vector3(axesSize, 0, 0), this_.camera.rotation);
        this_.yLabel = createAxisLabel('y', new THREE.Vector3(0, axesSize, 0), this_.camera.rotation);
        this_.zLabel = createAxisLabel('z', new THREE.Vector3(0, 0, axesSize), this_.camera.rotation);
        this_.scene.add(this_.xLabel);
        this_.scene.add(this_.yLabel);
        this_.scene.add(this_.zLabel);

        // LIGHT
        this_.light = new THREE.PointLight(0xffffff, 1.2);
        this_.light.position.set(lightX, lightY, lightZ);
        this_.scene.add(this_.light);

        var geometry = new THREE.Geometry();
        for (var i = 1; i < geom.length; i++) {
            geometry.vertices.push(toVector3(geom[i]));
        }

        var a, b, c, d;
        var uva, uvb, uvc, uvd;
        var sliceCount = stacks + 1;
        objectsGlobal[objectId] = [];
        for (var i = 0; i < stacks; i ++ ) {

            for (var j = 0; j < stacks; j ++ ) {

                a = i * sliceCount + j;
                b = i * sliceCount + j + 1;
                c = ( i + 1 ) * sliceCount + j + 1;
                d = ( i + 1 ) * sliceCount + j;

                uva = new THREE.Vector2( j / stacks, i / stacks );
                uvb = new THREE.Vector2( ( j + 1 ) / stacks, i / stacks );
                uvc = new THREE.Vector2( ( j + 1 ) / stacks, ( i + 1 ) / stacks );
                uvd = new THREE.Vector2( j / stacks, ( i + 1 ) / stacks );

                geometry.faces.push( new THREE.Face3( a, b, d ) );
                objectsGlobal[objectId].push([geometry.vertices[a], geometry.vertices[b], geometry.vertices[d]]);
                // uvs.push( [ uva, uvb, uvd ] );

                geometry.faces.push( new THREE.Face3( b, c, d ) );
                objectsGlobal[objectId].push([geometry.vertices[b], geometry.vertices[c], geometry.vertices[d]]);
                // uvs.push( [ uvb.clone(), uvc, uvd.clone() ] );

            }

        }
        objectId++;

        geometry.computeFaceNormals();
        geometry.computeVertexNormals();

        this_.colorMaterial = new THREE.MeshLambertMaterial({
            color: '#' + Math.floor(Math.random()*16777215).toString(16),
            side: THREE.DoubleSide,
            shading: THREE.SmoothShading
        });

        let mesh = new THREE.Mesh(geometry, this_.colorMaterial);
        this_.scene.add(mesh);
        this_.explicitMeshes.push(mesh);
    };

    Plot3dCollection.prototype.drawPrametricPlot3D = function(geom) {
        var this_ = this;
        var
            xMin = geom[0][0],
            xMax = geom[0][1],
            yMin = geom[0][2],
            yMax = geom[0][3],
            zMin = geom[0][4],
            zMax = geom[0][5],
            stacks = geom[0][6],
            lightX = 10,
            lightY = 12,
            lightZ = 0,
            color = geom[0][10],
            axesSize = 1.2 * Math.max(xMax, yMax, zMax);
        this_.axisHelper = new THREE.AxisHelper(axesSize);
        this_.scene.add(this_.axisHelper);

        this_.xLabel = createAxisLabel('x', new THREE.Vector3(axesSize, 0, 0), this_.camera.rotation);
        this_.yLabel = createAxisLabel('y', new THREE.Vector3(0, axesSize, 0), this_.camera.rotation);
        this_.zLabel = createAxisLabel('z', new THREE.Vector3(0, 0, axesSize), this_.camera.rotation);
        this_.scene.add(this_.xLabel);
        this_.scene.add(this_.yLabel);
        this_.scene.add(this_.zLabel);

        // LIGHT
        this_.light = new THREE.PointLight(0xffffff, 1.2);
        this_.light.position.set(lightX, lightY, lightZ);
        this_.scene.add(this_.light);

        var geometry = new THREE.Geometry();
        for (var i = 1; i < geom.length; i++) {
            geometry.vertices.push(toVector3(geom[i]));
        }

        var a, b, c, d;
        var uva, uvb, uvc, uvd;
        var sliceCount = stacks + 1;
        objectsGlobal[objectId] = []
        for (var i = 0; i < stacks; i ++ ) {

            for (var j = 0; j < stacks; j ++ ) {

                a = i * sliceCount + j;
                b = i * sliceCount + j + 1;
                c = ( i + 1 ) * sliceCount + j + 1;
                d = ( i + 1 ) * sliceCount + j;

                uva = new THREE.Vector2( j / stacks, i / stacks );
                uvb = new THREE.Vector2( ( j + 1 ) / stacks, i / stacks );
                uvc = new THREE.Vector2( ( j + 1 ) / stacks, ( i + 1 ) / stacks );
                uvd = new THREE.Vector2( j / stacks, ( i + 1 ) / stacks );

                geometry.faces.push( new THREE.Face3( a, b, d ) );
                objectsGlobal[objectId].push([geometry.vertices[a], geometry.vertices[b], geometry.vertices[d]]);
                // uvs.push( [ uva, uvb, uvd ] );

                geometry.faces.push( new THREE.Face3( b, c, d ) );
                objectsGlobal[objectId].push([geometry.vertices[b], geometry.vertices[c], geometry.vertices[d]]);
                // uvs.push( [ uvb.clone(), uvc, uvd.clone() ] );

            }

        }
        objectId++;

        geometry.computeFaceNormals();
        geometry.computeVertexNormals();

        this_.colorMaterial = new THREE.MeshLambertMaterial({
            color: '#' + Math.floor(Math.random()*16777215).toString(16),
            side: THREE.DoubleSide,
            shading: THREE.SmoothShading
        });

        let mesh = new THREE.Mesh(geometry, this_.colorMaterial);
        this_.scene.add(mesh);
        this_.parametricMeshes.push(mesh);
    };

    function toVector3(vert) {
        return new THREE.Vector3(vert[0], vert[1], vert[2]);
    }

    /**
     *
     * @param {string} text text of label
     * @param {THREE.Vector3} position
     * @param {THREE.Euler} rotation
     * @returns {THREE.Mesh|*}
     */
    function createAxisLabel(text, position, rotation) {
        var textGeom, textMaterial, textMesh;
        textGeom = new THREE.TextGeometry(text, {
            size: 1,
            height: 0.2,
            curveSegments: 6,
            font: "helvetiker"
        });
        textMaterial = new THREE.MeshBasicMaterial({color: 0x999999});
        textMesh = new THREE.Mesh(textGeom, textMaterial);
        textMesh.position.x = position.x;
        textMesh.position.y = position.y;
        textMesh.position.z = position.z;
        textMesh.setRotationFromEuler(rotation);
        return textMesh;
    }

    Plot3dCollection.prototype.animate = function () {
        var this_ = this;
        requestAnimationFrame(function () {
            this_.animate();
        });
        this.render();
        this.update();
    };

    Plot3dCollection.prototype.update = function () {
        this.controls.update();
        this.xLabel.setRotationFromEuler(this.camera.rotation);
        this.yLabel.setRotationFromEuler(this.camera.rotation);
        this.zLabel.setRotationFromEuler(this.camera.rotation);
    };

    Plot3dCollection.prototype.render = function () {
        this.renderer.render(this.scene, this.camera);
    };

    Plot3dCollection.prototype.replot = function (geom) {
        let implicitGeometries = geom['implicitPlots'];
        for (let i = 0; i < implicitGeometries.length || i < this.implicitMeshes.length; i++) {
            let implicitGeom = implicitGeometries[i];
            let mesh = this.implicitMeshes[i];

            mesh.geometry.dispose();
            mesh.geometry = new THREE.Geometry();

            for (let i = 1; i < implicitGeom.length; i++) {
                mesh.geometry.vertices.push(toVector3(implicitGeom[i]));
                if ((i + 1) % 3 === 0) {
                    mesh.geometry.faces.push(new THREE.Face3(i - 2, i - 1, i));
                }
            }
            mesh.geometry.computeFaceNormals();
            mesh.geometry.computeVertexNormals();
        }

        let explicitGeometries = geom['explicitPlots'];
        for (let i = 0; i < explicitGeometries.length || i < this.explicitMeshes.length; i++) {
            let explicitGeom = explicitGeometries[i];
            let mesh = this.explicitMeshes[i];
            for (let i = 1; i < explicitGeom.length; i++) {
                mesh.geometry.vertices[i - 1] = toVector3(explicitGeom[i]);
            }
            mesh.geometry.verticesNeedUpdate = true;
        }

        let parametricGeometries = geom['parametricPlots'];
        for (let i = 0; i < parametricGeometries.length || i < this.parametricMeshes.length; i++) {
            let parametricGeom = parametricGeometries[i];
            let mesh = this.parametricMeshes[i];
            for (let i = 1; i < parametricGeom.length; i++) {
                mesh.geometry.vertices[i - 1] = toVector3(parametricGeom[i]);
            }
            mesh.geometry.verticesNeedUpdate = true;
        }
    };

    return Plot3dCollection;
});
