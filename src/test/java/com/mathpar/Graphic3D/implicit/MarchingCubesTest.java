/**
* Copyright © 2011 Mathparca Ltd. All rights reserved.
*/

package com.mathpar.Graphic3D.implicit;

import com.mathpar.func.F;
import com.mathpar.func.Page;
import com.mathpar.number.Ring;
import com.mathpar.web.executor.ImplicitPlot3dCallable;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class MarchingCubesTest {

    public MarchingCubesTest() {
    }

    @Test
    public void testSimpleMesh() throws Exception {
        // 3x3 cube.
        final Ring ring = Ring.ringR64xyzt;
        Page page=new Page(ring,true);ring.page=page;
        String fStr="x^2 + y^2 - z^2 - 25";
        final F f = new F(fStr, ring);

        List<double[]> expVertices = Arrays.asList(
                // границы по осям.
                new double[]{-10, 10, -10, 10, -10, 10},

                new double[]{-10, -2.5, -10},
                new double[]{-10, 0, -7.5},
                new double[]{-2.5, 0, 0},
                new double[]{-2.5, -10, -10},
                new double[]{-10, -2.5, -10},
                new double[]{-2.5, 0, 0},
                new double[]{-2.5, -10, -10},
                new double[]{-2.5, 0, 0},
                new double[]{0, -2.5, 0},
                new double[]{-2.5, -10, -10},
                new double[]{0, -2.5, 0},
                new double[]{0, -10, -7.5},
                new double[]{10, 0, -7.5},
                new double[]{0, -2.5, 0},
                new double[]{2.5, 0, 0},
                new double[]{10, -2.5, -10},
                new double[]{0, -2.5, 0},
                new double[]{10, 0, -7.5},
                new double[]{10, -2.5, -10},
                new double[]{0, -10, -7.5},
                new double[]{0, -2.5, 0},
                new double[]{10, -2.5, -10},
                new double[]{2.5, -10, -10},
                new double[]{0, -10, -7.5},
                new double[]{-2.5, 10, -10},
                new double[]{0, 10, -7.5},
                new double[]{0, 2.5, 0},
                new double[]{-10, 2.5, -10},
                new double[]{-2.5, 10, -10},
                new double[]{0, 2.5, 0},
                new double[]{-10, 2.5, -10},
                new double[]{0, 2.5, 0},
                new double[]{-2.5, 0, 0},
                new double[]{-10, 2.5, -10},
                new double[]{-2.5, 0, 0},
                new double[]{-10, 0, -7.5},
                new double[]{2.5, 0, 0},
                new double[]{0, 2.5, 0},
                new double[]{0, 10, -7.5},
                new double[]{10, 0, -7.5},
                new double[]{2.5, 0, 0},
                new double[]{0, 10, -7.5},
                new double[]{10, 0, -7.5},
                new double[]{0, 10, -7.5},
                new double[]{2.5, 10, -10},
                new double[]{10, 0, -7.5},
                new double[]{2.5, 10, -10},
                new double[]{10, 2.5, -10},
                new double[]{-2.5, -10, 10},
                new double[]{-10, 0, 7.5},
                new double[]{-10, -2.5, 10},
                new double[]{0, -10, 7.5},
                new double[]{-10, 0, 7.5},
                new double[]{-2.5, -10, 10},
                new double[]{0, -10, 7.5},
                new double[]{-2.5, 0, 0},
                new double[]{-10, 0, 7.5},
                new double[]{0, -10, 7.5},
                new double[]{0, -2.5, 0},
                new double[]{-2.5, 0, 0},
                new double[]{2.5, 0, 0},
                new double[]{10, -2.5, 10},
                new double[]{10, 0, 7.5},
                new double[]{0, -2.5, 0},
                new double[]{10, -2.5, 10},
                new double[]{2.5, 0, 0},
                new double[]{0, -2.5, 0},
                new double[]{2.5, -10, 10},
                new double[]{10, -2.5, 10},
                new double[]{0, -2.5, 0},
                new double[]{0, -10, 7.5},
                new double[]{2.5, -10, 10},
                new double[]{0, 10, 7.5},
                new double[]{-2.5, 10, 10},
                new double[]{-10, 2.5, 10},
                new double[]{0, 2.5, 0},
                new double[]{0, 10, 7.5},
                new double[]{-10, 2.5, 10},
                new double[]{0, 2.5, 0},
                new double[]{-10, 2.5, 10},
                new double[]{-10, 0, 7.5},
                new double[]{0, 2.5, 0},
                new double[]{-10, 0, 7.5},
                new double[]{-2.5, 0, 0},
                new double[]{0, 2.5, 0},
                new double[]{2.5, 10, 10},
                new double[]{0, 10, 7.5},
                new double[]{2.5, 0, 0},
                new double[]{2.5, 10, 10},
                new double[]{0, 2.5, 0},
                new double[]{2.5, 0, 0},
                new double[]{10, 2.5, 10},
                new double[]{2.5, 10, 10},
                new double[]{2.5, 0, 0},
                new double[]{10, 0, 7.5},
                new double[]{10, 2.5, 10}
        );

        final MarchingCubes m = new MarchingCubes(ring, f, -10, 10, -10, 10, -10, 10, 3);
        final List<double[]> vertices = m.generateVertices();
        myAssert(expVertices, vertices, 0.001);
        F ff=new F(fStr,ring);

        page.insertObj("f", ff);
   
        final List<double[]> verticesF = new ImplicitPlot3dCallable(page,
                "\\implicitPlot3d(f, -10, 10, -10, 10, -10, 10, 3);", 0).call();
        myAssert(expVertices, verticesF, 0.001);
        final List<double[]> verticesFF = new ImplicitPlot3dCallable(page,
                "\\implicitPlot3d("+fStr+", -10, 10, -10, 10, -10, 10, 3);", 0).call();
        myAssert(expVertices, verticesFF, 0.001);
        
    }

    private static void myAssert(
            final List<double[]> expected, final List<double[]> actual, final double e) {
        assertEquals(expected.size(), actual.size());
        for (int i = 1; i < actual.size(); i++) {
            assertArrayEquals(expected.get(i), actual.get(i), e);
        }
    }

    @Test
    public void testLerp() {
        final double[] v1 = {1, 2, 3};
        final double[] v2 = {4, 5, 6};
        final double alpha = 0.5;

        final double[] lerpExp = {2.5, 3.5, 4.5};

        final double[] lerp = MarchingCubes.lerp(v1, v2, alpha);

        assertArrayEquals(lerpExp, lerp, 0.001);
    }
}
