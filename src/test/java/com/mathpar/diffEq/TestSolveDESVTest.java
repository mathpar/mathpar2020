/**
* Copyright © 2011 Mathparca Ltd. All rights reserved.
*/
package com.mathpar.diffEq;
import org.junit.jupiter.api.Test;
import static com.mathpar.func.PageTest.pageDoesntThrowException;
/**
 * @author Admin
 */
public class TestSolveDESVTest{

    public TestSolveDESVTest() {}
    
    @Test
    public void test_01(){
     //   System.out.println("\n  Тест 01");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE(\\d(y,x) - y(x^2+\\exp(x))=0);";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_02(){
     //   System.out.println("\n  Тест 02");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE(\\d(y,x) = x+\\sin(x));";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_03(){
   //     System.out.println("\n  Тест 03");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE(-9\\d(y,x) = \\cos(x)^2);";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_04(){
        System.out.println("\n  Тест 04");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE(\\d(y,x) - xy^2 = 2xy);";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_05(){
        System.out.println("\n  Тест 05");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE(x\\sqrt(1-y^2) = -y\\d(y,x)\\sqrt(1-x^2));";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_06(){
        System.out.println("\n  Тест 06");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE(x*\\exp(x-y) = \\d(y,x));";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_07(){
        System.out.println("\n  Тест 07");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE(\\d(y,x)\\exp(-x) = x-1);";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_08(){
        System.out.println("\n  Тест 08");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE(\\d(y,x)\\sin(x)-y\\cos(x) = 0);";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_09(){
        System.out.println("\n  Тест 09");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE(\\d(y,x) = (y+1)(x-1));";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_10(){
        System.out.println("\n  Тест 10");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE(\\d(y,x)\\ctg(x)+y = 2);";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_11(){
        System.out.println("\n  Тест 11");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE((x^2-yx^2)\\d(y,x) = (y^2+xy^2));";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_12(){
        System.out.println("\n  Тест 12");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE(\\d(y,x) = x^2y);";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_13(){
        System.out.println("\n  Тест 13");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE(\\d(y,x) = 4);";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_14(){
        System.out.println("\n  Тест 14");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE(\\d(y,x) = \\cos(x)\\exp(y));";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_15(){
        System.out.println("\n  Тест 15");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE(7\\d(y,x) = \\sin(x));";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_16(){
        System.out.println("\n  Тест 16");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE(y^2\\d(y,x) = x);";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_17(){
        System.out.println("\n  Тест 17");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE(\\d(y,x) = -xy);";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_18(){
        System.out.println("\n  Тест 18");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE(\\d(y,x)(xy+2x) = \\sin(\\exp(x^4+2)-\\tg(\\cos(x)-7))\\exp(y+x));";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_19(){
        System.out.println("\n  Тест 19");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE(\\d(y,x) - xy^3 = 0);";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_20(){
        System.out.println("\n  Тест 20");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE(\\d(y,x) =  yx-y+x-1);";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_21(){
        System.out.println("\n  Тест 21");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE((1+x^2)\\d(y,x) - 2xy = 0);";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_22(){
        System.out.println("\n  Тест 22");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE(1 + y + (x+1)\\d(y,x) = 0);";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_23(){
        System.out.println("\n  Тест 23");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE(\\d(y,x) = y^2\\cos(x));";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_24(){
        System.out.println("\n  Тест 24");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE(\\d(y,x) + y\\tg(x) = 0);";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_25(){
        System.out.println("\n  Тест 25");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE(y\\sqrt(9-x^2)\\d(y,x) = -\\sqrt(4-y^2));";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_26(){
        System.out.println("\n  Тест 26");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE(xy\\d(y,x) = (1-x^2)^2);";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_27(){
        System.out.println("\n  Тест 27");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE(\\exp(2x)(1+\\d(y,x)) = 1);";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_28(){
        System.out.println("\n  Тест 28");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE(\\d(y,x)(1+5y) = xy\\sin(2x));";
        pageDoesntThrowException(test);
    }
//    @Test
//    public void test_29(){
//        System.out.println("\n  Тест 29");
//        String test = "SPACE = Q[x,y];\n"
//                + "\\solveDE(\\d(y,x) = 9y^2 + 4);";
//        pageDoesntThrowException(test);
//    }
    @Test
    public void test_30(){
        System.out.println("\n  Тест 30");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE(\\d(y,x)\\tg(x) - y  = 1);";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_31(){
        System.out.println("\n  Тест 31");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE(x\\d(y,x) = y\\ln(y));";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_32(){
        System.out.println("\n  Тест 32");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE(y(1+x^2)\\d(y,x) = 1+y^2);";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_33(){
        System.out.println("\n  Тест 33");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE(1+y^2 = (1+x^2)\\d(y,x));";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_34(){
        System.out.println("\n  Тест 34");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE(y\\exp(2x) - (1+\\exp(2x))\\d(y,x) = 0);";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_35(){
        System.out.println("\n  Тест 35");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE(\\d(y,x) = 2\\sin(x)\\cos(x));";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_36(){
        System.out.println("\n  Тест 36");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE(1 + \\d(y,x) + y + x\\d(y,x) = 0);";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_37(){
        System.out.println("\n  Тест 37");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE(\\exp(x)+\\d(y,x) = \\exp(x));";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_38(){
        System.out.println("\n  Тест 38");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE((x+2)\\sqrt(y)-3x\\d(y,x) = 0);";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_39(){
        System.out.println("\n  Тест 39");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE(x\\d(y,x) = -y\\d(y,x)+x-4);";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_40(){
        System.out.println("\n  Тест 40");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE(x\\d(y,x) = -3\\d(y,x)+\\d(y,x)^2+x-4);";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_41(){
        System.out.println("\n  Тест 41");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE(x\\d(y,x) = x\\d(y,x)+\\cos(3x-5));";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_42(){
        System.out.println("\n  Тест 42");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE(\\d(y,x) = x/\\cos(y));";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_43(){
        System.out.println("\n  Тест 43");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE(x/\\d(y,x) = \\exp(x+y)/(2x^2y-6x^2));";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_44(){
        System.out.println("\n  Тест 44");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE(\\d(y,x) = \\sqrt(2x^2y-6x^2));";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_45(){
        System.out.println("\n  Тест 45");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE(\\exp(y)(1+x^2)\\d(y) - 2x(1+\\exp(y))\\d(x) = 0);";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_50(){
        System.out.println("\n  Тест 50");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE(3x\\d(y,x) = \\exp(\\sin(x)+(y^2+4)));";
        pageDoesntThrowException(test);
        System.out.println("");
    }
    
}