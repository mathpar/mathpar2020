/**
* Copyright © 2011 Mathparca Ltd. All rights reserved.
*/

package com.mathpar.diffEq;
import org.junit.jupiter.api.Test;
import static com.mathpar.func.PageTest.pageDoesntThrowException;
/**
 * @author Admin
 */
public class TestSolveETDTest{

    public TestSolveETDTest() {}
    
    @Test
    public void test_01(){
    //    System.out.println("\n  Тест 01");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE((2x-y+1)\\d(x)+(2y-x-1)\\d(y) = 0);";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_02(){
     //   System.out.println("\n  Тест 02");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE((3x^2-3y^2+4x)\\d(x)-(6xy+4y)\\d(y) = 0);";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_03(){
   //     System.out.println("\n  Тест 03");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE((6y-3x^2+3y^2)\\d(x)+(6x+6xy)\\d(y) = 0);";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_04(){
    //    System.out.println("\n  Тест 04");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE((2x(1-\\exp(y))/(1+x^2)^2)\\d(x)+(\\exp(y)/(1+x^2))\\d(y) = 0);";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_05(){
    //    System.out.println("\n  Тест 05");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE((3x^2+6xy^2)\\d(x)+(6x^2y+4y^3)\\d(y) = 0);";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_06(){
      //  System.out.println("\n  Тест 06");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE(2xy\\d(x)+(x^2+3y^2)\\d(y) = 0);";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_07(){
    //    System.out.println("\n  Тест 07");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE((\\exp(x)y+\\cos(y)\\sin(x)-x^3y^6)\\d(x)+(\\exp(x)+\\sin(y)\\cos(x)-3/2*y^5x^4)\\d(y) = 0);";
        pageDoesntThrowException(test);
    }
}