/**
* Copyright © 2011 Mathparca Ltd. All rights reserved.
*/

package com.mathpar.diffEq;
import org.junit.jupiter.api.Test;
import static com.mathpar.func.PageTest.pageDoesntThrowException;
/**
 * @author Admin
 */
public class TestSolveHDETest{

    public TestSolveHDETest() {}
    
    @Test
    public void test_01(){
    //    System.out.println("\n  Тест 01");
        String test = "SPACE = Q[x,y];"
                + "\\solveDE(2x^3\\d(y,x) = y(2x^2-y^2));";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_02(){
    //    System.out.println("\n  Тест 02");
        String test = "SPACE = Q[x,y];"
                + "\\solveDE(y^2+x^2\\d(y,x) = xy\\d(y,x));";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_03(){
    //    System.out.println("\n  Тест 03");
        String test = "SPACE = Q[x,y];"
                + "\\solveDE((x^2+y^2)\\d(y,x) = 2xy);";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_04(){
    //    System.out.println("\n  Тест 04");
        String test = "SPACE = Q[x,y];"
                + "\\solveDE(x\\d(y,x)-y = x\\tg(y/x));";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_05(){
    //    System.out.println("\n  Тест 05");
        String test = "SPACE = Q[x,y];"
                + "\\solveDE(x\\d(y,x) = y-x\\exp(y/x));";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_06(){
     //   System.out.println("\n  Тест 06");
        String test = "SPACE = Q[x,y];"
                + "\\solveDE(x\\d(y,x)-y = (x+y)\\ln((x+y)/x));";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_07(){
    //    System.out.println("\n  Тест 07");
        String test = "SPACE = Q[x,y];"
                + "\\solveDE(x\\d(y,x) = y\\cos(\\ln(y/x)));";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_08(){
     //    System.out.println("\n  Тест 08");
        String test = "SPACE = Q[x,y];"
                + "\\solveDE(x\\d(y,x) = y+\\sqrt(xy));";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_09(){
    //    System.out.println("\n  Тест 09");
        String test = "SPACE = Q[x,y];"
                + "\\solveDE(x\\d(y,x) = \\sqrt(x^2 - y^2)+y);";
        pageDoesntThrowException(test);
    }
    
//    @Test
//    public void test_10(){
//    //    System.out.println("\n  Тест 10");
//        String test = "SPACE = Q[x,y];"
//                + "\\solveDE(\\d(y,x) = (x^3+y^3)/(3x^3+5x^2y));";
//        pageDoesntThrowException(test);
//    }
    @Test
    public void test_11(){
    //    System.out.println("\n  Тест 11");
        String test = "SPACE = Q[x,y];"
                + "\\solveDE(x\\d(y,x) = 2\\sqrt(3x^2+y^2));";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_12(){
     //   System.out.println("\n  Тест 12");
        String test = "SPACE = Q[x,y];"
                + "\\solveDE(\\d(y,x) = \\arcsin((x^2+y^2)/(2xy)));";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_13(){
    //    System.out.println("\n  Тест 13");
        String test = "SPACE = Q[x,y];"
                + "\\solveDE(x^2\\d(y,x) + y^2 + xy + x^2 = 0);";
        pageDoesntThrowException(test);
    }
    //@Test
    public void test_14(){
     //   System.out.println("\n  Тест 14");
        String test = "SPACE = Q[x,y];"
                + "\\solveDE((x+4y)\\d(y,x) = 2x+3y-5);";
        pageDoesntThrowException(test);
    }
//    @Test
//    public void test_15(){
//    //    System.out.println("\n  Тест 15");
//        String test = "SPACE = Q[x,y];"
//                + "\\solveDE(y\\d(y,x)+4x=x\\sin(\\cos(\\exp(y/x))));";
//        pageDoesntThrowException(test);
//    }
}