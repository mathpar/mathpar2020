/**
* Copyright © 2011 Mathparca Ltd. All rights reserved.
*/
package com.mathpar.diffEq;
import org.junit.jupiter.api.Test;
import static com.mathpar.func.PageTest.pageDoesntThrowException;
/**
 * @author Admin
 */
public class TestSolveLDETest{

    public TestSolveLDETest() {}
    
    @Test
    public void test_01(){
    //    System.out.println("\n  Тест 01");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE(x\\d(y,x) + y = y^2\\ln(x));";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_02(){
      //  System.out.println("\n  Тест 02");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE(\\d(y,x) + y/\\sqrt(1-x^2) = y^2\\arcsin(x)/\\sqrt(1-x^2));";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_03(){
     //   System.out.println("\n  Тест 03");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE(\\d(y,x) + y = 3\\exp(-2x)y^2);";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_04(){
     //   System.out.println("\n  Тест 04");
        String test = "SPACE = Q[x,y];\\solveDE(\\d(y,x) - 2y/x = 2x\\sqrt(y));";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_05(){
     //   System.out.println("\n  Тест 05");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE(\\d(y,x) + y/(x+1) = -y^2);";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_06(){
     //   System.out.println("\n  Тест 06");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE(\\d(y,x) - 2y/x = -x^2y^2);";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_07(){
     //   System.out.println("\n  Тест 07");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE(x\\d(y,x) - 4y = x^2\\sqrt(y));";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_08(){
     //   System.out.println("\n  Тест 08");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE(\\d(y,x) + 3y/x = x^3y^3);";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_09(){
     //   System.out.println("\n  Тест 09");
        String test = "SPACE = Q[x,y];\n"
                + "\\solveDE(2\\d(y,x) - 3y\\cos(x) = -\\exp(-2x)(2+3\\cos(x))/y);";
        pageDoesntThrowException(test);
    }
    
    //линейные неоднородные уравнения
    @Test
    public void test_10(){
     //   System.out.println("\n  Тест 01");
        String test = "SPACE=Q[x,y];f=\\solveDE(x\\d(y,x) - y = 2x^3);";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_11(){
     //   System.out.println("\n  Тест 02");
        String test = "SPACE=Q[x,y];f=\\solveDE(\\d(y,x) + 2xy = x\\exp(-x^2));";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_12(){
    //    System.out.println("\n  Тест 03");
        String test = "SPACE=Q[x,y];f=\\solveDE(\\d(y,x) + y\\tg(x) = 1/\\cos(x));";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_13(){
    //    System.out.println("\n  Тест 04");
        String test = "SPACE=Q[x,y];f=\\solveDE(\\d(y,x) + y/x = 2\\exp(x^2));";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_14(){
     //   System.out.println("\n  Тест 05");
        String test = "SPACE=Q[x,y];f=\\solveDE(\\d(y,x) - 2y/(x+1) = (x+1)^3);";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_15(){
    //    System.out.println("\n  Тест 06");
        String test = "SPACE=Q[x,y];f=\\solveDE(x^2\\d(y,x) - 2xy = 3);";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_16(){
    //    System.out.println("\n  Тест 07");
        String test = "SPACE=Q[x,y];f=\\solveDE(x\\d(y,x) + (x+1)y = 3x^2\\exp(-x));";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_17(){
    //    System.out.println("\n  Тест 08");
        String test = "SPACE=Q[x,y];f=\\solveDE(\\d(y,x) - 2xy = 2x^3);";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_18(){
    //    System.out.println("\n  Тест 09");
        String test = "SPACE=Q[x,y];f=\\solveDE(\\d(y,x) + 4y/(4x-3) = \\ln(x));";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_19(){
     //   System.out.println("\n  Тест 10");
        String test = "SPACE=Q[x,y];f=\\solveDE(\\d(y,x) - y\\tg(x) = \\sin(x));";
        pageDoesntThrowException(test);
    }
    @Test
    public void test_20(){
    //    System.out.println("\n  Тест 11");
        String test = "SPACE=Q[x,y];f=\\solveDE(\\d(y,x) + 3y/x = 2/x^2);";
        pageDoesntThrowException(test);
    }
    
}