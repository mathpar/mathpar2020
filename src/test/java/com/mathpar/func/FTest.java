/**
* Copyright © 2011 Mathparca Ltd. All rights reserved.
*/

package com.mathpar.func;
import com.mathpar.polynom.*;
import com.mathpar.number.Element;
import com.mathpar.number.Fraction;
import com.mathpar.number.Ring;
import com.mathpar.func.*;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.*;

public class FTest {

    @Test
    public void testValueDivideOfPolynomials() {
        Ring r;
        r = Ring.ringR64xyzt; 
        r.CForm=new CanonicForms(r,true);
        com.mathpar.polynom.Polynom p1 = new Polynom("x y", r);
        Polynom p2 = new Polynom("x + 1", r);
        F f = new F(F.DIVIDE, p1, p2);
        Polynom[] vP=r.varPolynom;
        Element v = f.value(vP, r);

        assertTrue(v.equals(new Fraction(p1, p2), r), v.toString(r));
    }
}
