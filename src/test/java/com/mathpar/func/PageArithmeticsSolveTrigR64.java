/**
* Copyright © 2011 Mathparca Ltd. All rights reserved.
*/

package com.mathpar.func;

import com.mathpar.number.Ring;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.mathpar.func.PageTest.doPageTest;
import static com.mathpar.func.PageTest.doPageTest;

/**
 *
 * @author r1d1
 */
public class PageArithmeticsSolveTrigR64 {
    private Ring r;

    @BeforeEach
    public void beforeTest() {       
        r = new Ring("R64[x]");
    }
    
    @Test
    public void tanR64() {
        doPageTest(r, "\\solveTrig(\\tg(x)=1);", "(0.79+\\pi*n)");}
    
    @Test
    public void sinR64() {
        doPageTest(r, "\\solveTrig(\\sin(x)=1);", "(2*\\pi*n+0.5*\\pi)");}
    
    @Test
    public void cosR64() {
        doPageTest(r, "\\solveTrig(\\cos(x)=1);", "2*\\pi*n");}
    
    @Test
    public void ctgR64() {
        doPageTest(r, "\\solveTrig(\\ctg(x)=1);", "(0.79+\\pi*n)");}
    
}
