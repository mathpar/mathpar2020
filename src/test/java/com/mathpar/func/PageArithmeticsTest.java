/**
* Copyright © 2011 Mathparca Ltd. All rights reserved.
*/

package com.mathpar.func;

import java.util.LinkedHashMap;
import java.util.Map;
import com.mathpar.number.Ring;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.mathpar.func.PageTest.*;

/**
 * Tests of simple arithmetics in page.
 */
public class PageArithmeticsTest {
    private Ring r;

    @BeforeEach
    public void beforeTest() {
        r = new Ring("Z[x, y, z]");        
    }
    
 
    @Test 
    public void polynomVal() {
      doPageTest(r, "x=-2;y=2; z=\\pi/2 ;p=x +y+z; h=\\value(\\sin(\\value(p)))  ", "1");}
    @Test
    public void polynomVal02() { 
    doPageTest(r, "x=-2;y=2; z=1 ;p=x +y+z; b=\\value(p); h=\\value(\\sin(b))  ", "\\sin(1)");}
    
       @Test
    public void SEVERALeQ01() {
        doPageTest(r, "   p=x +y+z = x^2   ", "x^2");}
      @Test
    public void SEVERALeQ02() {
        doPageTest(r, " G=[1+3]=  (p)=x +y+z =1+x^2   ", "x^2+1");}
 

    @Test
    public void arithSimpleOps() {
        Map<String, String> tests = new LinkedHashMap<String, String>();
        tests.put("\\ceil(9/4)", "3;");
        tests.put("2 + 2", "4;");
        tests.put("2 * 2", "4;");
        tests.put("2 2", "4;");
        tests.put("2 - 2", "0;");
        tests.put("2^2", "4;");
        tests.put("5!", "120;");
        
        tests.put("\\floor(19/4)", "4;");
        tests.put("\\round(19/4)", "5;");
        tests.put("\\round(-19/4)", "-5;");
        tests.put("\\round(-10/4)", "-3;");
        tests.put("\\round(1 /4)", "0;");
        tests.put("\\round(-1 /4)", "0;");
        tests.put("\\round(-3/4)", "-1;");
        tests.put("\\round(3/4)", "1;");
        for (Map.Entry<String, String> test : tests.entrySet()) {
            doPageTest(r, test.getKey(), test.getValue());
        }
    }
}
