/**
* Copyright © 2011 Mathparca Ltd. All rights reserved.
*/

package com.mathpar.func;

import com.mathpar.number.Ring;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.mathpar.func.PageTest.*;
import static org.junit.jupiter.api.Assertions.*;

public class PageFunctionsTest {
    private Ring r=Ring.ringR64xyzt;

    @BeforeEach
    public void beforeTest() {
        r = new Ring("CZ[x, y, z]");
    }
    
    @Test
    public void Factor2() {
        doPageTest(new Ring("Q[x]"), "\\Factor(\\cos(x)^2 - \\sin(x)^2)", "\\cos(2x)");
    }
    
    @Test
    public void Factor1() {
        doPageTest(new Ring("Q[x]"), "\\Factor(\\sin(x)^2 + \\cos(x)^2)", "1");
    }

       @Test
    public void valueOfFname0C() {
        doPageTest(r, "\\Factor(\\sqrt(m) m^(1/3))", "m^{(5/6)}");
    }
//     
//   @Test
//    public void valueOfFname0B() {
//        doPageTest(r, "d= (m^(1/2))^(1/9)=m^(1/18)", "m^{(1/18)}");
//    }
//       @Test
//    public void valueOfFname0A() {
//        doPageTest(r, "q+b= (m^(1/2))^(1/9)=m^(1/18)", "m^{(1/18)}");
//    }
//    @Test
//    public void valueOfFname00() {
//        doPageTest(r, "  a=b+1+x; c=\\value(a,[w])", "(1+b+w)");
//    }
//    @Test
//    public void valueOfFname01() {
//        doPageTest(r, " \\clean();\\value(a,[w])", "a");
//    }
//           @Test
//    public void valueOfFname02() {
//        doPageTest(r, " \\clean(); a=\\sin(x+x^2);\\value(a,[w])", "\\sin(w^2+w)");
//    }
//    
//    @Test
//    public void funcSolveOneWithNames() {
//        doPageTest(r, "\\solve([y/n + S/T = 0],[y])", "(-1*n*S)/T");}
//    
//    @Test
//    public void funcSolveOneWithoutVectors() {
//        doPageTest(r, "\\solve(x + 1)", "-1");
//    }
//
//    @Test
//    public void funcSolveOneWithLetter() {
//        doPageTest(r, "\\solve(x - a)", "a");
//    }
//
//    @Test
//    public void funcSolveOneWithoutVectorsWithVars() {
//        doPageTest(r, "\\solve(x + 1, [x])", "-1");
//    }
//
//    @Test
//    public void funcSolveOne() {
//        doPageTest(r, "\\solve([x + 1])", "-1");
//    }
//
//    @Test
//    public void funcSolveOneWithEq() {
//        doPageTest(r, "\\solve([x + 1 = 0])", "-1");}
//    @Test
//    public void funcSolveOneWithEqWithVars() {
//        doPageTest(r, "\\solve([x + 1 = 0], [x])", "-1");
//    }
//
//    @Test
//    public void funcSolveSystem() {
//        doPageTest(r, "\\solve([x+3y-1, y+5x+7])", "[(-11/7),(6/7)]");
//    }
//
//    @Test
//    public void funcSolveSystemWithEqWithVars() {
//        doPageTest(r, "\\solve([x+3y-1=0, y+5x+7=0], [x, y])", "[(-11/7),(6/7)]");
//    }
//
//    @Test
//    public void funcSolveSystemWithWithVarsWithLetters() {
//        doPageTest(r, "\\solve([x-3y+2, y-3x+4, x+y+z-9], [x, y, z])", "[(7/4),(5/4),6]");
//    }
//
//    @Test
//    public void funcKornyakMatrix() {
//        Page page = new Page(Ring.ringR64xyzt,true);
//        doPageTest(page,
//                "SPACE = Z[];"
//                + "Perm = [[0, 1, 2, 1, 2, 0], [0, 1, 1, 0]];"
//                + "A = \\kornyakMatrix(Perm, 3);"
//                + "\\print(A);",
//                // OUT
//                "A = \n"
//                + "[[1, a, a]\n"
//                + " [a, 1, a]\n"
//                + " [a, a, 1]];");
//        assertEquals("Ring must change after kornyakMatrix call",
//                "Z64[a]", page.ring.toString());
//    }
//        
//            @Test
//    public void help_add_log() {
//        doPageTest( "SPACE=R64[x ];\n"+
//                "g= \\log(a,b);"
//                + "f=g+0; " 
//                 + "\\print(f);",
//                // OUT
//                "f=\\log_{a}(b)");
//    }
//             @Test
//    public void help_add_systLDE() {
//        
//        doPageTest("SPACE=R64[t];\n"+
//                "\\systLDE(\\d(f,t^2) =0 );",
//                // OUT
//                "\\systLDE(\\d_{t^2}(f) =0 );");
//    }
//              @Test
//    public void help_add_systLDE01() {    
//        
//        PageTest.doPageTest( "SPACE=R64[t]; g=\\systLDE(-x-y+\\d(y, t^7)=0, -x-z+\\d(z, t^5)=0);"
//     ,// OUT
//       "\\systLDE((\\d_{t^7}(y)-(y+x))=0,  (\\d_{t^5}(z)-(z+x))=0)");
//    }
//                  @Test
//    public void help_add_systLDE02() {    
//        
//        PageTest.doPageTest( "SPACE=R64[t]; g=\\systLDE(\\d(x, t)+x-2y=0, \\d(y, t)+4y+x=0);"
//     ,// OUT
//       "\\systLDE(((\\d_t(x)+x)-2*y)=0,   (\\d_t(y)+4*y+x) =0)");
//    }
}