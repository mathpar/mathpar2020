/**
* Copyright © 2011 Mathparca Ltd. All rights reserved.
*/

package com.mathpar.func;

import com.mathpar.number.Ring;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.mathpar.func.PageTest.*;

public class PageHelp002IntroTest {
        private Ring r;

    @BeforeEach
    public void beforeTest() {
        r = new Ring("Z[x, y, z]");
    }
 
        @Test
    public void help_XXXX() {
        doPageTest("SPACE=Z[x, y];\n"
                + "A = [[x^2, y], [4, x + y]];\n"
                + "a = \\elementOf(A);\n"
                + "B = a_{1, ?};\n" 
                + "b = \\elementOf(B);\n"
                +"h=a_{1,1};\n"
                + "\\print(B, h);",
                // OUT
                "B = [x^2,y];\n"
                + " h = x^2;");
    }
    
    @Test
    public void help_2_2_001() {
        doPageTest("SPACE = R64[x, y];\n"
                + "f1 = \\sin(x);\n"
                + "f2 = \\sin(\\cos(x + \\tg(y)));\n"
                + "f3 = (\\sin(x^2) + y);\n"
                + "\\print(f1, f2, f3);",
                // OUT
                "f1 = \\sin(x);\n"
                + "f2 = \\sin(\\cos(\\tg(y)+x));\n"
                + "f3 = (\\sin(x^2)+y);");
    }

    @Test
    public void help_2_3_001() {
        doPageTest("SPACE=R[x, y];\n"
                                + "f=\\sin(x^2+\\tg(y^3+x));\n"
                + "f=\\sin( \\tg( x));\n"
                + "g=\\value(\\sin( \\tg( x)), [  2]);\n"
                + "\\print(g);",
                // OUT
                "g = -0.82;");
    }

    @Test
    public void help_2_3_002() {
        doPageTest("SPACE=Z[x, y]; \n"
                + "f=x+y; \n"
                + "g=f^2; \n"
                + "r=\\value(f, [x^2, y^2]); \n"
                + "\\print(g, f, r);",
                // OUT
                "g = y^2+2yx+x^2;\n"
                + "f = y+x;\n"
                + "r = y^2+x^2;");
    }

    @Test
    public void help_2_3_003() {
        doPageTest("SPACE=R64[x]; \n"
                + "f=\\sin(x)/x; \n"
                + "g=\\lim(f, 0); \n"
                + "\\print(g);",
                // OUT
                "g = 1;");
    }

    @Test
    public void help_2_3_004() {
        doPageTest("SPACE=Z[x, y]; \n"
                + "f=\\sin(x^2+ \\tg(y^3+x)); \n"
                + "h= \\D(f, y); \n"
                + "\\print(h);",
                // OUT
                "h = 3y^2*\\cos(x^2+\\tg(y^3+x))/(\\cos(y^3+x))^2;");
    }

    @Test
    public void help_2_4_001() {
        doPageTest("SPACE=R64[x];\n"
                + "b=\\solve(x^2-5x+6=0);",
                // OUT
                "[3,2];");
    }

    @Test
    public void help_2_4_002() {
        doPageTest("SPACE=R64[x];\n"
                + "FLOATPOS=6;\n"
                + "b=\\solve(x^4+2x+1=0);",
                // OUT
                "[-0.543689,-1];");
    }

    @Test
    public void help_2_4_003() {
        doPageTest("SPACE=R64[x]; \n"
                + "FLOATPOS=0;\n"
                + "b=\\solve(x^3+3x^2+3x+1=0);",
                // OUT
                "-1");
    }

    @Test
    public void help_2_5_001() {
        doPageTest("SPACE=Z[x,y];\n"
                + "A=[[x, 4], [y, 5]];\n"
                + "V=[x, y, 1, 2, x^6 ];\n"
                + "\\print(A, V);",
                // OUT
                "A = \n"
                + "[[x, 4],\n"
                + " [y, 5]];\n"
                + " V = [x,y,1,2,x^6];");
    }

    @Test
    public void help_2_5_002() {
        doPageTest("SPACE=Z[x, y];\n"
                + "A=[[3, 4], [3, 1]];\n"
                + "B=[[2, 5], [4, 7]];\n"
                + "C=A+B;\n"
                + "G=A-B;\n"
                + "T=A*B;\n"
                + "\\print(C, G, T);",
                // OUT
                "C = \n"
                + "[[5, 9],\n"
                + " [7, 8]];\n"
                + " G = \n"
                + "[[1,  -1],\n"
                + " [-1, -6]];\n"
                + " T = \n"
                + "[[22, 43],\n"
                + " [10, 22]];");
    }

    @Test
    public void help_2_5_003() {
        doPageTest("SPACE=Z[x];\n"
                + "A=[[1,4],[-4,5]];\n"
                + "a=\\elementOf(A);\n"
                + "det=a_{1,1}*a_{2,2}-a_{1,2}*a_{2,1};\n"
                + "\\print(det);",
                // OUT
                "det = 21;");
    }

    @Test
    public void help_2_5_004() {
        doPageTest("SPACE=Z[x, y];\n"
                + "A = [[x^2, y], [4, x + y]];\n"
                + "a = \\elementOf(A);\n"
                + "B = a_{1, ?};\n"
                + "C = a_{?, 2};\n"
                + "b = \\elementOf(B);\n"
                + "c = \\elementOf(C);\n"
                + "h = b_{2} * c_{1, 1};\n"
                + "\\print(B, C, h);",
                // OUT
                "B = [x^2,y];\n"
                + " C = \n"
                + "[y,y+x]^T;\n"
                + " h = y^2;");
    }

    @Test
    public void help_2_5_005() {
        doPageTest("SPACE=Z[x, y];\n"
                + "A=3x*\\I_{2, 2};\n"
                + "B=\\O_{3, 3};\n"
                +" C=F_{23,56} ;\n "
                + "\\print(A, B,C);",
                // OUT
                "A = \n"
                + "[[3x, 0 ],\n"
                + " [0,  3x]];\n"
                + " B = \n"
                + "[[0, 0, 0],\n"
                + " [0, 0, 0],\n"
                + " [0, 0, 0]];\n"
                        + " C = F_{23,56};");
    }

    @Test
    public void help_2_6_001() {
        pageDoesntThrowException("SPACE=Z[];\n"
                + "a=\\randomNumber(10);\n"
                + "b=\\randomNumber(100);\n"
                + "\\print(a, b);");
    }

    @Test
    public void help_2_6_002() {
        pageDoesntThrowException("SPACE = Z[x, y];\n"
                + "f = \\randomPolynom(4, 4, 10, 5);\n"
                + "g = \\randomPolynom(4, 4, 10, 5);\n"
                + "h = f + g;\n"
                + "e = \\expand(h); \n"
                + "\\print(f, g, h, e);");
    }

    @Test
    public void help_2_6_003() {
        pageDoesntThrowException("SPACE = Z[x, y, z];\n"
                + "matr_n = \\randomMatrix(4, 4, 100, 5);\n"
                + "matr_p = \\randomMatrix(2, 2, 100, 1, 1, 4, 25, 3);\n"
                + "\\print(matr_n, matr_p);");
    }
}

