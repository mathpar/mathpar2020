/**
* Copyright © 2011 Mathparca Ltd. All rights reserved.
*/

package com.mathpar.func;

import org.junit.jupiter.api.Test;

import static com.mathpar.func.PageTest.pageDoesntThrowException;

public class PageHelp003PlotsTest {
    @Test
    public void help_3_1_001() {
        pageDoesntThrowException( 
                "\\set2D(-10, 10, -10, 10);\n"+
                "f = x^2 + \\tg(x^2 - 1);\n"
                + "p1 = \\plot(f);");
    }

    @Test
    public void help_3_1_002() {
        pageDoesntThrowException("f = \\sin(x);\n"
                + "p3 = \\plot([f, \\tg(x)]);");
    }

    @Test
    public void help_3_1_003() {
        pageDoesntThrowException("g = \\sin(x);\n"
                + "k = \\cos(x);\n"
                + "f = \\paramPlot([g, k], [-10, 10]);");
    }

//    @Test
//    public void help_3_1_004() {
//        pageDoesntThrowException("p=\\tablePlot(\n"
//                + "    [\n"
//                + "      [0, 1, 4, 9, 16, 25],\n"
//                + "      [0, 1, 2, 3, 4, 5]\n"
//                + "    ]);");
//    }

//    @Test
//    public void help_3_1_005() {
//        pageDoesntThrowException("f1 = \\plot(\\tg(x));\n"
//                + "f2=\\tablePlot(\n"
//                + "    [\n"
//                + "      [0, 1, 4, 9, 16, 25],\n"
//                + "      [0, 1, 2, 3, 4, 5]\n"
//                + "    ]);\n"
//                + "f3 = \\paramPlot([\\sin(x), \\cos(x)], [-10, 10]);\n"
//                + "f4 = \\tablePlot(\n"
//                + "    [\n"
//                + "      [0, 1, 4, 9, 16, 25],\n"
//                + "      [0, -1, -2, -3, -4, -5]\n"
//                + "    ]);\n"
//                + "\\showPlots([f1, f2, f3, f4],'lattice');");
//     }

    @Test
    public void help_3_2_001() {
        pageDoesntThrowException("f = x^2 / 20 + y^2 / 20;\n"
                + "\\plot3d(f, [-20, 20, -20, 20]);");
    }

    @Test
    public void help_3_2_002() {
        pageDoesntThrowException("\\plot3d([x / 20 + y^2 / 20, x^2 / 20 + y / 20],"
                + "[-20, 20, -20, 20]);");
    }
}
