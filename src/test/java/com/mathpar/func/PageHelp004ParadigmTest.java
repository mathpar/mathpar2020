/**
* Copyright © 2011 Mathparca Ltd. All rights reserved.
*/

package com.mathpar.func;

import org.junit.jupiter.api.Test;

import static com.mathpar.func.PageTest.doPageTest;

public class PageHelp004ParadigmTest {
    @Test
    public void help_4_5_001() {
        doPageTest("SPACE=ZMaxPlus[x, y];\n"
                + "a=2; b=9; c=a+b; d=a*b; \\print(c, d)",
                // OUT
                "c = 9;\n"
                + "d = 11;");
    }

    @Test
    public void help_4_6_001() {
        doPageTest("SPACE=Zp32[x, y]; \n"
                + "MOD32=7; \n"
                + "f1=37x+42y+55; \n"
                + "f2=2 f1;  \n"
                + "\\print(f1, f2);",
                // OUT
                "f1 = 2x-1;\n"
                + "f2 = 4x+5;");
    }
}
