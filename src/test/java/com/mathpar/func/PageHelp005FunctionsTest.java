/**
* Copyright © 2011 Mathparca Ltd. All rights reserved.
*/

package com.mathpar.func;

import com.mathpar.number.Element;
import com.mathpar.number.Ring;
import org.junit.jupiter.api.Test;
import com.mathpar.polynom.Polynom;

import static com.mathpar.func.PageTest.*;
import static org.junit.jupiter.api.Assertions.*;

public class PageHelp005FunctionsTest {

//    @Test
//    public void help_5_1_001() {
//        doPageTest("SPACE = R64[x, y];\n"
//                + "f1 = \\sin(x);\n"
//                + "f2 = \\sin(\\cos(x + \\tg(y)));\n"
//                + "f3 = \\sin(x^2) + y;\n"
//                + "\\print(f1, f2, f3);",
//                // OUT
//                "f1 = \\sin(x);\n"
//                + "f2 = \\sin(\\cos(\\tg(y)+x));\n"
//                + "f3 = (\\sin(x^2)+y);");
//    }
//
//    @Test
//    public void help_5_2_001() {
//        doPageTest("SPACE = R[x, y];\n"
//                + "f = \\sin(x^2 + \\tg(y^3 + x));\n"
//                + "g = \\value(f, [1, 2]);\n"
//                + "\\print(g);",
//                // OUT
//                "g = 0.52;");
//    }
//
//    @Test
//    public void help_5_2_002() {
//        doPageTest("SPACE = Z[x];\n"
//                + "RADIAN = 0;\n"
//                + "f = \\sin(x); \n"
//                + "g = \\value(f, 15); \n"
//                + "\\print(g);",
//                // OUT
//                "g = (\\sqrt{6}-(\\sqrt{2}))/(4);");
//    }
//
//    @Test
//    public void help_5_2_003() {
//        doPageTest("SPACE = Z[x];\n"
//                + "RADIAN = 0;\n"
//                + "f = \\sin(x);\n"
//                + "g = \\value(f, 225);\n"
//                + "\\print(g);",
//                // OUT
//                "g = (-1*\\sqrt{2})/(2);");
//    }
//
//    @Test
//    public void help_5_2_004() {
//        doPageTest("SPACE = Z[x];\n"
//                + "RADIAN = 0;\n"
//                + "f = \\cos(x);\n"
//                + "g = \\value(f, 54);\n"
//                + "\\print(g);",
//                // OUT
//                "g = \\sqrt{(5-\\sqrt{5})/(8)};");
//    }
//
//    @Test
//    public void help_5_2_005() {
//        doPageTest("SPACE = Z[x];\n"
//                + "RADIAN = 0;\n"
//                + "f = \\tg(x);\n"
//                + "g = \\value(f, 126);\n"
//                + "\\print(g);",
//                // OUT
//                "g = (-1*\\sqrt{(2*\\sqrt{5}+5)/(5)});");
//    }
//
//    @Test
//    public void help_5_2_006() {
//        doPageTest("SPACE = Z[x];\n"
//                + "RADIAN = 0;\n"
//                + "f = \\sin(x);\n"
//                + "g = \\value(f, 216);\n"
//                + "\\print(g);",
//                // OUT
//                "g = (-1*\\sqrt{(5-\\sqrt{5})/(8)});");
//    }
//
//    @Test
//    public void help_5_2_007() {
//        doPageTest("SPACE = Z[x];\n"
//                + "RADIAN = 0;\n"
//                + "f = \\cos(x);\n"
//                + "g = \\value(f, 108);\n"
//                + "\\print(g);",
//                // OUT
//                "g = (1-\\sqrt{5})/(4);");
//    }
//
//    @Test
//    public void help_5_3_001() {
//        doPageTest("SPACE = Z[x, y];\n"
//                + "f = x + y;\n"
//                + "g = f^2;\n"
//                + "r = \\value(g, [x^2, y^2]);\n"
//                + "\\print(r);",
//                // OUT
//                "r = y^4+2y^2x^2+x^4;");
//    }
//
//    @Test
//    public void help_5_4_001() {
//        doPageTest("SPACE = R64[x];\n"
//                + "f = \\sin(x) / x;\n"
//                + "g = \\lim(f, 0);\n"
//                + "\\print(g);",
//                // OUT
//                "g = 1;");
//    }
//
//    @Test
//    public void help_5_4_002() {
//        doPageTest("SPACE = R64[x];\n"
//                + "f = (x^2 - 2x + 2) / (x^2 + x - 2);\n"
//                + "g = \\lim(f, 1);\n"
//                + "\\print(g);",
//                // OUT
//                "g = \\infty;");
//    }
//
//    @Test
//    public void help_5_4_003() {
//        doPageTest("SPACE = R64[x];\n"
//                + "f = \\sin(x + 3) / (x^2 + 6x + 9);\n"
//                + "g = \\lim(f, -3);\n"
//                + "\\print(g);",
//                // OUT
//                "g = \\infty;");
//    }
//
//    @Test
//    public void help_5_4_004() {
//        doPageTest("SPACE = R64[x];\n"
//                + "f = (1 + 1 / x)^x;\n"
//                + "g=\\lim(f, \\infty);\n"
//                + "\\print(g);",
//                // OUT
//                "g = 2.72;");
//    }
//
//    @Test
//    public void help_5_5_001() {
//        doPageTest("SPACE=Z[x, y];\n"
//                + "f = \\sin(x^2 + \\tg(y^3 + x));\n"
//                + "h= \\D(f, y);\n"
//                + "\\print(h);",
//                // OUT
//                "h = 3y^2*\\cos(x^2+\\tg(y^3+x))/(\\cos(y^3+x))^2;");
//    }
//
//    @Test
//    public void help_5_5_002() {
//        doPageTest("SPACE = Z[x, y];\n"
//                + "f = \\sin(x^2 + \\tg(y^3 + x));\n"
//                + "h = \\D(f);\n"
//                + "\\print(h);",
//                // OUT
//                "h = (2x*\\cos(x^2+\\tg(y^3+x))*(\\cos(y^3+x))^2+\\cos(x^2+\\tg(y^3+x)))/(\\cos(y^3+x))^2;");
//    }
//
////    @Test
////    public void help_5_5_003() {
////        doPageTest("SPACE = Z[x, y, z];\n"
////                + "f = x^8y^4z^9;\n"
////                + "g = \\D(f, [x^2, y^2, z^2]);\n"
////                + "\\print(g);",
////                // OUT
////                "g = 48384z^7y^2x^6;");
////    }
//
//    @Test
//    public void help_5_6_001() {
//        doPageTest("SPACE = Z[x, y, z];\n"
//                + "l1 = \\int(x^6yz + 3x^2y - 2z) d x;\n"
//                + "dl1 = \\D(l1, x);\n"
//                + "l2 = \\int(x^6yz + 3x^2y - 2z) d y;\n"
//                + "dl2 = \\D(l2, y);\n"
//                + "l3 = \\int(x^6yz + 3x^2y - 2z) d z;\n"
//                + "dl3 = \\D(l3, z);\n"
//                + "\\print(l1, dl1, l2, dl2, l3, dl3);",
//                // OUT
//                "l1 = (1/7)zyx^7-2zx+yx^3;\n"
//                + " dl1 = zyx^6-2z+3yx^2;\n"
//                + " l2 = (1/2)zy^2x^6-2zy+(3/2)y^2x^2;\n"
//                + " dl2 = zyx^6-2z+3yx^2;\n"
//                + " l3 = (1/2)z^2yx^6-z^2+3zyx^2;\n"
//                + " dl3 = zyx^6-2z+3yx^2;");
//    }
// 
    @Test
    public void help_5_6_002() {
        doPageTest("SPACE = Q[x];\n"
                + "l = \\int(1/(x^2-5x+6)) d x;\n"
                + "dl = \\D(l,x);\n"
                + "\\print(l, dl);",
                // OUT
                "l = (-2)* \\arctgh(2x-5);\n"
                + " dl =1 /(x^2-5x+6);");
    }

    @Test
    public void help_5_6_003() {
        doPageTest("SPACE = Q[x];\n"
                + "l = \\int(\\exp(x)+\\exp(-x)) d x;\n"
                + "dl = \\D(l,x);\n"
                + "\\print(l, dl);",
                // OUT
                "l = 2*\\sh(x);\n"
                + " dl = 2*\\ch(x);");
    }

    @Test
    public void help_5_6_004() {
        doPageTest("SPACE = Q[x];\n"
                + "l = \\int(x*\\exp(x^2)) d x;\n"
                + "dl = \\D(l,x);\n"
                + "\\print(l, dl);",
                // OUT
                "l = 1/2*\\exp(x^2);\n"
                + "dl = x*\\exp(x^2);");
    }

    @Test
    public void help_5_6_005() {
        Ring ring = new Ring("Z[x]");ring.page  =new Page(ring,true);
        Element resInt = new Integrate().integrate(
                new F("(x*\\ln(x)*\\exp(x)+\\exp(x))/x", ring), new Polynom("x", ring), ring);
        Element b = new F("\\ln(x)*\\exp(x)", ring);
        assertTrue(resInt.equals(b, ring));
    }

    @Test
    public void help_5_6_006() {
        Ring ring = new Ring("R64[x]"); ring.page  =new Page(ring,true);
        Element resInt = new Integrate().integrate(
                new F("(\\ln(x+3)+\\ln(x+2)+\\ln(x+1))", ring), new Polynom("x", ring), ring);
        Element b = new F("(-3x) + 2\\ln(\\abs(x+2)) + \\ln(\\abs(x+1)) + 3\\ln(\\abs(x+3))+ x *\\ln(x+1) + x*\\ln(x+2) + x*\\ln(x+3)", ring);
        Element resEq = new F(F.SUBTRACT, new Element[] {resInt, b}).expand(ring);
        // System.out.println("RES="+resInt);
        boolean RES=resEq.isZero(ring);
        assertTrue(RES);
    }

    @Test
    public void help_5_6_007() {
        doPageTest("SPACE = Q[x];\n"
                + "l = \\int((2x^2+1)^3) d x;\n"
                + "dl = \\D(l,x);\n"
                + "m=\\factor(dl);\n"
                + "\\print(l, m);",
                // OUT
                "l = (8/7)x^7+(12/5)x^5+2x^3+x;\n"
                + "m = ( 2x^2+1)^3;");
    }

    @Test
    public void help_5_7_001() {
        doPageTest("SPACE=Q[x, y, z]; \n"
                + "g=\\ln(x^2*4x);\n"
                + "f=\\Expand(g);\n"
                + "\\print(f);",
                // OUT
                "f = (\\ln(4)+3*\\ln(x));");
    }

    @Test
    public void help_5_7_002() {
        doPageTest("SPACE=Q[x, y, z];\n"
                + "g=\\sin(x^2+4x+2\\pi);\n"
                + "f=\\Expand(g);\n"
                + "\\print(f);",
                // OUT
                "f =(\\cos(x^2)*\\sin(4x)+\\sin(x^2)*\\cos(4x));");
    }

    @Test
    public void help_5_7_003() {
        doPageTest("SPACE=Q[x, y, z];\n"
                + "g=\\cos(\\sin(x)+\\cos(y));\n"
                + "f=\\Expand(g);\n",
                // OUT
                " (\\cos(\\cos(y))*\\cos(\\sin(x))-\\sin(\\cos(y))*\\sin(\\sin(x)))");
    }

    @Test
    public void help_5_7_004() {
        doPageTest("SPACE=Q[x, y, z];\n"
                + "g=\\log_{2}(x)+\\log_{2}(y)-\\log_{2}(xz)+\\lg(y)+\\lg(y)-\\lg(z);\n"
                + "f=\\Factor(g);\n"
                + "\\print(f);",
                // OUT
                "f = (\\lg(y^2/z)-\\log_{2}(z/y)) ;");
    }

    @Test
    public void help_5_7_005() {
        doPageTest("SPACE=Q[x, y, z];\n"
                + "g=16\\sin(x/48)\\cos(x/48)\\cos(x/24)\\cos(x/12)\\cos(x/6);\n"
                + "f=\\Factor(g);\n"
                + "\\print(f);",
                // OUT
                "f = \\sin(1/3x);");
    }

        @Test
    public void help_add() {
        doPageTest( "SPACE=R64[x ];\n"+
                "f= \\log(a,b); " 
                 + "\\print(f);",
                // OUT
                "f=\\log_{a}(b)");
    }
    @Test
    public void help_5_7_006() {
        doPageTest("SPACE=C64[x, y, z];\n"
                + "g=\\ln(1-\\ix) - \\ln(1+\\ix) + \\e^(\\ix) - 2\\e^(-\\ix) + \\sin(x)^2 - \\cos(x)^2;\n"
                + "f=\\Factor(g);\n",
                // OUT
                "(2\\i  * \\sin(x)- (2\\i  * \\arctg(x)+ \\cos(2x)+ \\exp(-\\i x)))");
    }
    @Test
    public void help_5_7_007() {
        doPageTest("SPACE=R64[x, y, z];\n"
                + "g=(\\sin(x+y) + \\sin(x-y))\\cos(x) + (\\sin(x+y) + \\sin(x-y))\\sin(y);\n"
                + "f=\\Expand(g);\n"
                + "u=\\Factor(f);\n"
                + "\\print(f,u);",
                // OUT
                "f = (2*\\cos(x)*\\cos(y)*\\sin(x)+2*\\sin(y)*\\cos(y)*\\sin(x));"
               + " u =(\\cos(y)*\\sin(2x)+\\sin(x)*\\sin(2y));"
        );
    }
}
