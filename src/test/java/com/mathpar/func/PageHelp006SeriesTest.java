/**
* Copyright © 2011 Mathparca Ltd. All rights reserved.
*/

package com.mathpar.func;

import org.junit.jupiter.api.Test;

import static com.mathpar.func.PageTest.*;

public class PageHelp006SeriesTest {
    
        @Test
    public void help_6_1_000() {
        doPageTest("SPACE=R[x, y];\n"
                + "f=\\sum_{a=6}^{\\infty} (y^a\\sin(x a)\\cos(y)a_0);\n"
                + "\\print(f);",
                // OUT
                "f = \\sum_{a=6}^{\\infty} y^a*\\sin(x*a)*\\cos(y)*a_0;\n") ;
    }
            @Test
    public void help_6_1_000a() {
        doPageTest("SPACE=R[x, y];\n"
                + "g=\\sum_{i=4}^{\\infty} (x^i a\\sin(a i x));\n"
                + "\\print(g);",
                // OUT
                "g = \\sum_{i=4}^{\\infty}  x^i*a*\\sin(x*a*i);\n") ;
    }
    
    
//    @Test
//    public void help_6_1_001() {
//        doPageTest("SPACE=R[x, y];\n"
//                + "h=\\seriesAdd(f, g);\n"
//                + "\\print( h);",
//                // OUT
//                "h = (\\sum_{i=4}^{\\infty} (2*(x)^i*y*b*i+(x)^i*a*\\sin(a*i*x))+\\sum_{i=2}^{3}(2.00y*(x)^i*b*i));");
//    }

//    @Test
//    public void help_6_1_002() {
//        doPageTest("SPACE=R[x, y];\n"
//                + "f=\\sum_{i=1}^{\\infty} (x^i y i\\cos(b));\n"
//                + "g=\\sum_{i=2}^{\\infty} (5x^i a\\cos(a x i));\n"
//                + "h=\\seriesSubtract(f, g);\n"
//                + "\\print(f, g, h);",
//                // OUT
//                "f = \\sum_{i=1}^{\\infty} (x)^i*y*i*\\cos(b);\n"
//                + " g = \\sum_{i=2}^{\\infty} 5*(x)^i*a*\\cos(a*x*i);\n"
//                + " h = (\\sum_{i=2}^{\\infty} ((x)^i*y*i*\\cos(b)-5*(x)^i*a*\\cos(a*x*i))+y*(x)^1*\\cos(b));");
//    }
//
//    @Test
//    public void help_6_1_003() {
//        doPageTest("SPACE=R[x, y];\n"
//                + "f=\\sum_{i=0}^{\\infty} (2x^i y b i);\n"
//                + "g=\\sum_{i=2}^{\\infty} (5y^i x^2 b i\\cos(a_1 x));\n"
//                + "h=\\seriesSubtract(f, g);\n"
//                + "\\print(f, g, h);",
//                // OUT
//                "f = \\sum_{i=0}^{\\infty} 2*(x)^i*y*b*i;\n"
//                + "g = \\sum_{i=2}^{\\infty} 5*(y)^i*x^2*b*i*\\cos(a_1*x);\n"
//                + "h = (\\sum_{i=2}^{\\infty} (2*(x)^i*y*b*i-5*(y)^i*x^2*b*i*\\cos(a_1*x))+\\sum_{i=0}^{1}(2.00y*(x)^i*b*i));");
//    }
//
//    @Test
//    public void help_6_1_004() {
//        doPageTest("SPACE=R[x, y];\n"
//                + "f=\\sum_{a=6}^{\\infty} (x^a a_0);\n"
//                + "g=\\sum_{a=9}^{\\infty} (6x^a\\cos(a_1 x));\n"
//                + "h=\\seriesMultiply(f, g);\n"
//                + "\\print(f, g, h);",
//                // OUT
//                "f = \\sum_{a=6}^{\\infty} (x)^a*a_0;\n"
//                + "g = \\sum_{a=9}^{\\infty} 6*(x)^a*\\cos(a_1*x);\n"
//                + "h = \\sum_{a_2=6}^{\\infty} \\sum_{a=9}^{\\infty} (x)^{a_2}*a_0*6*(x)^a*\\cos(a_1*x);");
//    }
//
//    @Test
//    public void help_6_1_005() {
//        doPageTest("SPACE=R[x, y];\n"
//                + "f=\\sum_{a=6}^{\\infty} (y^a\\sin(x a)\\cos(y)a_0);\n"
//                + "g=\\sum_{a=9}^{\\infty} (6y^a\\sin(a x y^2));\n"
//                + "h=\\seriesMultiply(f, g);\n"
//                + "\\print(f, g, h);",
//                // OUT
//                "f = \\sum_{a=6}^{\\infty} (y)^a*\\sin(x*a)*\\cos(y)*a_0;\n"
//                + "g = \\sum_{a=9}^{\\infty} 6*(y)^a*\\sin(a*x*y^2);\n"
//                + "h = \\sum_{a_1=6}^{\\infty} \\sum_{a=9}^{\\infty} (y)^{a_1}*\\sin(x*a_1)*\\cos(y)*a_0*6*(y)^a*\\sin(a*x*y^2);");
//    }

//    @Test
//    public void help_6_1_006() {
//        doPageTest("SPACE=R[x];\n"
//                + "FLOATPOS=5;\n"
//                + "a=\\teilor(\\sin(x), 0, 5);\n"
//                + "c=\\value(a);\n"
//                + " [c,a];",
//                // OUT
//                 "[0.00833x^5-0.16667x^3+x,(((-x^3)*\\!(5)*\\!(1.0)+x^5*\\!(3)*\\!(1.0)+x*\\!(3)*\\!(5))/\\!(3)*\\!(5)*\\!(1.0))]" 
//                 );
//    }
}

 
