/**
* Copyright © 2011 Mathparca Ltd. All rights reserved.
*/

package com.mathpar.func;

import org.junit.jupiter.api.Test;

import static com.mathpar.func.PageTest.doPageTest;

public class PageHelp011ScriptingTest {
    @Test
    public void help_11_1_001() {
        doPageTest("SPACE = Z[];\n"
                + "\\procedure myProc2() {\n"
                + "  d = 4;\n"
                + "  \\print(d);\n"
                + "}\n"
                + "\\procedure myProc(c, d) {\n"
                + "  if (c < d) {\n"
                + "    \\return d;\n"
                + "  } else {\n"
                + "    \\return d + 5;\n"
                + "  }\n"
                + "}\n"
                + "\\myProc2();\n"
                + "a = 10;\n"
                + "c = \\myProc(5 + a, a);\n"
                + "\\print(a, c);",
                // OUT
                "d = 4;\n"
                + "a = 10;\n"
                + " c = 15;");
    }

    @Test
    public void help_11_2_001() {
        doPageTest("SPACE = Z[];\n"
                + "a = 5;\n"
                + "b = 1;\n"
                + "if(b < a) {\n"
                + "  b = b + a;\n"
                + "} else {\n"
                + "  \\print(a, b);\n"
                + "}\n"
                + "if(b < a) {\n"
                + "  b = b + a;\n"
                + "} else {\n"
                + "  \\print(a, b);\n"
                + "}",
                //OUT
                "a = 5;\n"
                + " b = 6;");
    }

    @Test
    public void help_11_2_002() {
        doPageTest("SPACE = Z[];\n"
                + "a = 0;\n"
                + "b = 10;\n"
                + "while(a < b) {\n"
                + "  a = a + 5;\n"
                + "  \\print(a);\n"
                + "}",
                // OUT
                "a = 5;\n"
                + "a = 10;");
    }

    @Test
    public void help_11_2_003() {
        doPageTest("SPACE = Z[];\n"
                + "for (i = 3; i \\le 11; i = i + 5) { \n"
                + "  \\print(i);\n"
                + "}",
                // OUT
                "i = 3;\n"
                + "i = 8;");
    }
}
