/**
* Copyright © 2011 Mathparca Ltd. All rights reserved.
*/

package com.mathpar.func;

import org.junit.platform.suite.api.SelectClasses;
import org.junit.platform.suite.api.Suite;

@SelectClasses({
    PageHelp002IntroTest.class,
    PageHelp003PlotsTest.class,
    PageHelp004ParadigmTest.class,
    PageHelp005FunctionsTest.class,
    PageHelp006SeriesTest.class,
    PageHelp011ScriptingTest.class
})

@Suite

public class PageHelpExamplesTest {
}
