/**
* Copyright © 2011 Mathparca Ltd. All rights reserved.
*/

package com.mathpar.func;

import static com.mathpar.func.PageTest.*;
import com.mathpar.number.Ring;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Interpretation tests.
 */
public class PageInterpretTest {
    private Ring r;
    private Page p;

    @BeforeEach
    public void beforeTest() {
        r = new Ring("Z[x, y, z]");
        p = new Page(r,true);
    }
 
         @Test
    public void checkB_AND() {
        doCheckTest(p, "\\neg((x \\& y) \\& z) \\lor x \\lor z", "x \\lor \\neg (x\\& (z \\& y)) \\lor z "   );

    }
             @Test
    public void checkForFname() {
        doCheckTest(p, "\\hbox{abc}", "\\hbox{abc}"   );

    }
    @Test
    public void interpretSingleVarTwoTimes() {
        String input = "a", expect = "a";
        doPageTest(p, input, expect);
        doPageTest(p, input, expect);
    }

    @Test
    public void interpretSingleVarTwoTimesWithAssignment() {
        doPageTest(p, "a", "a");
        doPageTest(p, "a = 5", "5");
    }

    @Test
    public void interpretProcedureMultipleSectionsSimple() {
        doPageTest(p, 0, "\\procedure p() { \\print(1); } \\p();", "1");
    }

    /**
     * https://bitbucket.org/mathpar/mathpar-archive-2012-2013/issue/33/cant-calculate-simple-task
     */
    @Test
    public void interpretIssue33() {
        String input = "SPACE = Z[x, y];\n"
                + "f = 4x^2 + y^2 - 2x + 6y - 1;\n"
                + "dfx = \\D(f, x);\n"
                + "dfx2 = \\D(dfx, x);\n"
                + "dfy = \\D(f, y);\n"
                + "dfy2 = \\D(dfy, y);\n"
                + "A = \\value(dfx2, [1,1]);\n"
                + "B = 0;\n"
                + "C = \\value(dfy2, [1,1]);\n"
                + "M = [[A, B], [B, C]];\n"
                + "det = \\det(M);\n"
                + "\\print(dfx, dfx2 ,A, M, det);",
                // output

                expect = "\ndfx = 8x-2;\n"
                + " dfx2 = 8;\n"
                + " A = 8;\n"
                + " M = \n"
                + "[[8, 0],\n"
                + " [0, 2]];\n"
                + " det = 16;";
        // Run multiple times.
        doPageTest(p, input, expect);
        doPageTest(p, input, expect);
    }

    /**
     * https://bitbucket.org/mathpar/mathpar/issue/25/o_-doesnt-work
     */
    @Test
    public void interpretIssue25() {
        doPageTest("a = 3; t=\\O_{a};",
                // OUT
                "[0, 0, 0]");
    }  
}
