/**
* Copyright © 2011 Mathparca Ltd. All rights reserved.
*/

package com.mathpar.func;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PageMethodsTest {
    @Test
    public void str2latexListOfMatrices() {
        String expected = " [ \\left(\\begin{array}{cc}1 & \\ 2 \\\\ 3 & \\ 4 \\end{array}\\right) ] ";
        String result = new Page().strToTexStr("[[[1, 2], [3, 4]]]");
        assertEquals(expected, result);
    }
    
    
}
