/**
* Copyright © 2011 Mathparca Ltd. All rights reserved.
*/

package com.mathpar.func;

import com.mathpar.number.Ring;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * @author gennadi
 */
public class PageSetsTest {
    private Ring ring;

    @BeforeEach
    public void beforeTest() {
        ring = new Ring("R64[x, y, z]");
    }

    @Test
    public void SetTest01() {
      //  Page p = new Page(ring);
        com.mathpar.func.PageTest.doPageTest(ring, " \\(1, 3 \\);", "\\(1, 3 \\)");
    }

    @Test
    public void SetTest02() {
        Page p = new Page(ring);
        com.mathpar.func.PageTest.doPageTest(ring,
                "\\[-151,0\\]\\cup \\[1, 3 \\sqrt{5} \\);",
                "\\[(-151),0\\]\\cup \\[1, 3*\\sqrt{5} \\)");
    }

    @Test
    public void SetTest03() {
        Page p = new Page(ring);
        com.mathpar.func.PageTest.doPageTest(ring,
                "\\set([1, 3 ])  \\setminus  \\[2, 3 \\);",
                "\\[1, 2 \\)\\cup\\{3\\}");
    }

    @Test
    public void SetTest06() {
        Page p = new Page(ring);
        com.mathpar.func.PageTest.doPageTest(ring,
                "\\[1, 6 \\)  \\setminus  \\[2, 3 \\];",
                "\\[1, 2 \\)\\cup\\(3, 6 \\)");
    }

    @Test
    public void SetTest07() {
        Page p = new Page(ring);
        com.mathpar.func.PageTest.doPageTest(ring,
                "\\[1, 3 \\]  \\triangle  \\[2, 8 \\);",
                "\\[1, 2 \\)\\cup  \\(3, 8 \\)");
    }

    @Test
    public void SetTest08() {
        Page p = new Page(ring);
        com.mathpar.func.PageTest.doPageTest(ring,
                " \\(-\\infty, 3 \\]\\' ;", "\\(3, \\infty \\)  ");
    }

    @Test
    public void SetTest09() {
        Page p = new Page(ring);
        com.mathpar.func.PageTest.doPageTest(ring,
                " \\{f_{x,y}: -1<x<0, 7<y, 8<z \\} ;",
                " \\{f_{x,y}: -1<x<0, 7<y, 8<z\\}");
    }

    @Test
    public void SetTest10() {
        Page p = new Page(ring);
        com.mathpar.func.PageTest.doPageTest(ring,
                " \\[3,4 \\] - \\[3,4 \\] ;", " \\emptyset");
    }

    @Test
    public void SetTest11() {
        Page p = new Page(ring);
        com.mathpar.func.PageTest.doPageTest(ring,
                " \\[3,4 \\] - \\[3,4 \\) ;", " \\{4\\}");
    }

    @Test
    public void SetTest12() {
        Page p = new Page(ring);
        com.mathpar.func.PageTest.doPageTest(ring,
                " \\[3, 4 \\) - \\[3, 4 \\] ;", " \\{4\\}");
    }

    @Test
    public void SetTest13() {
        Page p = new Page(ring);
        com.mathpar.func.PageTest.doPageTest(ring,
                " \\(3, 4 \\) \\cup \\[3, 4 \\] ;", "\\[3, 4 \\]");
    }

    @Test
    public void SetTest14() {
        Page p = new Page(ring);
        com.mathpar.func.PageTest.doPageTest(ring,
                " \\{ 1 \\} ;", "\\{ 1 \\}");
    }

    @Test
    public void SetTest15() {
        Page p = new Page(ring);
        com.mathpar.func.PageTest.doPageTest(ring,
                "( \\[3, 4 \\) \\cup \\[7, 8 \\] ) \\cup ( \\[3, 4 \\) \\cup \\[7, 8 \\] );", "\\[3, 4 \\) \\cup \\[7, 8 \\]");
    }
        @Test
    public void SetTest16() {
        Page p = new Page(ring);
        com.mathpar.func.PageTest.doPageTest(ring,
                "( \\(-\\infty, 4 \\) \\cup \\[7, 8 \\] ) - ( \\(-\\infty, 4 \\) \\cup \\[7, 8 \\] );", "\\emptyset");
    }
    
            @Test
    public void SetTest17() {
        Page p = new Page(ring);
        com.mathpar.func.PageTest.doPageTest(ring,
                " \\(2, 66 \\)    - \\(2, 66 \\);", "\\emptyset");
    }
}
