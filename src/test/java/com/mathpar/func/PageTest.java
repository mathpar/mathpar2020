/**
* Copyright © 2011 Mathparca Ltd. All rights reserved.
*/

package com.mathpar.func;

import com.mathpar.number.Ring;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.platform.suite.api.SelectClasses;
import org.junit.platform.suite.api.Suite;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * Some "integration" tests of Page.
 */
@SelectClasses({
        PageMethodsTest.class,
        PageArithmeticsTest.class,
        PageFunctionsTest.class,
        PageInterpretTest.class,
        PageHelpExamplesTest.class,
        PageSetsTest.class
})

@Suite

public final class PageTest {
    private Ring r;

    @BeforeEach
    public void beforeTest() {
        r = new Ring("R64[x, y, z]");
    }

    // @Test
    public void checkB_AND() {
        Page p = new Page(r,true);
        doCheckTest(p, "x \\& y", "y \\& x+1"   );

    }
    
    @Test
    public void taskChangeRing() {
        Page p = new Page(r,true);
        doPageTest(p, " \"Task R\" a=1; ", "1");
        doPageTest(p, " \"Task-Q\" a ; ", "1");
        doPageTest(p, " \"Task-Q\" a; ", "1");
    }

    /**
     * Checks if execution of {@code inputSourceCode} doesn't throw an
     * Exception. Usefull for testing input with undetermined output such as
     * generating random objects.
     *
     * @param inputSourceCode source code of tested script.
     */
    public static void pageDoesntThrowException(String inputSourceCode) {
        Page p = new Page(Ring.ringR64xyzt,true);
        p.ring.page=p;
        p.execution(inputSourceCode);
    }

    /**
     * Checks if {@code expectedResult} is equal to result of executing
     * {@code test} script by Page constructed with given Ring {@code ring}.
     *
     * @param inputSourceCode source code of tested script.
     * @param expectedResult  expected result output.
     * @throws Exception
     */
    public static void doPageTest(String inputSourceCode, String expectedResult) {
    
        doPageTest(Ring.ringR64xyzt, inputSourceCode, expectedResult);
    }

    /**
     * Checks if {@code expectedResult} is equal to result of executing
     * {@code test} script by Page constructed with given Ring {@code ring}.
     *
     * @param ring            ring.
     * @param inputSourceCode source code of tested script.
     * @param expectedResult  expected result output.
     */
    public static void doPageTest(Ring ring, String inputSourceCode,
                                  String expectedResult) {    
        Page p = new Page(ring,true); ring.page=p;
        doPageTest(p, 0, inputSourceCode, expectedResult);
    }

    /**
     * Checks if {@code expectedResult} is equal to result of executing
     * {@code test} script by Page {@code p} with given section number
     * {@code sectionNum}.
     *
     * @param p               page.
     * @param sectionNum      section number.
     * @param inputSourceCode source code of tested script.
     * @param expectedResult  expected result output.
     */
    public static void doPageTest(Page p, int sectionNum,
       String inputSourceCode, String expectedResult) {    
        p.execution(inputSourceCode, sectionNum); 
        String result = p.data.section[1].toString(); 
        if (p.ring.exception.length() != 0) {
            fail("Ring.exception: " + p.ring.exception);
        }
        assertEquals(expectedResult.replaceAll("[;\\s]*", ""),
                result.replaceAll("[;\\s]*", ""));
    }

    /**
     * /**
     * Checks if {@code expectedResult} is equal to result of executing
     * {@code test} script by Page {@code p} (section number is 0).
     *
     * @param p               page.
     * @param sectionNum      section number.
     * @param inputSourceCode source code of tested script.
     * @param expectedResult  expected result ouput.
     */
    public static void doPageTest(Page p, String inputSourceCode,
                                  String expectedResult) {
        doPageTest(p, 0, inputSourceCode, expectedResult);
    }

     public static void doCheckTest(Page p,  
                                  String inStr, String dbStr) {    
        boolean bb=p.checkStrs(inStr, dbStr); 
        assertTrue(bb);
    }
    
}
