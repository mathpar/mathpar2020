/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mathpar.func.integration;

import com.mathpar.number.Ring;
import org.junit.jupiter.api.Test;

/**
 *
 * @author c
 */
public class OtherFuncTest {
    
    
        @Test
    public void func1_q() {
        RationalFuncTest.doTest(new Ring("Q[x, y]"), "(y^2+1)*\\exp(y)","y","((y^2*\\exp(y)+ 3* \\exp(y))- 2y*\\exp(y))");
    }
    
    @Test
    public void func1_r() {
        RationalFuncTest.doTest(new Ring("R64[x, y]"), "(y^2+1)*\\exp(y)","y","((y^2*\\exp(y)+ 3* \\exp(y))- 2y*\\exp(y))");
    }
    
    @Test
    public void func2_q() {
        RationalFuncTest.doTest(new Ring("Q[x, y]"), "\\exp(y)*\\exp(y^2)*\\exp(x)*\\exp(y^3)*\\exp(y^4)","x","\\exp(y+y^2+y^3+y^4+x)");
    }
    
    @Test
    public void func2_r() {
        RationalFuncTest.doTest(new Ring("R64[x, y]"), "\\exp(y)*\\exp(y^2)*\\exp(x)*\\exp(y^3)*\\exp(y^4)","x","\\exp(y+y^2+y^3+y^4+x)");
    }
    
    
    @Test
    public void func3_q() {
        RationalFuncTest.doTest(new Ring("Q[x, y]"), "(y^2+1)*\\ln(y)","y","y * \\ln(y)+ 1/3y^3 * \\ln(y)+ (-1/9)y^3 + (-y)");
    }
    
    @Test
    public void func3_r() {
        RationalFuncTest.doTest(new Ring("R64[x, y]"), "(y^2+1)*\\ln(y)","y","((1/3y^3*\\ln(y)+ y*\\ln(y))- (1/9y^3+y))");
    }
    
    @Test
    public void func4_q() {
        RationalFuncTest.doTest(new Ring("Q[x]"), "1/x \\ln(x)","x","\\ln(x)^2/2");
    }
    
    @Test
    public void func4_r() {
        RationalFuncTest.doTest(new Ring("R64[x]"), "1/x \\ln(x)","x","\\ln(x)^2/2");
    }
    
    @Test
    public void func5_q() {
        RationalFuncTest.doTest(new Ring("Q[x]"), "\\exp(-x)","x","-\\exp(-x)");
    }
    
    @Test
    public void func5_r() {
        RationalFuncTest.doTest(new Ring("R64[x]"), "\\exp(-x)","x","-\\exp(-x)");
    }
    
    
    @Test
    public void func6_q() {
        RationalFuncTest.doTest(new Ring("Q[x]"), "(x + \\exp(x))^2","x","((1/2* \\exp(2x)+ 2x * \\exp(x)+ 1/3x^3)- 2* \\exp(x))");
    }
    
    @Test
    public void func6_r() {
        RationalFuncTest.doTest(new Ring("R64[x]"), "(x + \\exp(x))^2","x","((1/2* \\exp(2x)+ 2x * \\exp(x)+ 1/3x^3)- 2* \\exp(x))");
    }
    
    @Test
    public void func7_q() {
        RationalFuncTest.doTest(new Ring("Q[x]"), "2*\\exp(x)/(\\exp(4x) - 1)","x","-1* (\\arctg(\\exp(x))- 1/2* \\ln(\\abs(\\sh(1/2x)))+ 1/2* \\ln(\\abs(\\ch(1/2x))))");
    }
    
    @Test
    public void func7_r() {
        RationalFuncTest.doTest(new Ring("R64[x]"), "2*\\exp(x)/(\\exp(4x) - 1)","x","-1* (\\arctg(\\exp(x))- 1/2* \\ln(\\abs(\\sh(1/2x)))+ 1/2* \\ln(\\abs(\\ch(1/2x))))");
    }
    
    @Test
    public void func8_q() {
        RationalFuncTest.doTest(new Ring("Q[x]"), "\\sqrt(\\ln(x))/x","x","2/3 * \\ln(x)^(3/2)");
    }
    
    @Test
    public void func8_r() {
        RationalFuncTest.doTest(new Ring("R64[x]"), "\\sqrt(\\ln(x))/x","x","2/3 * \\ln(x)^(3/2)");
    }
    
    @Test
    public void func9_q() {
        RationalFuncTest.doTest(new Ring("Q[x]"), "\\sqrt(x) + 1/\\sqrt(1 - x^2) + \\sin(x)","x","(2/3 * x^{(3/2)}- (\\cos(x)+ 2 * \\arctg(((-x-1) / (x-1))^{(1/2)})))");
    }
    
    @Test
    public void func9_r() {
        RationalFuncTest.doTest(new Ring("R64[x]"), "\\sqrt(x) + 1/\\sqrt(1 - x^2) + \\sin(x)","x","(0.6666666666666667 * x^{1.5}- (\\cos(x)+ 2 * \\arctg(((-x-1) / (x-1))^{0.5})))");
    }
    
//    @Test
//    public void func10_q() {
//        RationalFuncTest.doTest(new Ring("Q[x]"), "5/\\sqrt(3 - 2*x^2)","x","5 * 2^{(1/2)}* \\arctg((-1/2) * ((6^{(1/2)}+ 2x)/ (1/2 * 6^{(1/2)}+ (-x)))^{(1/2)}* 2^{(1/2)})");
//    }
    
//    @Test
//    public void func10_r() {
//        RationalFuncTest.doTest(new Ring("R64[x]"), "5/\\sqrt(3 - 2*x^2)","x","7.07106781187 * \\arctg((-1/2) * ((2.44948974278 + 2x)/ (1/2 * 2.44948974278+ (-x)))^{(1/2)}* 1.41421356237)");
//    }
 
    @Test
    public void func11_q() {
        RationalFuncTest.doTest(new Ring("Q[x]"), "\\log(\\pi, x)","x","(x * \\ln(x)- x)/ \\ln(\\pi)");
    }
    
    @Test
    public void func11_r() {
        RationalFuncTest.doTest(new Ring("R64[x]"), "\\log(\\pi, x)","x","(x * \\ln(x)- x)/ \\ln(\\pi)");
    }

    @Test
    public void func12_q() {
        RationalFuncTest.doTest(new Ring("Q[x]"), "\\log(5, x)","x","(x * \\ln(x)- x)/ \\ln(5)");
    }
    
    @Test
    public void func12_r() {
        RationalFuncTest.doTest(new Ring("R64[x]"), "\\log(5, x)","x","(x * \\ln(x)- x)/ \\ln(5)");
    }

    
    @Test
    public void func13_q() {
        RationalFuncTest.doTest(new Ring("Q[x]"), "\\pi^x","x","(\\pi^x)/\\ln(\\pi)");
    }
    
    @Test
    public void func13_r() {
        RationalFuncTest.doTest(new Ring("R64[x]"), "\\pi^x","x","(\\pi^x)/\\ln(\\pi)");
    }

    @Test
    public void func14_q() {
        RationalFuncTest.doTest(new Ring("Q[x]"), "5^x","x","(5^x)/\\ln(5)");
    }
    
    @Test
    public void func14_r() {
        RationalFuncTest.doTest(new Ring("R64[x]"), "5^x","x","(5^x)/\\ln(5)");
    }

    @Test
    public void func15_q() {
        RationalFuncTest.doTest(new Ring("Q[x]"), "(\\ln(x) + 1)*x^x","x","x^x");
    }
    
    @Test
    public void func15_r() {
        RationalFuncTest.doTest(new Ring("R64[x]"), "(\\ln(x) + 1)*x^x","x","x^x");
    }
    
    @Test
    public void func16_q() {
        RationalFuncTest.doTest(new Ring("Q[x]"), "\\exp(2x) + \\ln(x*\\exp(2x))","x","x * \\ln(x)+ 1/2 * \\exp(2x)+ x^2 + (-x)");
    }
    
    @Test
    public void func16_r() {
        RationalFuncTest.doTest(new Ring("R64[x]"), "\\exp(2x) + \\ln(x*\\exp(2x))","x","x * \\ln(x)+ 1/2 * \\exp(2x)+ x^2 + (-x)");
    }
    
    @Test
    public void func17_q() {
        RationalFuncTest.doTest(new Ring("Q[x]"), "(x + 1/x) \\ln(x)","x","(1/2 * (\\ln(x))^2+ 1/2x^2 * \\ln(x))- 1/4x^2");
    }
    
    @Test
    public void func17_r() {
        RationalFuncTest.doTest(new Ring("R64[x]"), "(x + 1/x) \\ln(x)","x","(1/2 * (\\ln(x))^2+ 1/2x^2 * \\ln(x))- 1/4x^2");
    }
    
   //     @Test
  //  public void func18_q() {
  //      RationalFuncTest.doTest(new Ring("Q[x]"), "1/(\\exp(x)(\\exp(x)+1))","x"," (\\ln(\\abs(\\ch(1/2x)))- (\\exp(-x)+ 1/2x)) ");
  //  }
    
    @Test
    public void func18_r() {
        RationalFuncTest.doTest(new Ring("R64[x]"), "1/(\\exp(x)(\\exp(x)+1))","x"," (\\ln(\\abs(\\ch(0.5x)))- (\\exp(-x)+ 0.5x)) ");
    }
    
    @Test
    public void func19_q() {
        RationalFuncTest.doTest(new Ring("Q[x]"), "\\ln(\\sqrt(x)) + \\cubrt(\\exp(x))","x"," ((3 * \\exp(1/3x)+ 1/2x * \\ln(x))- 1/2x) ");
    }
    
    @Test
    public void func19_r() {
        RationalFuncTest.doTest(new Ring("R64[x]"), "\\ln(\\sqrt(x)) + \\cubrt(\\exp(x))","x"," ((3 * \\exp(0.3333333333333333333333x)+ 0.5x * \\ln(x))- 0.5x) ");
    }
    
    @Test
    public void func20_q() {
        RationalFuncTest.doTest(new Ring("Q[x]"), "\\ln((x + 1) * \\sqrt(x/\\cubrt(x+1))/(x - 1)) - (5/6)*\\ln(x+1) + \\ln(x-1)","x"," 1/2x * \\ln(x)- 1/2x ");
    }
    
    @Test
    public void func20_r() {
        RationalFuncTest.doTest(new Ring("R64[x]"), "\\ln((x + 1) * \\sqrt(x/\\cubrt(x+1))/(x - 1)) - (5/6)*\\ln(x+1) + \\ln(x-1)","x"," 0.5x * \\ln(x)- 0.5x ");
    }
    
    @Test
    public void func21_q() {
        RationalFuncTest.doTest(new Ring("Q[x]"), "1/\\sqrt(x^2 + 1)","x"," \\ln(\\abs((x^2+1)^{(1/2)}+ x)) ");
    }
    
//    @Test
//    public void func21_r() {
//        RationalFuncTest.doTest(new Ring("R64[x]"), "1/\\sqrt(x^2 + 1)","x","\\ln(\\abs((x^2+1)^{(0.5)}+ x))");
//    }
    
    @Test
    public void func22_q() {
        RationalFuncTest.doTest(new Ring("Q[x]"), "x*\\sqrt(x + 1)","x","(2/5 * \\abs(x+1)* (x+1)^{(3/2)}- 2/3 * (x+1)^{(3/2)})");
    }
    
    @Test
    public void func22_r() {
        RationalFuncTest.doTest(new Ring("R64[x]"), "x*\\sqrt(x + 1)","x","(0.4 * \\abs(x+1)* (x+1)^{(1.5)}- 0.6666666666666667 * (x+1)^{(1.5)})");
    }
    
}
