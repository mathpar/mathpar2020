/**
* Copyright © 2011 Mathparca Ltd. All rights reserved.
*/

package com.mathpar.func.integration;

import com.mathpar.func.F;
import com.mathpar.func.Integrate;
import com.mathpar.func.Page;
import com.mathpar.number.Element;
import com.mathpar.number.Ring;
import com.mathpar.polynom.Polynom;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;
/**
 *
 * @author gennadi
 */
public class RationalFuncTest {

    public static void doTest(Ring ring, String  inp, String  arg, String  out) {
        ring.page=new Page(ring);
        F f=new F(inp,ring); Polynom x= new Polynom(arg, ring); F fO=new F(out,ring); 
        Element res=new Integrate().integrate(f, x, ring);
        fO = (F) fO.toNewRing(ring);
        res = res.toNewRing(ring);
      Element eee=  ring.CForm.InputForm((F)res.subtract(fO, ring),ring.page, ring.page.expr );
    Element sub=eee.factor(ring);//.Expand(ring);
        assertTrue(sub.isZero(ring));
    }

    @Test
    public void pol1_z() {
        doTest(new Ring("Q[z ]"), "z^2","z","z^3/3");
    }
    
     @Test
    public void pol2_x_z() {
        doTest(new Ring("Q[z,x ]"), "z^2x^2","z","x^2 z^3/3");
    }
    

      @Test
    public void fraction_q01() {
        doTest(new Ring("Q[x]"), "(x+2)/(x^2+4x+2)","x","1/2*\\ln(\\abs(x^2+4x+2))");
    }
    
    
    @Test
    public void fraction_q02() {
        doTest(new Ring("Q[x]"), "1/(x^3+x)","x","\\ln(\\abs(x))- 1/2* \\ln(\\abs(x^2+1))");
    }
  
    @Test
    public void fraction_r02() {
        doTest(new Ring("R64[x]"), "1/(x^3+x)","x","\\ln(\\abs(x))- 1/2* \\ln(\\abs(x^2+1))");
    }
    
    @Test
    public void fraction_q03() {
        doTest(new Ring("Q[x]"), "1/(x^2+1)","x","\\arctg(x)");
    }
    
    @Test
    public void fraction_r03() {
        doTest(new Ring("R64[x]"), "1/(x^2+1)","x","\\arctg(x)");
    }
    
    @Test
    public void fraction_q04() {
        doTest(new Ring("Q[x]"), "3/(x+2)","x","3* \\ln(\\abs(x+2))");
    }
  
    @Test
    public void fraction_r04() {
        doTest(new Ring("R64[x]"), "3/(x+2)","x","3* \\ln(\\abs(x+2))");
    }
    
    
    @Test
    public void fraction_q05() {
        doTest(new Ring("Q[x]"), "3x/(x+2)","x","(3x - 6* \\ln(\\abs(x+2)))");
    }
  
    @Test
    public void fraction_r05() {
        doTest(new Ring("R64[x]"), "3x/(x+2)","x","(3x - 6* \\ln(\\abs(x+2)))");
    }
    
    @Test
    public void fraction_q06() {
        doTest(new Ring("Q[x]"), "(3x+4)/(x+2)","x","(3x - 2* \\ln(\\abs(x+2)))");
    }
  
    @Test
    public void fraction_r06() {
        doTest(new Ring("R64[x]"), "(3x+4)/(x+2)","x","(3x - 2* \\ln(\\abs(x+2)))");
    }
    
    @Test
    public void fraction_q07() {
        doTest(new Ring("Q[x]"), "1/(x^2 + x + 1)","x","2/3* 3^{(1/2)}* \\arctg(2/3x * 3^{(1/2)}+ 1/3* 3^{(1/2)})");
    }
  
    @Test
    public void fraction_r07() {
        doTest(new Ring("R64[x]"), "1/(x^2 + x + 1)","x","1.1547005383792515* \\arctg( (x+0.5)/0.8660254037844388)");
    }
    
    
    @Test
    public void fraction_q08() {
        doTest(new Ring("Q[x]"), "(4x + 5)/(x^2 + 2x + 3)","x","(2* \\ln(\\abs(x^2+2x+3))+ 1/2* 2^{(1/2)}* \\arctg(1/2x * 2^{(1/2)}+ 1/2* 2^{(1/2)}))");
    }
    
    @Test
    public void fraction_r08() {
        doTest(new Ring("R64[x]"), "(4x + 5)/(x^2 + 2x + 3)","x","(2* \\ln(\\abs(x^2+2x+2.9999999999999996))+ 0.7071067811865476* \\arctg( (x+1)/1.414213562373095))");
    }
    
    
    @Test
    public void fraction_q09() {
        doTest(new Ring("Q[x]"), "(4x^2 + 5x + 6)/(x^2 + 2x + 3)","x","(4x - (((-3/2)* 2^{(1/2)}* (-1)* \\arctg(1/2x * 2^{(1/2)}+ 1/2* 2^{(1/2)}))+ 3/2* \\ln(\\abs(x^2+2x+3))))");
    }
    
    @Test
    public void fraction_r09() {
        doTest(new Ring("R64[x]"), "(4x^2 + 5x + 6)/(x^2 + 2x + 3)","x","(4x - (2.1213203435596424* \\arctg( (x+1)/1.4142135623730951)+ 1.5* \\ln(\\abs(x^2+2x+3.0000000000000004))))");
    }
    
}
