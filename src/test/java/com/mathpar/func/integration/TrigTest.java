/**
* Copyright © 2011 Mathparca Ltd. All rights reserved.
*/

package com.mathpar.func.integration;

import com.mathpar.number.Ring;
import org.junit.jupiter.api.Test;

/**
 *
 * @author gennadi
 */
public class TrigTest {
    
    
    
    
    @Test
    public void sin2x_q() {
        RationalFuncTest.doTest(new Ring("Q[x]"), "\\sin(2x+3)","x"," (-1/2)* \\cos(2x+3)");
    }
 
    //@Test
    // public void sin2x_r() {
  //      RationalFuncTest.doTest(new Ring("R64[x]"), "\\sin(2x+3)","x"," (-1/2)* \\cos(2x+3)");
  //  }
   //  CHECK PLEASE at test TrigTest.func2_r  --   ERROR  at 1589 of Integrate.java  ************



    @Test
    public void cos2x_q() {
        RationalFuncTest.doTest(new Ring("Q[x]"), "\\cos(2x+3)","x","1/2* \\sin(2x+3)");
    }
 
    @Test
    public void cos2x_r() {
        RationalFuncTest.doTest(new Ring("R64[x]"), "\\cos(2x+3)","x","1/2* \\sin(2x+3)");
    }   
    
    @Test
    public void func1_q() {
        RationalFuncTest.doTest(new Ring("Q[x]"), "\\sin(x)^2\\cos(x)^2","x"," 1/8x-1/32* \\sin(4x)");
    }
    
    @Test
    public void func1_r() {
        RationalFuncTest.doTest(new Ring("R64[x]"), "\\sin(x)^2\\cos(x)^2","x"," 1/8x-1/32* \\sin(4x)");
    }
    
    @Test
    public void func2_q() {
        RationalFuncTest.doTest(new Ring("Q[x]"), "\\sin(x)\\exp(\\cos(x))","x","-1*\\exp(\\cos(x))");
    }
    
//    @Test
//    public void func2_r() {
//        RationalFuncTest.doTest(new Ring("R64[x]"), "\\sin(x)\\exp(\\cos(x))","x"," -1* \\exp(\\cos(x))");
//    }
    
    @Test
    public void func3_q() {
        RationalFuncTest.doTest(new Ring("Q[x]"), "\\ctg(2x+3)","x","1/2* \\ln(\\abs(\\sin(2x+3)))");
    }
    
    @Test
    public void func3_r() {
        RationalFuncTest.doTest(new Ring("R64[x]"), "\\ctg(2x+3)","x","1/2* \\ln(\\abs(\\sin(2x+3)))");
    }
    
    @Test
    public void func4_q() {
        RationalFuncTest.doTest(new Ring("Q[x]"), "\\tg(2x+3)","x","(-1/2)* \\ln(\\abs(\\cos(2x+3)))");
    }
    
    @Test
    public void func4_r() {
        RationalFuncTest.doTest(new Ring("R64[x]"), "\\tg(2x+3)","x","(-1/2)* \\ln(\\abs(\\cos(2x+3)))");
    }
    
    @Test
    public void func5_q() {
        RationalFuncTest.doTest(new Ring("Q[x]"), "\\exp(x)*\\sin(x)","x","1/2* \\exp(x)* \\sin(x)- 1/2* \\exp(x)* \\cos(x)");
    }
    
    @Test
    public void func5_r() {
        RationalFuncTest.doTest(new Ring("R64[x]"), "\\exp(x)*\\sin(x)","x","1/2* \\exp(x)* \\sin(x)- 1/2* \\exp(x)* \\cos(x)");
    }
    
    @Test
    public void func6_q() {
        RationalFuncTest.doTest(new Ring("Q[x]"), "1/\\cos(x)^2","x","\\tg(x)");
    }
    
//    @Test
//    public void func6_r() {
//        RationalFuncTest.doTest(new Ring("R64[x]"), "1/\\cos(x)^2","x","\\tg(x)");
//    }
//
    @Test
    public void func7_q() {
        RationalFuncTest.doTest(new Ring("Q[x]"), "1/\\sin(x)^2","x","-\\ctg(x)");
    }
    
//    @Test
//    public void func7_r() {
//        RationalFuncTest.doTest(new Ring("R64[x]"), "1/\\sin(x)^2","x","-\\ctg(x)");
//    }
//
    @Test
    public void func8_q() {
        RationalFuncTest.doTest(new Ring("Q[x]"), "1/(1-\\cos(x))","x","-\\ctg(x/2)");
    }
    
    @Test
    public void func8_r() {
        RationalFuncTest.doTest(new Ring("R64[x]"), "1/(1-\\cos(x))","x","-\\ctg(x/2)");
    }
    
    @Test
    public void func9_q() {
        RationalFuncTest.doTest(new Ring("Q[x]"), "\\tg(x)^2","x","(\\sin(x) - x*\\cos(x) - \\i*\\cos(x))/\\cos(x)");
    }
    
    @Test
    public void func9_r() {
        RationalFuncTest.doTest(new Ring("R64[x]"), "\\tg(x)^2","x","(\\sin(x) - x*\\cos(x) - \\i*\\cos(x))/\\cos(x)");
    }
    
    @Test
    public void func10_q() {
        RationalFuncTest.doTest(new Ring("Q[x]"), "\\sin(x)*\\tg(\\cos(x))","x","\\ln(\\abs(\\cos(\\cos(x))))");
    }
    
    @Test
    public void func10_r() {
        RationalFuncTest.doTest(new Ring("R64[x]"), "\\sin(x)*\\tg(\\cos(x))","x","\\ln(\\abs(\\cos(\\cos(x))))");
    }
}
