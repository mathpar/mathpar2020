package com.mathpar.func.parser;

import com.mathpar.func.F;
import com.mathpar.func.LimitOf;
import com.mathpar.number.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LimitOfTest {

    @Test
    public void limitOfCubicFunction() {
        Ring ring = new Ring("R64[x]");
        ring.setAccuracy(50);
        LimitOf limitOf = new LimitOf(ring);

        // (x^3 - 4) / (x - 2) при x -> 1
        F function = new F("(x^3 - 4) / (x - 2)", ring);
        Element result = limitOf.Limit(function, new Element[]{NumberR64.valueOf("1")});
        Element expected = NumberR64.valueOf("3");
        assertEquals(expected, result);
    }

    @Test
    public void limitOfSquareFunction() {
        Ring ring = new Ring("R64[x]");
        ring.setAccuracy(50);
        LimitOf limitOf = new LimitOf(ring);

        //(x^2) при x -> 2
        F function = new F("x^2", ring);
        Element result = limitOf.Limit(function, new Element[]{new NumberR64(2)});
        Element expected = new NumberR64(4);
        assertEquals(expected, result);
    }

    @Test
    public void limitOfLinearFunction() {
        Ring ring = new Ring("R64[x]");
        ring.setAccuracy(50);
        LimitOf limitOf = new LimitOf(ring);

        //(2x + 3) при x -> 5
        F function = new F("2*x + 3", ring);
        Element result = limitOf.Limit(function, new Element[]{new NumberR64(5)});
        Element expected = new NumberR64(13);
        assertEquals(expected, result);
    }

    //@Test
    public void limitOfSquareRootFunction() {
        Ring ring = new Ring("R64[x]");
        ring.setAccuracy(50);
        LimitOf limitOf = new LimitOf(ring);

        // sqrt(x) при x -> 16
        F function = new F("sqrt(x)", ring);
        Element result = limitOf.Limit(function, new Element[]{new NumberR64(16)});
        Element expected = new NumberR64(4);
        assertEquals(expected, result);
    }

    //@Test
    public void limitOfExponentialFunction() {
        Ring ring = new Ring("R64[x]");
        ring.setAccuracy(50);
        LimitOf limitOf = new LimitOf(ring);

        //(e^x) при x -> 2
        F function = new F("exp(x)", ring);
        Element result = limitOf.Limit(function, new Element[]{new NumberR64(2)});
        Element expected = new NumberR64(Math.exp(2));
        assertEquals(expected, result);
    }

    //@Test
    public void limitOfNaturalLogarithmFunction() {
        Ring ring = new Ring("R64[x]");
        ring.setAccuracy(50);
        LimitOf limitOf = new LimitOf(ring);

        // ln(x) при x -> 1
        F function = new F("ln(x)", ring);
        Element result = limitOf.Limit(function, new Element[]{NumberR64.ONE});
        Element expected = NumberR64.ZERO;
        assertEquals(expected, result);
    }

    //@Test
    public void limitOfSinFunction() {
        Ring ring = new Ring("R64[x]");
        ring.setAccuracy(50);
        LimitOf limitOf = new LimitOf(ring);

        //sin(x) при x -> 0
        F function = new F("sin(x)", ring);
        Element result = limitOf.Limit(function, NumberR64.ZERO);
        Element expected = NumberR64.ONE;
        assertEquals(expected, result);
    }

    @Test
    public void limitOfPolynomialFunction() {
        Ring ring = new Ring("R[x]");
        ring.setAccuracy(50);
        LimitOf limitOf = new LimitOf(ring);

        // Тестуємо ліміт (2x^3 + 1 - 15x + x^2) при x -> 1
        F function = new F("2x^3 + 1 - 15x + x^2", ring);
        Element result = limitOf.Limit(function, ring.numberONE);
        Element expected = new NumberR(-11);
        assertEquals(expected, result);
    }
}
