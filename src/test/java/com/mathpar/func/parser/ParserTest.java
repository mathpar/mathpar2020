/**
* Copyright © 2011 Mathparca Ltd. All rights reserved.
*/

package com.mathpar.func.parser;

import org.junit.platform.suite.api.SelectClasses;
import org.junit.platform.suite.api.Suite;

/**
 * Parser test suite.
 *
 * @author ivan
 */
@SelectClasses({
    com.mathpar.func.parser.FunctionTest.class,
    com.mathpar.func.parser.FunctionSpecTest.class,
    com.mathpar.func.parser.PolynomialAndNumberTest.class,
    com.mathpar.func.parser.SymbolTest.class})
@Suite
public class ParserTest {
}
