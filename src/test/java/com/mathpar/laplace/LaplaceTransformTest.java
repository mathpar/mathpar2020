/**
* Copyright © 2011 Mathparca Ltd. All rights reserved.
*/

package com.mathpar.laplace;

import com.mathpar.func.F;
import com.mathpar.laplaceTransform.SystemLDE;
import com.mathpar.number.Element;
import com.mathpar.number.Ring;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

/**
 *
 * @author mixail
 */
public class LaplaceTransformTest {
    @Disabled("Don't spam with debug logs.")
    @Test
    public void laplace() throws Exception {
        Ring ring = new Ring("R64[t]");
        F dif1 = new F("\\systLDE(\\d(x,t,2)+\\d(x,t)-\\d(y,t)=1,\\d(x,t)+x-\\d(y,t,2)=1+4\\exp(t))", ring);
        F initC1 = new F("\\initCond(\\d(x,t,0,0)=1,\\d(x,t,0,1)=2,\\d(y,t,0,0)=0,\\d(y,t,0,1)=1)", ring);
        Element res1 = new SystemLDE().solveSystemLDE(dif1, initC1, ring);//для системы
        ////////////////////////////////////////////////////////////////////////
        F dif2 = new F("\\systLDE(3\\d(x,t)+2*x+\\d(y,t)=1,\\d(x,t)+4\\d(y,t)+3*y=0)", ring);
        F initC2 = new F("\\initCond(\\d(x,t,0,0)=0,\\d(x,t,0,1)=0,\\d(y,t,0,0)=0,\\d(y,t,0,1)=0)", ring);
        Element res2 = new SystemLDE().solveSystemLDE(dif2, initC2, ring);//для системы
        ////////////////////////////////////////////////////////////////////////
        F dif3 = new F("\\systLDE(\\d(x, t)+x-2y=0, \\d(y, t)+x+4y=0)", ring);
        F initC3 = new F("\\initCond(\\d(x, t, 0, 0)=1, \\d(y, t, 0, 0)=1)", ring);
        Element res3 = new SystemLDE().solveSystemLDE(dif3, initC3, ring);//для системы
        ////////////////////////////////////////////////////////////////////////
        F dif4 = new F("\\systLDE(\\d(x, t)+2x+2y=10\\exp(2t), \\d(y, t)-2x+y=7\\exp(2t))", ring);
        F initC4 = new F("\\initCond(\\d(x, t, 0, 0)=1, \\d(y, t, 0, 0)=3)", ring);
        Element res4 = new SystemLDE().solveSystemLDE(dif4, initC4, ring);//для системы
        ////////////////////////////////////////////////////////////////////////
        F dif5 = new F("\\systLDE(\\d(x, t)-y+z=0, -x-y+\\d(y, t)=0, -x-z+\\d(z, t)=0)", ring);
        F initC5 = new F("\\initCond(\\d(x, t, 0, 0)=1, \\d(y, t, 0, 0)=2, \\d(z, t, 0, 0)=3)", ring);
        Element res5 = new SystemLDE().solveSystemLDE(dif5, initC5, ring);//для системы
    }
}
