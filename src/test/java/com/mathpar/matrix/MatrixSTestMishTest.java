/**
* Copyright © 2011 Mathparca Ltd. All rights reserved.
*/

package com.mathpar.matrix;

import com.mathpar.number.Element;
import com.mathpar.number.Ring;
import com.mathpar.polynom.Polynom;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 *
 * @author gennadi
 */
public class MatrixSTestMishTest {
    
 
    /**
     * @param args the command line arguments
     */
    
//     @Test
//    public void  misha16() {
//        Ring ring = new Ring("R[t]");
//        ring.setMachineEpsilonR(50);
//        ring.setDefaultRing();
//        int n = 32;
//        Element[][] A = new Element[n][];
//        for (int i = 0; i < n; i++) {
//            A[i] = new Element[n];
//            for (int j = 0; j < n; j++) {
//                if (j % 2 == 0) {
//                    if (i == j) {
//                        A[i][j] = new Polynom("t", ring);
//                    } else {
//                        A[i][j] = new Polynom("1", ring);
//                    }
//                } else {
//                    if (i == j) {
//                        A[i][j] = new Polynom("-t", ring);
//                    } else {
//                        A[i][j] = new Polynom("-1", ring);
//                    }
//                }
//            }
//        }
//        //Преобразование матрицы полиномов в объект MatrixS
//        MatrixS matrix = new MatrixS(A, ring);
//        //Получение присоединенной матрицы
//        MatrixS adjMatrix = matrix.adjoint(ring);
//    //    System.out.println("inp="+matrix);
//    //    System.out.println("result="+adjMatrix);
//        assertTrue("Size=n:",
//                (adjMatrix.size==n));
//    }
}