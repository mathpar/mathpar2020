/**
* Copyright © 2011 Mathparca Ltd. All rights reserved.
*/

package com.mathpar.matrix;

import com.mathpar.number.Element;
import com.mathpar.number.NumberZ;
import com.mathpar.number.Ring;
import java.util.Random;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.mathpar.func.PageTest.doPageTest;
import static com.mathpar.matrix.Table.fromString;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static com.mathpar.func.PageTest.doPageTest;

/**
 *
 * @author gennadi
 */
public class PageIndecesSea {
 
    private Ring r;

    @BeforeEach
    public void beforeTest() {       
        r = new Ring("R64[x]");
    }
 
//       @Test
//    public void first() {
//        doPageTest(r,
//    "A=[[X,Y],[1,2],[3,4]];B=\\Table(A, ['AA','BB']);", "??");}
//    
//          @Test
//    public void R64_00() {
//        doPageTest(r, "f_{a}= 2x; u=f_{a}; \\D(u) ", "2.0");
//    }
//    @Test
//    public void R64_01() {
//        doPageTest(r, " t=1; h_{1,j,k}=4;\\print(h_{t, j, k})", "h_{t,j,k}=4.0");}
//   
//        @Test
//    public void R64_02() {
//        doPageTest(r, "i=t;j=h;a=h_{i,j};\\print(a)", "a=h_{t,h}");}
// 
//            @Test
//    public void R64_04() {
//        doPageTest(r, "h_{k+1}=7;  \\print(h_{k+1})", "h_{k+1}=7.0");}
// 
//      @Test
//    public void R64_01() {
//        doPageTest(r, "j=5; h_{1,j}=6; k=h_{1,j}+j\\print(a)", "a=h_{1,j}");}
//    
    
    
     @Test
    public void tanR641() {
        doPageTest(r, "t=\\table([[1,2],[3,4]],['a','b']);  ", 
             "[[\\hbox{a},\\hbox{b}],[1,3],[2,4]]"  );}
 

 
//            
//    public void  tableFromString() throws Exception {
//        NumberZ z = NumberZ.MINUS_ONE;
//        Element[][] m= MatrixS.randomScalarArr2d(5, 2, 100, new int[]{2},new Random(),  r);
//        Table table =  new Table(new MatrixD(m), new String[]{"t (с)","U (В)"});
//        String st="   ";
//        Table table1= Table.fromString(st);
//        
//        
//        assertArrayEquals(new String[] {"t (с)", "U (В)"}, table.axesSign);
//    }
    
    
}
