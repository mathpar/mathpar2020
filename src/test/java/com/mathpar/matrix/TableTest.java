/**
* Copyright © 2011 Mathparca Ltd. All rights reserved.
*/

package com.mathpar.matrix;

import com.mathpar.number.NumberZ;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static com.mathpar.matrix.Table.fromString;
import static org.junit.jupiter.api.Assertions.*;

public class TableTest {
    public TableTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of fromString method, of class Table.
     */
    @Test
    public void testFromStringHeader() throws Exception {
        NumberZ z = NumberZ.MINUS_ONE;
        Table table = fromString("t, с	U, В\n"
                + "0.1	-0.02\n"
                + "0.3	-0.02\n"
                + "0.5	-0.02\n"
                + "0.7	-0.02\n"
                + "0.9	0");
        assertArrayEquals(new String[] {"t (с)", "U (В)"}, table.axesSign);
    }
}
