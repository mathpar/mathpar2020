/**
* Copyright © 2011 Mathparca Ltd. All rights reserved.
*/

package com.mathpar.number;
import com.mathpar.func.F;
import com.mathpar.func.Fname;
import com.mathpar.func.*;
import com.mathpar.polynom.FactorPol;
import com.mathpar.polynom.Polynom;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.MatcherAssert.assertThat;

public class ElementTest {
    @Test
    public void arrayGCDsingleElement() {
        Ring ring = Ring.ringZxyz;
        Element[] arr = new Element[]{new NumberZ("5")};
        assertThat("GCD of single element should return this element",
                new NumberZ("5").equals(Element.arrayGCD(arr, ring), ring));
    }

    @Test
    public void expandFnameOrIdExpandsNotOnlyRoot() {
        Element in = new F(F.ID, new Fname("some name", new NumberZ("42")));
        Element out = in.ExpandFnameOrId();
        assertThat(out, instanceOf(NumberZ.class));
    }

    @Test
    public void expandFnameOrIdDoesntTouchUsualFunction() {
        Ring ring = new Ring("R[x]");
        Element in = new F(F.SIN, new Element[]{
                new F(F.ADD, new Polynom("x^2", ring),
                        new F(F.COS, new Polynom("x", ring)))
        });
        Element out = in.ExpandFnameOrId();
        System.out.println("in="+in);System.out.println("out="+out);
        assertThat("Function without Fname or ID didn't change", out.equals(in, ring));
    }

    @Test
    public void powNumberFractionalDegree() {
       //  TODO: split this to separate tests.
        Ring r = new Ring("Q[x]");
        Page p=new Page(r,true); r.page=p;
        Element in = new NumberZ("5");
        Element out = in.pow(r.numberZERO(), r);
        assertThat("A^0 = 1", out.equals(r.numberONE(), r));
        out = in.pow(r.numberONE(), r);
        assertThat("A^1 = A", out.equals(in, r));
        out = in.pow(new Fraction(new NumberZ("1"), new NumberZ("4")), r);
        assertThat("A^{1/b} = sqrt[b](A)",
                out.equals(new F(F.ROOTOF, in, new NumberZ("4")), r));
        out = in.pow(new Fraction(new NumberZ("3"), new NumberZ("4")), r);
        assertThat("A^{a/b} = sqrt[b](A^a)",
                out.equals(new F(F.ROOTOF, new NumberZ("125"), new NumberZ("4")), r));
        out = in.pow(new Fraction(new NumberZ("-3"), new NumberZ("4")), r);
        assertThat("A^{-a/b} = 1/(sqrt[b](A^a))", out.equals(new Fraction(
                NumberZ.ONE, new F(F.ROOTOF, new Element[]{
                new NumberZ("125"), new NumberZ("4")
        })), r));

        in = new Polynom("x^4", r); Polynom  jjjj=new Polynom("x", r);
        out = in.pow(new Fraction(new NumberZ("1"), new NumberZ("4")), r);
        assertThat("(x^4)^{1/4} = x", out.equals(new FactorPol( (new Polynom("x", r)) ),r));
        out = in.pow(new Fraction(new NumberZ("1"), new NumberZ("2")), r);
        assertThat("(x^4)^{1/2} = x^2", out.equals(
                new FactorPol(new int[]{2}, new Polynom[]{new Polynom("x", r)}), r));
        out = in.pow(new Fraction(new NumberZ("-3"), new NumberZ("4")), r);
        assertThat("(x^4)^{-3/4} = 1/(x^3)", out.equals(
                new Fraction(NumberZ.ONE,
                        new FactorPol(new int[]{3}, new Polynom[]{new Polynom("x", r)})), r));

        in = new Polynom("(x+2)(x+3)^4", r);
        out = in.pow(new Fraction(new NumberZ("1"), new NumberZ("4")), r);
        assertThat("((x+2)(x+3)^4)^{1/4} = (x+3)*sqrt[4](x+2)",
                out.equals(
                        new F(
                                F.MULTIPLY,
                                new FactorPol(new Polynom("x+3", r)),
                                new F(
                                        F.ROOTOF, new FactorPol(new Polynom("x+2", r)),
                                        new NumberZ("4"))), r));
    }
}
