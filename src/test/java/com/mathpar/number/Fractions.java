/**
* Copyright © 2011 Mathparca Ltd. All rights reserved.
*/

package com.mathpar.number;

import com.mathpar.func.F;
import com.mathpar.polynom.Polynom;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

/**
 *
 * @author gennadi
 */
public class Fractions {
        
    
    
    public static void doTest(Ring ring, String  num, String  den, String result,  int fName) {
       // System.out.println(" start="+num+"   "+den);
        Polynom n= new Polynom(num, ring);
        Polynom d= new Polynom(den, ring);
        Polynom out= new Polynom(result, ring);
        Element fr=new Fraction(n,d);
        Element res = (new F(fName, new Element[]{fr})).valueOf(ring);

        Element sub=res.subtract(out, ring).expand(ring);
        System.out.println(" res="+res.toString(ring)+"   "+out.toString(ring)+"  "+sub);
        assertTrue(sub.isZero(ring));
    }

    @Test
    public void pol1_z() {
        doTest(new Ring("Q[z]"), "z^2","z+1", "z^2 ", F.NUM);
    }
}
    
   