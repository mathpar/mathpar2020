package com.mathpar.number;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class NumberLimitTest {

    @Test
    public void left() {
        assertEquals(-1, NumberLimit.fromLeft(1).left());
        assertEquals(1, NumberLimit.fromRight(1).left());
        assertEquals(0, NumberLimit.neutral(1).left());
    }

    @Test
    public void isINFINITY() {
        assertFalse(NumberLimit.neutral(1).isINFINITY());

        assertTrue(NumberLimit.neutral(Double.POSITIVE_INFINITY).isINFINITY());
        assertTrue(NumberLimit.neutral(Double.NEGATIVE_INFINITY).isINFINITY());
    }

    @Test
    public void isNegativeInfinity() {
        assertTrue(new NumberLimit(Double.NEGATIVE_INFINITY).isNegativeInfinity());
        assertFalse(new NumberLimit(1).isNegativeInfinity());
        assertFalse(new NumberLimit(Double.POSITIVE_INFINITY).isNegativeInfinity());
    }

    @Test
    public void isPositiveInfinity() {
        assertTrue(new NumberLimit(Double.POSITIVE_INFINITY).isPositiveInfinity());
        assertFalse(new NumberLimit(1).isPositiveInfinity());
        assertFalse(new NumberLimit(Double.NEGATIVE_INFINITY).isPositiveInfinity());
    }

    @Test
    public void isNAN() {
        assertTrue(new NumberLimit(Double.NaN).isNAN());
        assertFalse(new NumberLimit(5).isNAN());
        assertFalse(new NumberLimit(Double.POSITIVE_INFINITY).isNAN());
        assertFalse(new NumberLimit(Double.NEGATIVE_INFINITY).isNAN());
    }

    @Test
    public void negate() {
        NumberLimit number = new NumberLimit(5.31, 1);
        NumberLimit negated = number.negate();
        assertEquals(-5.31, negated.doubleValue(), 0.001);
        assertEquals(1, negated.r_or_l);

        NumberLimit zeroNumber = new NumberLimit(0, 0);
        NumberLimit zeroNegated = zeroNumber.negate();
        assertEquals(0, zeroNegated.doubleValue(), 0.001);
        assertEquals(0, zeroNegated.r_or_l);

        NumberLimit negativeNumber = new NumberLimit(-10, -1);
        NumberLimit negativeNegated = negativeNumber.negate();
        assertEquals(10, negativeNegated.doubleValue(), 0.001);
        assertEquals(-1, negativeNegated.r_or_l);
    }

    @Test
    public void negateRoL() {
        NumberLimit number = new NumberLimit(5.31, 1);
        NumberLimit negatedRoL = number.negateRoL();
        assertEquals(5.31, negatedRoL.doubleValue(), 0.001);
        assertEquals(-1, negatedRoL.r_or_l);

        NumberLimit zeroNumber = new NumberLimit(0, 0);
        NumberLimit zeroNegatedRoL = zeroNumber.negateRoL();
        assertEquals(0, zeroNegatedRoL.doubleValue(), 0.001);
        assertEquals(0, zeroNegatedRoL.r_or_l);

        NumberLimit negativeNumber = new NumberLimit(-10, -1);
        NumberLimit negativeNegatedRoL = negativeNumber.negateRoL();
        assertEquals(-10, negativeNegatedRoL.doubleValue(), 0.001);
        assertEquals(1, negativeNegatedRoL.r_or_l);
    }

    @Test
    public void signum() {
        NumberLimit positiveNumber = new NumberLimit(5.31);
        int signumValue = positiveNumber.signum();
        assertEquals(1, signumValue);

        NumberLimit zeroNumber = new NumberLimit(0);
        signumValue = zeroNumber.signum();
        assertEquals(1, signumValue);

        NumberLimit negativeNumber = new NumberLimit(-10);
        signumValue = negativeNumber.signum();
        assertEquals(-1, signumValue);
    }

    @Test
    public void negateAll() {
        NumberLimit number = new NumberLimit(5.31, 1);
        NumberLimit negatedAll = number.negateAll();
        assertEquals(-5.31, negatedAll.doubleValue(), 0.001);
        assertEquals(-1, negatedAll.r_or_l);

        NumberLimit zeroNumber = new NumberLimit(0, 0);
        NumberLimit zeroNegatedAll = zeroNumber.negateAll();
        assertEquals(0, zeroNegatedAll.doubleValue(), 0.001);
        assertEquals(0, zeroNegatedAll.r_or_l);

        NumberLimit negativeNumber = new NumberLimit(-10, -1);
        NumberLimit negativeNegatedAll = negativeNumber.negateAll();
        assertEquals(10, negativeNegatedAll.doubleValue(), 0.001);
        assertEquals(1, negativeNegatedAll.r_or_l);
    }

    @Test
    public void doubleValue() {
        NumberLimit number = new NumberLimit(5.31);
        double doubleVal = number.doubleValue();
        assertEquals(5.31, doubleVal, 0.001);

        NumberLimit negativeNumber = new NumberLimit(-10);
        doubleVal = negativeNumber.doubleValue();
        assertEquals(-10, doubleVal, 0.001);
    }

    @Test
    public void longValue() {
        NumberLimit number = new NumberLimit(5.31);
        long longVal = number.longValue();
        assertEquals(5, longVal);

        NumberLimit negativeNumber = new NumberLimit(-3.26);
        longVal = negativeNumber.longValue();
        assertEquals(-3, longVal);
    }

    @Test
    public void intValue() {
        NumberLimit number = new NumberLimit(5.31);
        int intVal = number.intValue();
        assertEquals(5, intVal);

        NumberLimit negativeNumber = new NumberLimit(-10);
        intVal = negativeNumber.intValue();
        assertEquals(-10, intVal);
    }

    @Disabled
    public void testToString() {
    }

    @Test
    public void toNumber() {
        NumberLimit number = new NumberLimit(5.0, 1);
        Ring ring = new Ring();

        Element resultZ = number.toNumber(Ring.Z, ring);
        assertTrue(resultZ instanceof NumberZ);

        Element resultZ64 = number.toNumber(Ring.Z64, ring);
        assertTrue(resultZ64 instanceof NumberZ64);

        Element resultZp32 = number.toNumber(Ring.Zp32, ring);
        assertTrue(resultZp32 instanceof NumberZp32);

        Element resultZp = number.toNumber(Ring.Zp, ring);
        assertTrue(resultZp instanceof NumberZp);

        Element resultR = number.toNumber(Ring.R, ring);
        assertTrue(resultR instanceof NumberR);

        Element resultR64 = number.toNumber(Ring.R64, ring);
        assertTrue(resultR64 instanceof NumberLimit);

        Element resultR128 = number.toNumber(Ring.R128, ring);
        assertTrue(resultR128 instanceof NumberR128);
    }

    @Test
    public void isZero() {
        NumberLimit zeroNumber = new NumberLimit(0);
        Ring ring = new Ring();
        assertTrue(zeroNumber.isZero(ring));

        NumberLimit nonZeroNumber = new NumberLimit(5.31);
        assertFalse(nonZeroNumber.isZero(ring));
    }

    @Test
    public void testEquals() {
        NumberLimit number1 = new NumberLimit(5, 1);
        NumberLimit number2 = new NumberLimit(5, 1);
        Ring ring = new Ring();
        assertTrue(number1.equals(number2, ring));

        NumberLimit number3 = new NumberLimit(5, 1);
        NumberLimit number4 = new NumberLimit(3, 1);
        assertFalse(number3.equals(number4, ring));
    }

    @Test
    public void compareTo() {
        NumberLimit number1 = new NumberLimit(5, 1);
        NumberLimit number2 = new NumberLimit(5, 1);
        assertEquals(0, number1.compareTo(number2));

        NumberLimit number3 = new NumberLimit(5, 1);
        NumberLimit number4 = new NumberLimit(3, 1);
        assertTrue(number3.compareTo(number4) > 0);
    }


    @Disabled
    public void testCompareTo() {
    }

    @Disabled
    public void isOne() {
    }
}