package com.mathpar.number;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class NumberR128Test {

    @Test
    public void testNumberR128ConstructorIgnoresExponentWhenValueIsNan() {
        NumberR128 numberR128 = new NumberR128(Double.NaN, 1000);

        assertEquals(new NumberR128(Double.NaN, 0), numberR128);
    }

    @Test
    public void testNumbElementTypeReturnsR128() {
        NumberR128 number = new NumberR128();
        assertEquals(Ring.R128, number.numbElementType());
    }

    @Test
    public void testObjectEqualsForDifferentTypesReturnsFalse() {
        assertFalse(new NumberR128(0, 0).equals(new NumberR64(0)));
    }

    @Test
    public void testObjectEqualsForSameValueReturnsTrue() {
        double maxPreciseIntegerForDouble = 1L << 53;

        assertTrue(new NumberR128(maxPreciseIntegerForDouble, 0).equals(new NumberR128(maxPreciseIntegerForDouble, 0)));
    }

    @Test
    public void testObjectEqualsForSameValueDifferentExponentReturnsTrue() {
        double maxPreciseIntegerForDouble = 1L << 53;

        // asserting that values are normalized before doing equals
        assertTrue(new NumberR128(maxPreciseIntegerForDouble, 0).equals(new NumberR128(1L << 52, 1)));
        assertTrue(new NumberR128(maxPreciseIntegerForDouble, 0).equals(new NumberR128(1L, 53)));
    }

    @Test
    public void testDoubleValueWithZeroExponentReturnsSameValue() {
        double d = 42.42;
        assertEquals(d, new NumberR128(d, 0).doubleValue(), 0.0);

        d = -42.42;
        assertEquals(d, new NumberR128(d, 0).doubleValue(), 0.0);
    }

    @Test
    public void testDoubleValueBigPositiveExponentReturnsPositiveInfinity() {
        double d = 42.42;
        assertEquals(Double.POSITIVE_INFINITY, new NumberR128(d, 3000).doubleValue(), 0.0);
    }

    @Test
    public void testDoubleValueBigPositiveExponentReturnsNegativeInfinity() {
        double d = -42.42;
        assertEquals(Double.NEGATIVE_INFINITY, new NumberR128(d, 3000).doubleValue(), 0.0);
    }

    @Test
    public void testDoubleValueBigNegativeExponentReturnsPlusZero() {
        double d = 42.42;
        assertEquals(0.0, new NumberR128(d, -3000).doubleValue(), 0.0);
    }

    @Test
    public void testDoubleValueBigNegativeExponentReturnsMinusZero() {
        double d = -42.42;
        assertEquals(-0.0, new NumberR128(d, -3000).doubleValue(), 0.0);
    }

    @Test
    public void testDoubleValueReturnsZeroWhenValueIsZero() {
        NumberR128 numberR128 = new NumberR128(0, 9999);
        assertEquals(0.0, numberR128.doubleValue(), 0.0);
    }

    @Test
    public void testDoubleValueReturnsValueInDoubleRangeWhenExponentInRange() {
        double value = 42.42;
        NumberR128 numberR128 = new NumberR128(value, 10);

        assertEquals(value * Math.pow(2, 10), numberR128.doubleValue(), 0.0);
    }

    @Test
    public void testIsZeroReturnsTrueForZero() {
        Ring ring = new Ring();
        NumberR128 numberR128 = new NumberR128(0, 1000);
        assertTrue(numberR128.isZero(ring));
    }

    @Test
    public void testIsZeroReturnsTrueForZeroPlusMinusHalfEpsilon() {
        Ring ring = new Ring();
        double value = ring.MachineEpsilonR64.value / 2;
        assertTrue(new NumberR128(value, 0).isZero(ring));
        assertTrue(new NumberR128(-value, 0).isZero(ring));
    }

    @Test
    public void testIsZeroReturnsTrueForZeroPlusMinusEplsilon() {
        Ring ring = new Ring();
        double value = ring.MachineEpsilonR64.value;
        assertFalse(new NumberR128(value, 0).isZero(ring));
        assertFalse(new NumberR128(-value, 0).isZero(ring));
    }

    @Test
    public void testIsZeroReturnsTrueForZeroPlusMinusTwoEplsilon() {
        Ring ring = new Ring();
        double value = 2 * ring.MachineEpsilonR64.value;
        assertFalse(new NumberR128(value, 0).isZero(ring));
        assertFalse(new NumberR128(-value, 0).isZero(ring));
    }

    @Test
    public void testIsZeroResultDependsOnEpsilonRingSetEpsilon() {
        Ring ring = new Ring();
        ring.setMachineEpsilonR64(2);
        double epsilon = ring.MachineEpsilonR64.value;

        assertTrue(new NumberR128(0, 0).isZero(ring));
        assertTrue(new NumberR128(epsilon / 2, 0).isZero(ring));
        assertTrue(new NumberR128(- epsilon / 2, 0).isZero(ring));

        assertFalse(new NumberR128(epsilon, 0).isZero(ring));
        assertFalse(new NumberR128(-epsilon, 0).isZero(ring));
        assertFalse(new NumberR128(2 * epsilon, 0).isZero(ring));
        assertFalse(new NumberR128(2 * -epsilon, 0).isZero(ring));
    }

    @Test
    public void testIsOneReturnsTrueForOne() {
        Ring ring = new Ring();
        NumberR128 numberR128 = new NumberR128(1, 0);
        assertTrue(numberR128.isOne(ring));
    }

    @Test
    public void testIsOneReturnsTrueForOnePlusMinusHalfEpsilon() {
        Ring ring = new Ring();
        double value = ring.MachineEpsilonR64.value / 2;
        assertTrue(new NumberR128(1 + value, 0).isOne(ring));
        assertTrue(new NumberR128(1 - value, 0).isOne(ring));
    }

    @Test
    public void testIsOneReturnsTrueForOnePlusMinusEplsilon() {
        Ring ring = new Ring();
        double value = ring.MachineEpsilonR64.value;
        assertFalse(new NumberR128(1 + value, 0).isOne(ring));
        assertFalse(new NumberR128(1 - value, 0).isOne(ring));
    }

    @Test
    public void testIsOneReturnsTrueForOnePlusMinusTwoEplsilon() {
        Ring ring = new Ring();
        double value = 2 * ring.MachineEpsilonR64.value;
        assertFalse(new NumberR128(1 + value, 0).isOne(ring));
        assertFalse(new NumberR128(1 - value, 0).isOne(ring));
    }

    @Test
    public void testIsOneResultDependsOnEpsilonRingSetEpsilon() {
        Ring ring = new Ring();
        ring.setMachineEpsilonR64(2);
        double epsilon = ring.MachineEpsilonR64.value;

        assertTrue(new NumberR128(1, 0).isOne(ring));
        assertTrue(new NumberR128(1 + epsilon / 2, 0).isOne(ring));
        assertTrue(new NumberR128(1 - epsilon / 2, 0).isOne(ring));

        assertFalse(new NumberR128(1 + epsilon, 0).isOne(ring));
        assertFalse(new NumberR128(1 - epsilon, 0).isOne(ring));
        assertFalse(new NumberR128(1 + 2 * epsilon, 0).isOne(ring));
        assertFalse(new NumberR128(1 - 2 * epsilon, 0).isOne(ring));
    }

    @Test
    public void testIsMinusOneReturnsTrueForMinusOne() {
        Ring ring = new Ring();
        NumberR128 numberR128 = new NumberR128(-1, 0);
        assertTrue(numberR128.isMinusOne(ring));
    }

    @Test
    public void testIsMinusOneReturnsTrueForMinusOnePlusMinusHalfEpsilon() {
        Ring ring = new Ring();
        double value = ring.MachineEpsilonR64.value / 2;
        assertTrue(new NumberR128(-1 + value, 0).isMinusOne(ring));
        assertTrue(new NumberR128(-1 - value, 0).isMinusOne(ring));
    }

    @Test
    public void testIsMinusOneReturnsTrueForMinusOnePlusMinusEplsilon() {
        Ring ring = new Ring();
        double value = ring.MachineEpsilonR64.value;
        assertFalse(new NumberR128(-1 + value, 0).isMinusOne(ring));
        assertFalse(new NumberR128(-1 - value, 0).isMinusOne(ring));
    }

    @Test
    public void testIsMinusOneReturnsTrueForMinusOnePlusMinusTwoEplsilon() {
        Ring ring = new Ring();
        double value = 2 * ring.MachineEpsilonR64.value;
        assertFalse(new NumberR128(-1 + value, 0).isMinusOne(ring));
        assertFalse(new NumberR128(-1 - value, 0).isMinusOne(ring));
    }

    @Test
    public void testIsMinusOneResultDependsOnEpsilonRingSetEpsilon() {
        Ring ring = new Ring();
        ring.setMachineEpsilonR64(2);
        double epsilon = ring.MachineEpsilonR64.value;

        assertTrue(new NumberR128(-1, 0).isMinusOne(ring));
        assertTrue(new NumberR128(-1 + epsilon / 2, 0).isMinusOne(ring));
        assertTrue(new NumberR128(-1 - epsilon / 2, 0).isMinusOne(ring));

        assertFalse(new NumberR128(-1 + epsilon, 0).isMinusOne(ring));
        assertFalse(new NumberR128(-1 - epsilon, 0).isMinusOne(ring));
        assertFalse(new NumberR128(-1 + 2 * epsilon, 0).isMinusOne(ring));
        assertFalse(new NumberR128(-1 - 2 * epsilon, 0).isMinusOne(ring));
    }

    @Test
    public void testAddWithNaNReturnsNaN() {
        NumberR128 numberR128 = new NumberR128(1, 0);
        Ring ring = new Ring();
        Element result = numberR128.add(Element.NAN, ring);
        assertEquals(Element.NAN, result);
    }

    @Test
    public void testAddWithOtherNumberR128ReturnsSumOfDouble() {
        double value = 42.42;
        NumberR128 numberR128 = new NumberR128(value);
        Ring ring = new Ring();
        double otherValue = 18.18;
        NumberR128 other = new NumberR128(otherValue);
        Element result = numberR128.add(other, ring);
        assertEquals(new NumberR128(value + otherValue), result);
    }

    @Test
    public void testSubtractWithNaNReturnsNaN() {
        NumberR128 numberR128 = new NumberR128(1, 0);
        Ring ring = new Ring();
        Element result = numberR128.subtract(Element.NAN, ring);
        assertEquals(Element.NAN, result);
    }

    @Test
    public void testSubtractWithOtherNumberR128ReturnsDiffOfDouble() {
        double value = 42.42;
        NumberR128 numberR128 = new NumberR128(value);
        Ring ring = new Ring();
        double otherValue = 18.18;
        NumberR128 other = new NumberR128(otherValue);
        Element result = numberR128.subtract(other, ring);
        assertEquals(new NumberR128(value - otherValue), result);
    }

    @Test
    public void testMultiplyWithNaNReturnsNaN() {
        NumberR128 numberR128 = new NumberR128(1, 0);
        Ring ring = new Ring();
        Element result = numberR128.multiply(Element.NAN, ring);
        assertEquals(Element.NAN, result);
    }

    @Test
    public void testMultiplyWithOtherNumberR128ReturnsProductOfDouble() {
        double value = 42.42;
        NumberR128 numberR128 = new NumberR128(value);
        Ring ring = new Ring();
        double otherValue = 18.18;
        NumberR128 other = new NumberR128(otherValue);
        Element result = numberR128.multiply(other, ring);
        assertEquals(new NumberR128(value * otherValue), result);
    }

    @Test
    public void testDivideWithNaNReturnsNaN() {
        NumberR128 numberR128 = new NumberR128(1, 0);
        Ring ring = new Ring();
        Element result = numberR128.divide(Element.NAN, ring);
        assertEquals(Element.NAN, result);
    }

    @Test
    public void testDivideWithOtherNumberR128ReturnsQuotientOfDouble() {
        double value = 42.42;
        NumberR128 numberR128 = new NumberR128(value);
        Ring ring = new Ring();
        double otherValue = 18.18;
        NumberR128 other = new NumberR128(otherValue);
        Element result = numberR128.divide(other, ring);
        assertEquals(new NumberR128(value / otherValue), result);
    }

}
