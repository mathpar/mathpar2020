package com.mathpar.number;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class NumberZ64Test {
    @Test
    public void toStringTest() {
        Ring r = new Ring("Z64[x]");
        NumberZ64 a = new NumberZ64("42");
        String res = a.toString(r);
        assertEquals( "42", res);
    }

    @Test
    public void associativeAdditionTest() {
        //(a + b) + c = a + (b + c) for all a, b, c in R

        Ring r = new Ring("Z64[x]");
        NumberZ64 a = new NumberZ64("2");
        NumberZ64 b = new NumberZ64("3");
        NumberZ64 c = new NumberZ64("423");

        assertEquals(a.add(b, r).add(c, r), b.add(c, r).add(a, r));
    }

    @Test
    public void commutativeAdditionTest() {
        //a + b = b + a for all a, b in R

        Ring r = new Ring("Z64[x]");
        NumberZ64 a = new NumberZ64("2");
        NumberZ64 b = new NumberZ64("3");

        assertEquals(a.add(b, r), b.add(a, r));
    }

    @Test
    public void additiveIdentityTest() {
        //There is an element 0 in R such that a + 0 = a for all a in R

        Ring r = new Ring("Z64[x]");
        NumberZ64 a = new NumberZ64("2");

        assertEquals(a.add(NumberZ64.ZERO, r), a);
    }

    @Test
    public void additiveInverseTest() {
        //For each a in R there exists −a in R such that a + (−a) = 0

        Ring r = new Ring("Z64[x]");
        NumberZ64 a = new NumberZ64("2");
        NumberZ64 b = (NumberZ64) a.multiply(NumberZ64.MINUS_ONE);

        assertEquals(a.add(b, r),NumberZ64.ZERO);
    }

    @Test
    public void associativeMultiplicationTest() {
        //(a · b) · c = a · (b · c) for all a, b, c in R

        Ring r = new Ring("Z64[x]");
        NumberZ64 a = new NumberZ64("2");
        NumberZ64 b = new NumberZ64("3");
        NumberZ64 c = new NumberZ64("423");

        assertEquals(a.multiply(b, r).multiply(c, r), b.multiply(c, r).multiply(a, r));
    }

    @Test
    public void multiplicativeIdentityTest() {
        //There is an element 1 in R such that a · 1 = a and 1 · a = a for all a in R

        Ring r = new Ring("Z64[x]");
        NumberZ64 a = new NumberZ64("7");

        assertEquals(a.multiply(NumberZ64.ONE, r), a);
    }

    @Test
    public void leftDistributivityTest() {
        // a · (b + c) = (a · b) + (a · c) for all a, b, c in R (left distributivity)

        Ring r = new Ring("Z64[x]");
        NumberZ64 a = new NumberZ64("2");
        NumberZ64 b = new NumberZ64("3");
        NumberZ64 c = new NumberZ64("3");

        assertEquals(a.multiply((b.add(c,r)), r), a.multiply(b, r).add(a.multiply(c, r),r));
    }
    @Test
    public void rightDistributivityTest() {
        //(b + c) · a = (b · a) + (c · a) for all a, b, c in R (right distributivity)

        Ring r = new Ring("Z64[x]");
        NumberZ64 a = new NumberZ64("2");
        NumberZ64 b = new NumberZ64("3");
        NumberZ64 c = new NumberZ64("3");

        assertEquals(b.add(c,r).multiply(a,r), b.multiply(a, r).add(c.multiply(a, r),r));
    }
}