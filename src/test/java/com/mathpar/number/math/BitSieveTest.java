package com.mathpar.number.math;

import com.mathpar.number.NumberZ;

import java.util.Random;
import org.junit.jupiter.api.Test;

import static com.thoughtworks.selenium.SeleneseTestBase.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class BitSieveTest {
    @Test
    public void testRetrieveWithCertainty10() {
        BitSieve bs = new BitSieve(new NumberZ(3), 64);
        NumberZ initValue = new NumberZ(4);
        int certainty = 10;
        Random rnd = new Random(100);

        NumberZ primeCandidate = bs.retrieve(initValue, certainty, rnd);
        assertNotNull(primeCandidate, "Prime candidate should not be null");
        assertEquals(primeCandidate, new NumberZ(5));
        assertTrue("Retrieved number should be probably prime with certainty level " + certainty, primeCandidate.isProbablePrime(certainty));
    }

    @Test
    public void testRetrieveWithCertainty20() {
        BitSieve bs = new BitSieve(new NumberZ(3), 64);
        NumberZ initValue = new NumberZ(102);
        int certainty = 20;
        Random rnd = new Random(100);


        NumberZ primeCandidate = bs.retrieve(initValue, certainty, rnd);
        assertNotNull(primeCandidate, "Prime candidate should not be null");
        assertEquals(primeCandidate, new NumberZ(103));
        assertTrue("Retrieved number should be probably prime with certainty level " + certainty, primeCandidate.isProbablePrime(certainty));
    }
}