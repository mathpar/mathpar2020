 /**
* Copyright © 2011 Mathparca Ltd. All rights reserved.
*/

package com.mathpar.polynom;

import com.mathpar.func.F;
import com.mathpar.number.Element;
import com.mathpar.number.NumberZ;
import com.mathpar.number.Ring;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;

import java.util.concurrent.TimeUnit;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;

public class ZhegalkinTest {
    
        @Test
        @Timeout(value = 5000, unit = TimeUnit.MILLISECONDS)
    public void factorInR64() {
        Ring r = new Ring("R64[x,y,z,t]");
        Polynom p = new Polynom(" xyzt ", r);
        Zhegalkin res = new Zhegalkin(p);
        Polynom p1 = new Polynom("(xyz)", r);
        Polynom p2 = new Polynom("x ", r);
        res=res.add(p1).add(p2);
          // System.out.println("res="+res.toString(r));
        int[] pow = new int[] {1, 1, 1, 1, 1, 1, 1, 0,1,0,0,0};
        Zhegalkin expected = new Zhegalkin(pow, 3);
       // res=res.multiply(res);
  // System.out.println(expected.toString(r));
    // System.out.println(res.toString(r));
        res = res.subtract(expected);
            // System.out.println("pol==="+res);
        Boolean bb= res.isZero(r);
        assertTrue(bb);
    }
    
    
 
    
}
