/**
* Copyright © 2011 Mathparca Ltd. All rights reserved.
*/

package com.mathpar.polynom;

import com.mathpar.func.Page;
import com.mathpar.number.Ring;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 *
 * @author gennadi
 */
public class solveOfEqTest {
 
    private Ring ring;

    @BeforeEach
    public void beforeTest() {
        ring = new Ring("Q[x, y, z]");
    }
 
            @Test
    public void Test1() {
        Page p = new Page(ring);
        
        com.mathpar.func.PageTest.doPageTest(p,
                "TIMEOUT=1; b = \\solve(x^2 + 4x - 7 = 0);", " [(\\sqrt{11}+ (-2)), ((-1)* \\sqrt{11}+ (-2))]");
    }
}
