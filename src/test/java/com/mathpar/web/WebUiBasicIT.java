/**
* Copyright © 2011 Mathparca Ltd. All rights reserved.
*/

package com.mathpar.web;

import com.codeborne.selenide.Configuration;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class WebUiBasicIT {
    //ToDo @Rule // automatically takes screenshot of every failed test

    @BeforeAll
    public static void beforeClass() {
        Configuration.baseUrl = "http://localhost:8080";
    }

    @BeforeEach
    public void before() {
        open("/mathpar/en/");
    }

    //@Test
    public void sidebarClickExpandsFirstLevelPanel() {
        $("#kbd_space_const").shouldHave(cssClass("collapse"));
        $("button[data-target='#kbd_space_const']").click();
        $("#kbd_space_const").shouldBe(visible);
    }

    //@Test
    public void sidebarClickExpandsSecondLevelPanel() {
        $("button[data-target='#kbd_space_const']").click();

        $("#kbd_space_const_const").shouldHave(cssClass("collapse"));
        $("*[data-target='#kbd_space_const_const']").click();
        $("#kbd_space_const_const").shouldBe(visible);
    }

    //@Test
    public void sidebarButtonsInsertText() {
        $("button[data-target='#kbd_space_const']").click();

        $("*[data-inserts='SPACE = Z[];']", 0).click();
        $("#section_0 textarea").shouldHave(value("SPACE = Z[];"));
    }
}
